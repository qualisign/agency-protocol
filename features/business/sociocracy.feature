Feature: Circle Pattern
  Enable semi-autonomous working groups with consent governance

  Background:
    Given the minimal organizational agent exists
    And decisions are made by consent
    And circles are semi-autonomous
    And double links connect circles

  Scenario: Circle Formation
    Given work needs organization
    When forming a circle
    Then establish:
      | Element         | Content                                                    |
      | Aim            | Circle's purpose and domain                               |
      | Members        | Core circle participants                                  |
      | Functions      | Required work areas                                       |
      | Agreements     | Operating principles                                      |
    And ensure:
      | Principle      | Implementation                                             |
      | Equivalence    | Member equality in decisions                              |
      | Effectiveness  | Clear work domain                                         |
      | Transparency   | Open information flow                                     |
      | Accountability | Clear responsibilities                                    |

  Scenario: Double Link Formation
    Given circles need connection
    When establishing links
    Then create:
      | Role            | Purpose                                                    |
      | Leader         | Downward context and resources                            |
      | Delegate       | Upward feedback and needs                                 |
      | Backup         | Support for each link                                     |
    And ensure:
      | Principle      | Implementation                                             |
      | Equivalence    | Equal voice in both circles                               |
      | Flow           | Two-way information exchange                              |
      | Integration    | Policy alignment                                          |
      | Learning       | Experience sharing                                        |

Feature: Consent Decision Pattern
  Enable inclusive decision-making without blocking progress

  Scenario: Consent Process
    Given a decision needs making
    When seeking consent
    Then follow:
      | Phase           | Action                                                     |
      | Present        | Share proposal                                            |
      | Clarify        | Questions for understanding                               |
      | Quick React    | Initial responses                                         |
      | Consent Round  | Check for objections                                      |
      | Integration    | Address paramount objections                              |
    And verify:
      | Criterion       | Test                                                      |
      | Understanding  | Proposal is clear                                         |
      | Safety         | No harm foreseen                                         |
      | Progress       | Moves toward aim                                         |
      | Measurability  | Effects can be evaluated                                 |

  Scenario: Objection Processing
    Given an objection is raised
    When processing objection
    Then evaluate:
      | Test            | Verification                                              |
      | Harm           | Creates specific risk                                     |
      | Purpose        | Limits aim achievement                                    |
      | Improvability  | Can be improved                                          |
      | Timeliness     | Based on known data                                      |
    And integrate:
      | Step            | Action                                                    |
      | Understand     | Clarify objection basis                                  |
      | Generate       | Develop alternatives                                     |
      | Test           | Check new proposal                                       |
      | Resolve        | Gain consent                                             |

Feature: Role Selection Pattern
  Enable consent-based role allocation

  Scenario: Role Selection
    Given a role needs filling
    When selecting person
    Then execute:
      | Phase           | Action                                                     |
      | Define         | Role requirements                                         |
      | Nominate       | Write private nominations                                 |
      | Share          | Present nominations                                       |
      | Change         | Adjust nominations                                        |
      | Consent        | Process selection                                         |
    And ensure:
      | Principle      | Implementation                                             |
      | Transparency   | Open process                                              |
      | Qualification  | Clear fit                                                 |
      | Willingness    | Positive acceptance                                       |
      | Support        | Group backing                                             |

Feature: Meeting Pattern
  Enable effective circle gatherings

  Scenario: Circle Meeting
    Given a circle needs to meet
    When conducting meeting
    Then include:
      | Element         | Purpose                                                    |
      | Opening        | Present and ready                                         |
      | Administrative | Meeting basics                                            |
      | Content        | Main agenda items                                         |
      | Review         | Meeting effectiveness                                     |
      | Closing        | Clear completion                                          |
    And maintain:
      | Practice       | Implementation                                             |
      | Rounds        | Structured participation                                   |
      | Facilitation  | Process guidance                                          |
      | Recording     | Clear decisions                                           |
      | Evaluation    | Continuous improvement                                     |

Feature: Policy Development Pattern
  Enable clear agreements for operation

  Scenario: Policy Creation
    Given guidance is needed
    When developing policy
    Then specify:
      | Element         | Content                                                    |
      | Aim            | Policy purpose                                            |
      | Scope          | Application domain                                        |
      | Specifics      | Clear guidelines                                         |
      | Measurements   | Success indicators                                        |
    And ensure:
      | Principle      | Implementation                                             |
      | Clarity        | Unambiguous guidance                                      |
      | Necessity      | Addresses real need                                       |
      | Effectiveness  | Achieves purpose                                          |
      | Review         | Regular evaluation                                        |

Feature: Operations Pattern
  Enable effective day-to-day work

  Scenario: Operational Decision Making
    Given daily work occurs
    When making decisions
    Then apply:
      | Principle       | Implementation                                             |
      | Consent Range  | Clear decision scope                                      |
      | Lead Links     | Operational authority                                     |
      | Circle Policies| Agreed guidelines                                         |
      | Feedback       | Regular review                                            |
    And ensure:
      | Aspect         | Practice                                                   |
      | Flow          | Smooth operation                                          |
      | Learning      | Experience capture                                        |
      | Adjustment    | Quick adaptation                                          |
      | Support       | Resource availability                                      |

Feature: Development Pattern
  Enable continuous organizational evolution

  Scenario: Learning Integration
    Given experience accumulates
    When evolving practice
    Then focus:
      | Area            | Development                                               |
      | Skills         | Individual capacity                                      |
      | Process        | Meeting effectiveness                                    |
      | Understanding  | Sociocratic principles                                   |
      | Application    | Context adaptation                                       |
    And maintain:
      | Principle      | Implementation                                             |
      | Growth         | Continuous improvement                                    |
      | Integration    | Coherent practice                                        |
      | Adaptation     | Local relevance                                          |
      | Sustainability | Long-term viability                                      |

Feature: Circle Interaction Pattern
  Enable effective inter-circle coordination

  Scenario: Circle Cooperation
    Given circles need coordination
    When working together
    Then support:
      | Mechanism       | Purpose                                                    |
      | Joint Circles  | Cross-domain collaboration                                |
      | Helping Circles| Temporary support                                         |
      | External Expert| Specialized input                                         |
      | Delegation    | Specific tasks                                            |
    And maintain:
      | Principle      | Implementation                                             |
      | Clarity        | Clear relationships                                       |
      | Autonomy       | Respected domains                                         |
      | Cooperation    | Mutual support                                            |
      | Learning       | Shared growth                                             |
