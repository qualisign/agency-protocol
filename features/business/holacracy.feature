Feature: Role Pattern
  Enable clear energization of work through explicit roles

  Background:
    Given the minimal organizational agent exists
    And work is organized through roles
    And roles are defined by purpose and accountabilities
    And roles are distinct from individuals

  Scenario: Role Definition
    Given work needs to be done
    When defining a role
    Then specify:
      | Element         | Content                                                    |
      | Purpose        | Role's unique contribution                                |
      | Domain         | Areas of exclusive authority                              |
      | Accountabilities| Ongoing expectations                                     |
      | Policies       | Constraints on domain control                            |
    And ensure:
      | Principle      | Implementation                                             |
      | Clarity        | Unambiguous scope                                         |
      | Autonomy       | Clear authority                                           |
      | Necessity      | Serves broader purpose                                    |
      | Distinctness   | No overlap with other roles                              |

  Scenario: Role Assignment
    Given roles exist
    When energizing roles
    Then track:
      | Element         | Content                                                    |
      | Focus Role     | Primary accountability                                    |
      | Supporting Roles| Additional energized roles                               |
      | Capacity       | Available energy/time                                     |
      | Fit            | Role-person alignment                                     |
    And maintain:
      | Principle      | Implementation                                             |
      | Flexibility    | Dynamic assignment                                        |
      | Transparency   | Clear energization                                        |
      | Balance        | Sustainable loading                                       |
      | Growth         | Skill development                                         |

Feature: Circle Pattern
  Enable nested autonomy through circle structure

  Scenario: Circle Structure
    Given work needs organization
    When forming circles
    Then define:
      | Element         | Content                                                    |
      | Purpose        | Circle's reason for existence                             |
      | Domain         | Circle's exclusive authority                              |
      | Inner Roles    | Contained roles and sub-circles                          |
      | Outer Link     | Connection to broader circle                             |
    And ensure:
      | Principle      | Implementation                                             |
      | Wholeness      | Complete scope coverage                                   |
      | Autonomy       | Independent operation                                     |
      | Alignment      | Purpose hierarchy                                         |
      | Integration    | Clear boundaries                                          |

  Scenario: Circle Roles
    Given a circle exists
    When establishing core roles
    Then create:
      | Role            | Purpose                                                    |
      | Lead Link      | Purpose manifestation                                     |
      | Rep Link       | Perspective sharing                                       |
      | Facilitator    | Governance process                                        |
      | Secretary      | Operational records                                       |

Feature: Governance Process Pattern
  Enable dynamic evolution through structured processes

  Scenario: Governance Meeting
    Given tensions need processing
    When holding governance meeting
    Then follow:
      | Phase           | Purpose                                                    |
      | Check-in       | Present and ready                                         |
      | Admin          | Logistics review                                          |
      | Agenda Build   | Tension collection                                        |
      | Processing     | Tension integration                                       |
      | Closing        | Reflection and data                                       |
    And ensure:
      | Principle      | Implementation                                             |
      | Participation  | All roles represented                                     |
      | Focus          | One tension at a time                                     |
      | Integration    | All objections resolved                                   |
      | Documentation  | Clear records                                             |

  Scenario: Tension Processing
    Given a tension is raised
    When processing in governance
    Then follow:
      | Step            | Action                                                     |
      | Present        | Proposer describes tension                                |
      | Clarify        | Questions for understanding                               |
      | React          | Quick reactions shared                                    |
      | Amend          | Proposer clarifies/adjusts                               |
      | Objection      | Valid objections raised                                  |
      | Integration    | Objections resolved                                      |
    And validate:
      | Criterion       | Test                                                      |
      | Harm           | Creates harm/regression                                   |
      | Limits         | Limits role's purpose                                    |
      | Data           | Based on present data                                    |
      | Causation      | Clear causal link                                        |

Feature: Tactical Process Pattern
  Enable operational coordination

  Scenario: Tactical Meeting
    Given operations need coordination
    When holding tactical meeting
    Then process:
      | Phase           | Purpose                                                    |
      | Check-in       | Present and ready                                         |
      | Checklist      | Recurring actions                                         |
      | Metrics        | Key indicators                                            |
      | Projects       | Progress updates                                          |
      | Tensions       | Quick triage                                              |
      | Closing        | Reflection                                                |
    And maintain:
      | Principle      | Implementation                                             |
      | Pace           | Quick processing                                          |
      | Focus          | Operational needs                                         |
      | Action         | Clear next-steps                                          |
      | Tracking       | Project status                                            |

Feature: Tension Sensing Pattern
  Enable evolutionary pressure through tension processing

  Scenario: Tension Sensing
    Given work happens
    When sensing tensions
    Then identify:
      | Element         | Content                                                    |
      | Gap            | Current vs needed reality                                 |
      | Impact         | Effect on purpose                                         |
      | Scope          | Relevant circle/role                                      |
      | Urgency        | Response timing                                           |
    And process:
      | Path            | Criteria                                                   |
      | Governance     | Role/policy change needed                                 |
      | Tactical       | Operational coordination                                  |
      | Individual     | Within current authority                                  |

Feature: Strategy Pattern
  Enable aligned autonomy through clear context

  Scenario: Strategy Formation
    Given a circle needs direction
    When developing strategy
    Then specify:
      | Element         | Content                                                    |
      | Context        | Environmental understanding                               |
      | Approach       | How purpose is pursued                                   |
      | Focus          | Priority areas                                           |
      | Constraints    | Implementation bounds                                    |
    And ensure:
      | Principle      | Implementation                                             |
      | Clarity        | Actionable guidance                                      |
      | Flexibility    | Adaptable framework                                      |
      | Alignment      | Supports purpose                                         |
      | Evolution      | Regular updates                                          |

Feature: Practice Integration Pattern
  Enable coherent adoption and practice

  Scenario: Practice Evolution
    Given constitution exists
    When evolving practice
    Then support:
      | Aspect          | Implementation                                             |
      | Learning       | Skill development                                         |
      | Coaching       | Practice guidance                                         |
      | Tools          | Process support                                           |
      | Culture        | Mindset development                                       |
    And maintain:
      | Principle      | Implementation                                             |
      | Fidelity       | Core rules followed                                      |
      | Adaptation     | Local customization                                      |
      | Growth         | Continuous improvement                                    |
      | Sustainability | Long-term viability                                      |
