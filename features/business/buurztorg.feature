Feature: Buurtzorg Inspired Professional Service Team Pattern
  Enable self-organizing professional teams in any domain

  Background:
    Given the minimal organizational agent exists
    And professionals need high autonomy
    And client relationships are primary
    And expertise develops through practice
    And support should enhance not control

  Scenario: Team Definition
    Given professionals want to form a team
    When establishing the team entity
    Then define core boundaries:
      | Element         | Content                                                    |
      | Size           | Small enough for direct relationships (8-12)               |
      | Scope          | Clear service domain and territory                         |
      | Capabilities   | Required professional competencies                         |
      | Resources      | Minimum viable tool set                                    |
    And ensure principles:
      | Principle      | Implementation                                             |
      | Wholeness      | Team has all needed capabilities                          |
      | Sustainability | Workload allows excellence                                |
      | Autonomy       | Team controls practice decisions                          |
      | Coherence      | Clear shared purpose                                      |

  Scenario: Client Relationship Pattern
    Given a team serves clients
    When managing service relationships
    Then track:
      | Element         | Content                                                    |
      | Client         | Service recipient entity                                   |
      | Context        | Relevant situation and needs                              |
      | Network        | Connected stakeholders                                    |
      | Journey        | Service evolution and outcomes                            |
    And maintain:
      | Principle      | Implementation                                             |
      | Continuity     | Stable professional relationships                         |
      | Empowerment    | Client capability building                                |
      | Integration    | Holistic need consideration                               |
      | Learning       | Continuous improvement                                    |

  Scenario: Professional Practice Pattern
    Given service relationships exist
    When delivering professional services
    Then enable:
      | Aspect         | Implementation                                             |
      | Solutions     | Team-developed approaches                                  |
      | Methods       | Team-chosen techniques                                     |
      | Standards     | Team-maintained quality                                    |
      | Innovation    | Team-driven improvement                                    |
    And ensure:
      | Principle      | Verification                                              |
      | Excellence     | Meets professional standards                              |
      | Ethics        | Upholds field principles                                  |
      | Evidence      | Documented effectiveness                                   |
      | Evolution     | Continuous refinement                                      |

Feature: Team Self-Organization Pattern
  Enable effective autonomous team operation

  Scenario: Internal Organization
    Given a team needs to coordinate
    When self-organizing
    Then implement:
      | Process        | Purpose                                                    |
      | Dialogue       | Regular reflection and planning                           |
      | Decisions      | Clear agreement methods                                   |
      | Roles          | Flexible responsibility sharing                           |
      | Reviews        | Systematic learning                                       |
    And maintain:
      | Principle      | Implementation                                             |
      | Initiative     | Members drive action                                      |
      | Collaboration  | Skills are shared                                         |
      | Transparency   | Clear information flow                                    |
      | Growth        | Continuous development                                     |

Feature: Coach Support Pattern
  Enable team development without control

  Scenario: Coach Role Definition
    Given teams need development support
    When establishing coach relationships
    Then define:
      | Element        | Content                                                    |
      | Purpose       | Team capability building                                   |
      | Scope         | Process and development focus                             |
      | Boundaries    | No operational authority                                  |
      | Access        | Team-controlled engagement                                |
    And ensure:
      | Principle      | Implementation                                             |
      | Autonomy       | Team retains decisions                                    |
      | Development    | Focus on team growth                                      |
      | Trust          | Confidential support                                      |
      | Availability   | Responsive but not dependent                              |

Feature: Network Learning Pattern
  Enable knowledge sharing across teams

  Scenario: Network Formation
    Given multiple teams exist
    When forming learning networks
    Then enable:
      | Connection     | Purpose                                                    |
      | Direct        | Team-to-team learning                                     |
      | Domain        | Practice area focus                                       |
      | Regional      | Geographic collaboration                                  |
      | Innovation    | New solution development                                  |
    And ensure:
      | Principle      | Implementation                                             |
      | Voluntary      | Teams choose connections                                  |
      | Practical     | Focus on working solutions                                |
      | Open          | Knowledge flows freely                                    |
      | Dynamic       | Networks evolve with needs                                |

Feature: Support Infrastructure Pattern
  Enable minimal but effective support systems

  Scenario: Support Service Definition
    Given teams need operational support
    When defining support services
    Then provide:
      | Service        | Purpose                                                    |
      | Admin         | Essential bureaucracy handling                             |
      | Tools         | Professional practice support                              |
      | Space         | Physical/virtual workspace                                 |
      | Learning      | Development resources                                      |
    And ensure:
      | Principle      | Implementation                                             |
      | Simplicity     | Minimal overhead                                          |
      | Enablement     | Enhances practice                                         |
      | Responsiveness | Quick resolution                                          |
      | Value         | Clear benefit to practice                                 |

Feature: Organization Integration Pattern
  Enable appropriate connection to larger organization

  Scenario: Organizational Relationship
    Given teams operate within larger contexts
    When defining organizational relationships
    Then establish:
      | Element        | Purpose                                                    |
      | Boundaries    | Clear team autonomy scope                                 |
      | Resources     | Fair resource agreements                                  |
      | Standards     | Minimal viable requirements                               |
      | Contribution  | Value sharing arrangements                                |
    And ensure:
      | Principle      | Implementation                                             |
      | Independence   | Team practice autonomy                                    |
      | Alignment      | Shared purpose clarity                                    |
      | Fairness      | Equitable exchanges                                       |
      | Growth        | Mutual development                                        |

Feature: Results Pattern
  Enable outcome focus without control

  Scenario: Results Tracking
    Given professional work occurs
    When tracking results
    Then measure:
      | Aspect         | Focus                                                      |
      | Client Value   | Outcome achievement                                       |
      | Quality        | Professional standards                                    |
      | Development    | Capability growth                                         |
      | Sustainability | Practice health                                           |
    And ensure:
      | Principle      | Implementation                                             |
      | Usefulness     | Metrics serve practice                                    |
      | Learning       | Focus on improvement                                      |
      | Wholeness      | Consider full impact                                      |
      | Simplicity     | Minimal measurement burden                                |
