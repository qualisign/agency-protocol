Feature: Core Organizational Agent
  Enable verifiable collective action with minimal structural assumptions

  Background:
    Given the Organization Agent exists
    And it has a content-based address
    And it can verify cryptographic signatures
    And it maintains an immutable record

  Scenario: Entity Registration
    Given an entity needs to be recognized
    When registering the entity
    Then record only:
      | Element         | Content                                                    |
      | Address        | Content-based identity                                     |
      | Type           | Entity classification (agent/group/resource)               |
      | Verifier       | How to validate entity actions                            |
    And ensure:
      | Requirement    | Implementation                                             |
      | Uniqueness     | Address is globally unique                                |
      | Verifiability  | Actions can be validated                                  |
      | Minimality     | No additional assumptions                                 |

  Scenario: Relationship Declaration
    Given entities exist
    When declaring a relationship
    Then record only:
      | Element         | Content                                                    |
      | Parties        | Participating entity addresses                             |
      | Type           | Nature of relationship                                     |
      | Evidence       | Proof of mutual acknowledgment                            |
    And enforce:
      | Rule           | Implementation                                             |
      | Consent        | All parties have signed                                   |
      | Independence   | No implicit dependencies                                  |
      | Mutability     | Can be modified by consent                               |

  Scenario: Activity Recording
    Given relationships exist
    When activity occurs
    Then record only:
      | Element         | Content                                                    |
      | Actor          | Entity address                                            |
      | Action         | What occurred                                             |
      | Context        | Relevant relationships                                    |
      | Evidence       | Cryptographic proof                                       |
    And verify:
      | Check          | Implementation                                             |
      | Identity       | Actor signature valid                                     |
      | Context        | Referenced relationships exist                            |
      | Proof          | Evidence is verifiable                                    |

Feature: Structure Evolution
  Enable structural change with minimal constraints

  Scenario: Structure Change
    Given a change is proposed
    When processing the change
    Then require only:
      | Element         | Content                                                    |
      | Affected       | Impacted relationships                                     |
      | Change         | What is different                                          |
      | Acknowledgment | Proof of relevant consent                                 |
    And ensure:
      | Rule           | Implementation                                             |
      | Sovereignty    | Changes require only affected parties                     |
      | Clarity        | Changes are unambiguous                                   |
      | History        | Changes are traceable                                     |

Feature: Extension Points
  Enable specialized organizational patterns

  Scenario: Pattern Implementation
    Given organizational patterns need implementation
    When extending the core
    Then provide:
      | Extension Point | Purpose                                                    |
      | Validation     | How actions are verified                                   |
      | Composition    | How entities combine                                       |
      | Evolution      | How changes occur                                          |
      | Integration    | How patterns connect                                       |
    And ensure:
      | Rule           | Implementation                                             |
      | Independence   | Patterns don't interfere                                  |
      | Composability  | Patterns can be combined                                  |
      | Verifiability  | Pattern outcomes are provable                            |
