Feature: Protocol Messages
  Message handling must be transport agnostic

  Scenario Outline: Message Verification
    Given a <message_type> message is sent
    When it is received
    Then its authenticity must be verifiable
    And its content must be integrity protected
    And its sender must be identifiable by content address

    Examples:
      | message_type     |
      | promise         |
      | fulfillment     |
      | merit update    |
      | name resolution |
      | credit stake    |

  Scenario Outline: Cross-Infrastructure Operation
    Given agents exist on <infrastructure_type>
    When they interact through the protocol
    Then all protocol operations must work correctly
    And all verifications must succeed
    And no protocol guarantees should be weakened

    Examples:
      | infrastructure_type |
      | cloud servers      |
      | blockchain         |
      | local network      |
      | offline storage    |
      | mesh network       |
