Feature: Bluesky Agent Name System (BANS)

  Background:
    Given agents use Bluesky to manage names and tags
    And tags can inherit from other tags to form hierarchies
    And namespaces are organized as hashtags for discoverability

  Scenario: Registering an agent with a name and tags
    Given "Agent A" posts the following to Bluesky:
      """
      #RegisterName Name:ServiceX Tags:service,category1,featureA
      """
    When users search for "ServiceX" in Bluesky
    Then "Agent A" is discoverable
    And associated with the tags "service", "category1", and "featureA"

  Scenario: Updating an agent's name and tags
    Given "Agent A" has registered with the name "ServiceX" and tags "service,category1,featureA"
    When "Agent A" posts an update:
      """
      #UpdateName OldName:ServiceX NewName:ServiceXPro AddTags:premium
      """
    Then the name changes to "ServiceXPro"
    And the tag "premium" is added to "Agent A"

  Scenario: Agents using hierarchical tags
    Given the following tag hierarchy is posted to Bluesky:
      """
      #TagHierarchy Parent:domain1 Subtags:category1
      #TagHierarchy Parent:category1 Subtags:featureA
      """
    And "Agent A" registers with the tag "featureA":
      """
      #RegisterName Name:AgentA Tags:featureA
      """
    When users search for agents with the tag "domain1"
    Then "Agent A" is included due to tag inheritance.

  Scenario: Searching for agents by name
    Given the following agents register on Bluesky:
      """
      #RegisterName Name:ServiceX Tags:service,category1,featureA
      #RegisterName Name:ServiceY Tags:service,category2,featureB
      """
    When a user searches for "ServiceX"
    Then "Agent A" is returned with its associated tags.

  Scenario: Searching for agents by tag
    Given agents register with these tags:
      """
      #RegisterName Name:ServiceX Tags:service,category1,featureA
      #RegisterName Name:ServiceY Tags:service,category2,featureB
      #RegisterName Name:ServiceZ Tags:utility,category1,featureC
      """
    When a user searches for agents with the tag "category1"
    Then the system returns:
      """
      Agent:ServiceX
      Agent:ServiceZ
      """
    And includes their tags.

  Scenario: Agents defining and using custom tags
    Given "Agent B" posts a custom tag:
      """
      #DefineTag Name:custom-feature Properties:Custom:true
      """
    And includes it in their registration:
      """
      #RegisterName Name:AgentB Tags:custom-feature
      """
    Then the tag "custom-feature" is recognized by other agents.

  Scenario: Updating tag definitions and propagating changes
    Given the tag "premium" is updated:
      """
      #UpdateTag Name:premium AddProperty:SupportLevel:24/7
      """
    And "Agent A" uses the tag "premium"
    Then "Agent A" inherits the property "SupportLevel:24/7"

  Scenario: Agents handling tag deprecation and updates
    Given the tag "legacy" is deprecated:
      """
      #DeprecateTag Name:legacy
      """
    And "Agent C" uses "legacy"
    Then "Agent C" receives a notification to update their tags:
      """
      #Notification Agent:AgentC Message:TagLegacyDeprecated
      """

  Scenario: Agents ensuring consistency in tag usage
    Given "Agent D" attempts to register:
      """
      #RegisterName Name:AgentD Tags:fast
      """
    And "fast" has multiple definitions
    Then "Agent D" is prompted to specify:
      """
      #ConflictResolution Tag:fast Options:Performance,Efficiency
      """

  Scenario: Agents resolving name conflicts in namespaces
    Given "Agent F" and "Agent G" both register with the name "CommonService":
      """
      #RegisterName Name:CommonService
      """
    When users search for "CommonService"
    Then a list of agents is returned for selection:
      """
      Agents:AgentF,AgentG
      """

  Scenario: Agents using namespaces for discoverability
    Given "Agent H" registers in the "Utilities" namespace:
      """
      #RegisterName Name:AgentH Namespace:Utilities Tags:utility
      """
    When users search in "Utilities"
    Then "Agent H" is discoverable.

  Scenario: Agents handling temporal changes in tags and names
    Given "Agent J" registers:
      """
      #RegisterName Name:ServiceBeta Tags:beta
      """
    When "Agent J" updates:
      """
      #UpdateName OldName:ServiceBeta NewName:ServiceStable RemoveTags:beta
      """
    Then users searching for "ServiceStable" find "Agent J"
    And users searching for "beta" do not.

  Scenario: Agents resolving namespaces with multiple inheritance
    Given "Agent K" registers with "multifunction":
      """
      #RegisterName Name:AgentK Tags:multifunction
      """
    And "multifunction" inherits:
      """
      #TagHierarchy Parent:function1,function2 Subtag:multifunction
      """
    When users search for "function1" or "function2"
    Then "Agent K" is included.

  Scenario: Agents specifying properties in tags
    Given "Agent M" uses:
      """
      #RegisterName Name:AgentM Tags:secure Property:Encryption:AES-256
      """
    And "Agent N" uses:
      """
      #RegisterName Name:AgentN Tags:secure Property:Encryption:RSA-2048
      """
    When users search for "secure" with "Encryption:AES-256"
    Then only "Agent M" is included.

  Scenario: Agents organizing tags into ontologies
    Given "Agent O" defines:
      """
      #TagHierarchy Parent:service Subtags:data
      #TagHierarchy Parent:data Subtags:analytics
      """
    When a user searches under "service > data"
    Then "Agent O" is included.

  Scenario: Agents using tags for versioning
    Given "Agent V" registers with:
      """
      #RegisterName Name:AgentV Tags:version:1.0
      """
    When "Agent V" updates:
      """
      #UpdateTag OldVersion:1.0 NewVersion:2.0
      """
    Then users find the updated version "2.0".

  Scenario: Agents managing tag properties and definitions
    Given "Agent R" uses:
      """
      #RegisterName Name:AgentR Tags:standard Properties:ISO:9001
      """
    When the tag "standard" is updated:
      """
      #UpdateTag Name:standard AddProperty:SupportLevel:24/7
      """
    Then "Agent R" reviews and applies changes.

  Scenario: Agents specifying localization in tags
    Given "Agent S" registers with:
      """
      #RegisterName Name:AgentS Tags:region:Europe
      """
    When users search for "region:Europe"
    Then "Agent S" is included.

  Scenario: Agents using the system for discovery
    Given agents register with names and tags
    When users search via names or tags
    Then Bluesky returns relevant agents with associated data:
      """
      #SearchResults Agents:AgentX,AgentY
      Tags:service,featureA
      """
