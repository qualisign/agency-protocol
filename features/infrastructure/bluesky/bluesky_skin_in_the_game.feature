Feature: Skin in the Game - Incentivizing Good-Faith Participation via Bluesky

  Background:
    Given a decentralized ledger records all agent actions immutably
    And agents have unique content-based addresses derived from their public keys
    And agents interact via Bluesky posts for promises, merit updates, and credit stakes
    And a merit service tracks and calculates relative and absolute merit scores
    And agents use Bluesky to stake credits and receive updates on their standing

  Scenario: Agent's promise fulfillment affects their normalized relative merit score
    Given agents "Agent A", "Agent B", and "Agent C" are being tracked in the "Data Sharing" domain
    And their initial records are posted via Bluesky:
      """
      #MeritUpdate Domain:DataSharing 
      Records:
      | Agent    | Promises Kept | Promises Made | Success Rate |
      | Agent A  | 15            | 23            | 0.652        |
      | Agent B  | 15            | 24            | 0.625        |
      | Agent C  | 10            | 24            | 0.417        |
      """
    And the merit service normalizes the success rates:
      """
      #MeritScores Domain:DataSharing
      Normalized Scores:
      | Agent    | Normalized Score |
      | Agent A  | 1.0              |
      | Agent B  | 0.857            |
      | Agent C  | 0.0              |
      """
    When "Agent A" fulfills a new promise and posts:
      """
      #PromiseFulfilled Agent:AgentA Domain:DataSharing
      New Success Rate: 0.667
      """
    Then the merit service recalculates scores:
      """
      #MeritScores Domain:DataSharing
      Updated Scores:
      | Agent    | Success Rate | Normalized Score |
      | Agent A  | 0.667        | 1.0              |
      | Agent B  | 0.625        | 0.714            |
      | Agent C  | 0.417        | 0.0              |
      """
    And "Agent A" maintains the highest normalized score.

  Scenario: Agent's failure to fulfill promises affects their normalized relative merit score
    Given "Agent B" posts a failed promise:
      """
      #PromiseFailed Agent:AgentB Domain:DataSharing
      New Success Rate: 0.600
      """
    When the merit service recalculates scores
    Then it posts updated results:
      """
      #MeritScores Domain:DataSharing
      Updated Scores:
      | Agent    | Success Rate | Normalized Score |
      | Agent A  | 0.667        | 1.0              |
      | Agent B  | 0.600        | 0.600            |
      | Agent C  | 0.417        | 0.0              |
      """
    And "Agent B"'s merit decreases.

  Scenario: Merit service combines normalized relative merits to produce absolute merit scores
    Given "Agent D" posts their domain-specific scores:
      """
      #DomainScores Agent:AgentD
      | Domain            | Success Rate | Normalized Score | Weight |
      | Data Sharing      | 0.750        | 0.857            | 0.5    |
      | Service Provision | 0.800        | 1.0              | 0.3    |
      | Product Review    | 0.600        | 0.667            | 0.2    |
      """
    When the merit service calculates their absolute score
    Then it posts:
      """
      #AbsoluteMerit Agent:AgentD
      Absolute Merit: 0.862
      """

  Scenario: Agents are incentivized to improve their performance via Bluesky
    Given agents post their understanding of how merit scores affect opportunities:
      """
      #MeritOpportunity Agent:AgentE
      Fulfilled Promises: 20, Normalized Merit: 0.85
      Opportunities: High-value tasks available
      """
    When "Agent E" considers making a promise
    Then they are incentivized to fulfill it successfully to improve their score.

  Scenario: Agent's credit is affected by staking on promises and outcomes
    Given "Agent F" posts their credit balance:
      """
      #CreditStatus Agent:AgentF
      Balance: 100
      """
    And stakes 10 credits on a promise:
      """
      #StakeCredits Agent:AgentF Amount:10 Promise:ProvideAnalysis
      """
    When they fulfill the promise:
      """
      #PromiseFulfilled Agent:AgentF
      EarnedCredits: 20
      """
    Then their balance updates:
      """
      #CreditStatus Agent:AgentF
      Balance: 120
      """
    And the merit service recalculates their score:
      """
      #MeritUpdate Agent:AgentF
      Updated Normalized Merit: 0.90
      """

  Scenario: Agent loses staked credit upon failing to fulfill a promise
    Given "Agent G" posts their credit balance:
      """
      #CreditStatus Agent:AgentG
      Balance: 80
      """
    And stakes 20 credits on a promise:
      """
      #StakeCredits Agent:AgentG Amount:20 Promise:DeliverProductUpdate
      """
    When they fail to fulfill it:
      """
      #PromiseFailed Agent:AgentG
      """
    Then their balance updates:
      """
      #CreditStatus Agent:AgentG
      Balance: 60
      """
    And their merit score decreases:
      """
      #MeritUpdate Agent:AgentG
      Updated Normalized Merit: 0.50
      """

  Scenario: Merit-based opportunities via Bluesky
    Given "Agent H" posts their scores:
      """
      #MeritScores Domain:Consulting
      | Agent    | Success Rate | Normalized Score |
      | Agent H  | 0.900        | 1.0              |
      | Agent I  | 0.750        | 0.625            |
      | Agent J  | 0.600        | 0.250            |
      """
    When a client posts a request:
      """
      #RequestForAgent Domain:Consulting Task:Critical
      """
    Then "Agent H" is selected due to the highest merit:
      """
      #TaskAwarded Agent:AgentH Task:Critical
      """

  Scenario: Agents dynamically adapt to merit recalculations
    Given "Agent K" posts their performance in "Design":
      """
      #MeritScores Domain:Design
      | Agent    | Success Rate | Normalized Score |
      | Agent K  | 0.700        | 0.777            |
      | Agent L  | 0.800        | 1.0              |
      | Agent M  | 0.500        | 0.333            |
      """
    When "Agent M" improves:
      """
      #PromiseFulfilled Agent:AgentM SuccessRate:0.750
      """
    Then the merit service recalculates scores:
      """
      #MeritScores Domain:Design
      | Agent    | Success Rate | Normalized Score |
      | Agent L  | 0.800        | 1.0              |
      | Agent M  | 0.750        | 0.833            |
      | Agent K  | 0.700        | 0.555            |
      """
