Feature: Bluesky Data as Labor
  Enable Agency Protocol's data sharing and control features using Bluesky posts and interactions.

  Background:
    Given an agent operates on the Bluesky network
    And data sharing agreements are expressed as structured posts
    And tags are used to manage data access and terms

  Scenario: Secure Data Sharing
    Given "Agent A" has encrypted data "Data X" stored securely
    When "Agent A" publishes a Bluesky post with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.share"               |
      | reference        | Encrypted URI for "Data X"          |
      | terms            | Usage terms for "Agent B"           |
    And "Agent A" sends the decryption key to "Agent B" privately
    Then "Agent B" accesses "Data X" as permitted
    And no unauthorized agent can access it.

  Scenario: Revoking Data Access
    Given "Agent A" has shared "Data Y" with "Agent B"
    When "Agent A" updates the post with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.revoke"              |
      | reference        | URI for "Data Y"                    |
    Then "Agent B" is notified of the revocation
    And further access to "Data Y" is denied.

  Scenario: Sharing Data Agreements via Bluesky Posts
    Given "Agent C" wants to share data "Data Z"
    When "Agent C" posts a data-sharing promise with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.promise"             |
      | terms            | Usage terms for "Data Z"            |
    And "Agent D" replies to accept the promise
    Then "Agent C" shares "Data Z" privately with "Agent D"
    And the interaction is recorded as a thread.

  Scenario: Data Usage Compliance
    Given "Agent E" specifies usage terms in a Bluesky promise
    When "Agent F" accesses shared data
    Then violations of the terms are flagged with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.flag"                |
      | details          | Description of the violation        |
    And "Agent F"'s reputation is impacted.

  Scenario: Cross-Application Data Portability
    Given "Agent G" shares data in a standard format via Bluesky
    When "Agent G" accesses the data via another Bluesky-compliant service
    Then the data is interoperable
    And "Agent G" retains control over its use.

  Scenario: Requesting Data Deletion
    Given "Agent H" has shared "Data W" with "Agent I"
    When "Agent H" posts a deletion request with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.delete"              |
      | reference        | URI for "Data W"                    |
    Then "Agent I" acknowledges the request via reply
    And deletes "Data W" and ceases further use.

  Scenario: Anonymized Data Sharing for Research
    Given "Agent L" wants to share anonymized data "Data Anon"
    When "Agent L" posts an offer with:
      | Field            | Content                              |
      | recordType       | "app.bsky.data.offer"               |
      | anonymized       | true                                |
    And "Agent M" replies to accept the offer
    Then "Agent L" shares the anonymized data
    And the terms are visible in the post thread.

  Scenario: Enhancing Data Discoverability
    Given "Agent T" includes metadata in their Bluesky posts
    When "Agent U" searches using relevant tags
    Then "Agent U" finds "Agent T"'s data posts
    And requests access through a structured reply.
