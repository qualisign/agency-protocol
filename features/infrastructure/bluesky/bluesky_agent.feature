Feature: Agency Protocol via Bluesky

  Scenario: Agent Creation
    Given a Bluesky user posts a message
      """
      #CreateAgent PublicKey:Base64EncodedPublicKey
      """
    When the system processes the message
    Then the agent has a content-based address derived from its base state
    And the agent's public key can be retrieved from this address
    And the system replies to the post with
      """
      ✅ Agent created! Address: [content-based-address]
      """

  Scenario: Registration with the Registry Service
    Given a Bluesky user posts
      """
      #RegisterAgent Address:[agent-address]
      """
    When the system verifies the agent's address
    Then a REGISTER event is emitted to announce the agent with address "X" is online
    And the system posts a public message
      """
      🔔 New Agent Registered: [agent-address]
      """
    And other subscribed services are notified

  Scenario: Updating the Agent's Promises
    Given a Bluesky user posts
      """
      #UpdateAgent Address:[old-address] Promises:[new-promise-content]
      """
    When the system verifies the signature of the request
    Then a new agent state is created
    And the system replies with
      """
      🔄 Agent Updated! New Address: [new-address]
      """
    And a forward event is emitted to redirect requests from the old address

  Scenario: Creating an Intention
    Given a Bluesky user posts
      """
      #CreateIntention Description:"float in water"
      """
    When the system processes the intention creation
    Then the intention has a unique content-based address
    And the system replies
      """
      ✅ Intention created: Address:[content-based-address]
      """

  Scenario Outline: Agent Making and Signing a Promise Based on an Intention
    Given a Bluesky user posts
      """
      #MakePromise IntentionAddress:<intention-address> Signature:[signature]
      """
    When the system verifies the signature and intention
    Then the promise has a unique content-based address
    And the system replies
      """
      🔐 Promise made for intention: [intention-address]
      """
    Examples:
      | intention-address |
      | float-in-water    |
      | deliver-messages  |

  Scenario: Verifying a Promise's Signature
    Given a Bluesky user posts
      """
      #VerifyPromise Address:[promise-address]
      """
    When the system verifies the signature
    Then the system replies
      """
      ✅ Verified! Signed by: [agent-public-key]
      """

  Scenario: Updating an Agent's Promises
    Given a Bluesky user posts
      """
      #UpdatePromise Address:[current-agent-address] NewPromise:[new-content]
      """
    When the system verifies the update
    Then the system replies
      """
      🔄 Promise updated! New Address: [new-agent-state-address]
      """

  Scenario: Tracking Changes to an Agent's Promises
    Given a Bluesky user posts
      """
      #GetHistory Address:[agent-address]
      """
    When the system retrieves the change history
    Then the system replies with the full history of promise updates

  Scenario: Agent's Promised Name Matches Expectations
    Given a Bluesky user posts
      """
      #SetName Name:[promised-name]
      """
    When other users interact with the agent using the name "[promised-name]"
    Then the agent recognizes and responds appropriately

  Scenario: Agent Changes Its Promised Name
    Given a Bluesky user posts
      """
      #UpdateName OldName:[old-name] NewName:[new-name]
      """
    When the system verifies the request
    Then the system replies
      """
      🔄 Name updated to: [new-name]
      """

  Scenario: Agent Authentication When Making a Promise
    Given a Bluesky user posts
      """
      #Authenticate AgentAddress:[agent-address] Promise:[promise-content] Signature:[signature]
      """
    When the system verifies the signature
    Then the system replies
      """
      ✅ Authentication successful!
      """

  Scenario: Preventing Duplicate or Replay Attacks
    Given a Bluesky user posts a duplicate promise
      """
      #MakePromise Address:[existing-promise-address]
      """
    When the system detects the duplicate
    Then the system replies
      """
      ❌ Duplicate promise rejected.
      """
