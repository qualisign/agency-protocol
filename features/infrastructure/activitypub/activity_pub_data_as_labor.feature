Feature: ActivityPub Data as Labor
  Implement Agency Protocol's data sharing and control features using ActivityPub.

  Background:
    Given an agent has an ActivityPub `Actor` object
    And the agent can share data using ActivityPub messages
    And data references and permissions are managed through ActivityPub-compatible metadata

  Scenario: Secure Data Sharing
    Given "Agent A" has data "Data X" encrypted and stored at a secure location
    When "Agent A" creates an `Offer` activity with:
      | Field            | Content                              |
      | type             | "Offer"                             |
      | object.type      | "DataAsset"                         |
      | object.reference | Encrypted URI for "Data X"          |
      | terms            | Usage terms for "Agent B"           |
    And "Agent A" shares the decryption key with "Agent B" securely
    Then "Agent B" receives the key and can access "Data X" under the specified terms.

  Scenario: Revoking Data Access
    Given "Agent A" has shared "Data Y" with "Agent B"
    When "Agent A" sends a `Revoke` activity with:
      | Field            | Content                              |
      | type             | "Revoke"                            |
      | object.reference | URI for "Data Y"                    |
      | target           | "Agent B"                           |
    Then "Agent B" is notified of the revocation
    And "Agent B" is expected to cease access and delete any local copies.

  Scenario: Sharing Data Agreements via Promises
    Given "Agent C" wants to share data "Data Z" under specific conditions
    When "Agent C" sends a `Create` activity with:
      | Field            | Content                              |
      | type             | "Create"                            |
      | object.type      | "Promise"                           |
      | object.terms     | Usage terms for data sharing         |
    And "Agent D" sends an `Accept` activity to agree to the promise
    Then "Agent C" shares "Data Z" securely with "Agent D"
    And both agents have a recorded agreement in their ActivityPub inboxes.

  Scenario: Data Usage Compliance
    Given "Agent E" specifies privacy terms in a promise
    When "Agent F" accesses data shared by "Agent E"
    Then "Agent F" must adhere to the specified terms
    And violations are recorded using a `Flag` activity.

  Scenario: Cross-Application Data Portability
    Given "Agent G" shares data using a standardized ActivityStreams format
    When "Agent G" accesses the data via a new ActivityPub-compliant application
    Then the application processes the data seamlessly
    And "Agent G" retains control over the data.

  Scenario: Requesting Data Deletion
    Given "Agent H" has shared "Data W" with "Agent I"
    When "Agent H" sends a `Delete` activity for "Data W" with:
      | Field            | Content                              |
      | type             | "Delete"                            |
      | object.reference | URI for "Data W"                    |
    Then "Agent I" acknowledges the request with an `Accept` activity
    And "Agent I" deletes "Data W" and ceases further use.

  Scenario: Anonymized Data Sharing for Research
    Given "Agent L" creates an anonymized dataset "Data Anon"
    When "Agent L" sends an `Offer` activity with:
      | Field            | Content                              |
      | type             | "Offer"                             |
      | object.type      | "DataAsset"                         |
      | anonymized       | true                                |
    And "Agent M" accepts the offer
    Then "Agent M" receives the anonymized dataset under agreed terms.

  Scenario: Enhancing Data Discoverability
    Given "Agent T" includes metadata for their data references
    When "Agent U" searches using specific criteria
    Then "Agent U" discovers "Agent T"'s data via ActivityPub feeds
    And requests access through a `Request` activity.
