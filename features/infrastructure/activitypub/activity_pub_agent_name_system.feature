Feature: ActivityPub Agent Name System
  Provide name and tag management for Agency Protocol agents using ActivityPub.

  Background:
    Given an Agency Protocol agent has registered on ActivityPub
    And namespaces are represented as hashtags
    And tags can inherit from other tags to form hierarchies

  Scenario: Registering an Agent with a Name and Tags
    Given "Agent A" publishes their `Actor` object with:
      | Field             | Content                             |
      | id                | ActivityPub URI                    |
      | preferredUsername | "ServiceX"                         |
      | tags              | "#service,#category1,#featureA"    |
    Then the name "ServiceX" is associated with "Agent A"
    And the tags "service, category1, featureA" are linked to "Agent A".

  Scenario: Updating Agent Tags and Names
    Given "Agent A" has tags "#service,#category1,#featureA"
    When "Agent A" updates their profile to include:
      | Field             | Content                             |
      | preferredUsername | "ServiceXPro"                      |
      | tags              | "#service,#premium"                |
    Then the name changes to "ServiceXPro"
    And the tag "premium" is added to the agent.

  Scenario: Searching for Agents by Name
    Given the following agents are registered:
      | Agent Name  | Tags                                |
      | ServiceX    | "#service,#category1,#featureA"     |
      | ServiceY    | "#service,#category2,#featureB"     |
    When a user searches for "ServiceX" on the ActivityPub network
    Then "Agent A" is returned along with associated tags.

  Scenario: Hierarchical Tag Inheritance
    Given a tag hierarchy is defined via ActivityPub:
      | Parent Tag    | Subtags                            |
      | #domain1      | #category1                        |
      | #category1    | #featureA                         |
    And "Agent A" uses the tag "#featureA"
    When users search for agents with "#domain1"
    Then "Agent A" is included in the results due to inheritance.

  Scenario: Resolving Name Conflicts
    Given "Agent B" and "Agent C" both register the name "CommonService"
    When a user searches for "CommonService"
    Then a list of matching agents is returned for selection:
      | Agent        | Tags                                 |
      | Agent B      | "#service,#category1"               |
      | Agent C      | "#utility,#featureX"                |
