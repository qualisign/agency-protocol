Feature: ActivityPub Actor Integration
  Map Agency Protocol agents to ActivityPub actors

  Background:
    Given an Agency Protocol agent exists
    And the agent has a content-based address
    And the agent can perform WebFinger discovery

  Scenario: Actor Profile Creation
    Given the agent needs an ActivityPub presence
    When creating an Actor object
    Then it should include:
      | Field          | Content                               |
      | type          | "Person"                              |
      | id            | Agent's ActivityPub URI               |
      | preferredUsername | Derived from agent identity       |
      | publicKey     | Agent's public key                    |
      | agencyProtocolAddress | Agent's content address       |
    And it should be discoverable via WebFinger
    And it should support ActivityPub following/followers

  Scenario: Cross-Protocol Identity Verification
    Given an agent has an ActivityPub presence
    When another entity verifies their identity
    Then they should be able to verify:
      | Aspect        | Method                                |
      | AP Identity  | WebFinger + Actor resolution          |
      | Agency Identity | Content address resolution          |
      | Linkage      | Cross-signed verification record      |
      | Current State | Protocol state verification          |

Feature: Promise Activity Translation
  Express Agency Protocol promises as ActivityPub activities

  Scenario: Promise Publication
    Given a new promise is made
    When publishing to ActivityPub
    Then it should create:
      | Activity Type | Content                               |
      | Create      | Promise object                         |
      | object.type | "Promise"                              |
      | target      | Relevant audiences                     |
    And the promise should be expressed in ActivityStreams vocabulary
    And include Agency Protocol verification metadata

  Scenario: Promise Fulfillment
    Given a published promise exists
    When it is fulfilled
    Then it should generate:
      | Activity Type | Content                               |
      | Complete     | Fulfillment object                    |
      | object.type | "Fulfillment"                          |
      | inReplyTo   | Original promise URI                   |
    And federation servers should forward the fulfillment
    And verification proofs should be included

Feature: Federation Support
  Enable seamless federation with ActivityPub networks

  Scenario: Content Federation
    Given a promise-related activity is published
    When federation occurs
    Then the content should be:
      | Requirement   | Implementation                        |
      | Addressable  | Have unique URI                       |
      | Signed       | Using AP and Agency Protocol keys     |
      | Verifiable   | Include cryptographic proofs          |
      | Discoverable | Via AP federation protocols           |

  Scenario: Promise Thread Federation
    Given a promise has updates or responses
    When federating the thread
    Then all related activities should be linked
    And chronological order should be maintained
    And cross-protocol verification should be possible

Feature: Merit Expression in ActivityPub
  Represent Agency Protocol merit in federated context

  Scenario: Merit Profile Attributes
    Given an agent has merit scores
    When expressing them in ActivityPub
    Then they should be included as:
      | Attribute Type | Format                               |
      | PropertyValue | Domain-specific scores               |
      | endorsement  | Verified merit proofs                |
      | capability   | Domain tags                          |

  Scenario: Merit Update Federation
    Given an agent's merit scores change
    When publishing updates
    Then they should be expressed as:
      | Activity Type | Content                               |
      | Update       | New merit scores                      |
      | object.type | "MeritScore"                          |
      | proof        | Verification chain                    |

Feature: Protocol-Aware Feed Generation
  Customize ActivityPub feeds based on Agency Protocol data

  Scenario: Merit-Weighted Feed Generation
    Given multiple agents publish content
    When generating feeds
    Then content should be ranked by:
      | Factor        | Weight                                |
      | Agent Merit  | High                                  |
      | Promise Relevance | High                            |
      | Network Distance | Medium                           |
      | Time Decay   | Low                                   |

  Scenario: Domain-Specific Feed Filtering
    Given content exists across domains
    When filtering feeds
    Then they should be filterable by:
      | Criteria      | Implementation                        |
      | Domain Tags  | Agency Protocol domains               |
      | Merit Threshold | Minimum merit scores               |
      | Promise Status | Fulfillment state                   |

Feature: Data Sharing via ActivityPub
  Enable secure data sharing through federation

  Scenario: Data Announcement
    Given an agent wants to share data
    When publishing availability
    Then it should create:
      | Activity Type | Content                               |
      | Offer        | Data sharing terms                    |
      | object.type | "DataAsset"                           |
      | access       | Agency Protocol controls              |

  Scenario: Data Access Request
    Given a data announcement exists
    When another actor requests access
    Then it should generate:
      | Activity Type | Content                               |
      | Request      | Access requirements                   |
      | object.type | "DataRequest"                         |
      | terms        | Proposed agreement                    |

Feature: Cross-Protocol Promise Interactions
  Enable promise-related interactions across protocols

  Scenario: Promise Acceptance via ActivityPub
    Given a promise is published
    When an ActivityPub actor accepts it
    Then it should create:
      | Activity Type | Content                               |
      | Accept       | Promise acceptance                    |
      | object      | Original promise                       |
      | terms        | Accepted conditions                   |

  Scenario: Promise Verification
    Given a promise interaction occurs via ActivityPub
    When verification is needed
    Then both protocols' verification should succeed
    And proofs should be available via either protocol
    And state should be consistent across protocols

Feature: Compliance and Moderation
  Handle cross-protocol moderation requirements

  Scenario: Content Moderation
    Given content exists across protocols
    When moderation is needed
    Then actions should be coordinated across:
      | Protocol     | Action                                |
      | ActivityPub | Standard moderation                   |
      | Agency      | Merit impact                          |
      | Federation  | Network propagation                   |

  Scenario: Moderation Impact Recording
    Given a moderation action occurs
    When recording impact
    Then it should affect:
      | Aspect       | Effect                                |
      | Merit Scores | Domain-specific adjustments           |
      | Visibility  | Federation scope                      |
      | Access      | Protocol-wide permissions             |

Feature: Service Discovery and Capability Advertisement
  Enable service discovery across federated networks

  Scenario: Service Advertisement
    Given an agent provides services
    When publishing to ActivityPub
    Then they should be discoverable via:
      | Method       | Implementation                        |
      | Tags        | Domain-specific capabilities          |
      | Collections | Service offerings                     |
      | Extensions  | Agency Protocol metadata              |

  Scenario: Service Status Updates
    Given a service state changes
    When updating availability
    Then it should publish:
      | Update Type  | Content                               |
      | Status      | Current service state                 |
      | capability  | Available features                    |
      | terms       | Usage conditions                      |

Feature: Federation-Enhanced Network Effects
  Leverage federation for protocol growth

  Scenario: Cross-Protocol Discovery
    Given activities occur on either protocol
    When discovery happens
    Then users should find:
      | Content Type | Discovery Path                        |
      | Promises    | Federation + Agency Protocol          |
      | Services    | Federated tags + merit scores         |
      | Data Assets | Controlled federation                 |

  Scenario: Network Growth Measurement
    Given cross-protocol activities occur
    When measuring growth
    Then metrics should track:
      | Metric       | Measurement                           |
      | Reach       | Federated instance coverage           |
      | Engagement  | Cross-protocol interactions           |
      | Value Flow  | Promise fulfillment across networks   |
      | Growth Rate | New agent activation                  |
