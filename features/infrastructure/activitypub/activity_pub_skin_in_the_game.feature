Feature: ActivityPub Skin in the Game
  Implement Agency Protocol's merit and credit system on ActivityPub.

  Background:
    Given agents have ActivityPub profiles
    And agents use ActivityPub messages for staking and fulfilling promises
    And the merit service tracks performance metrics

  Scenario: Merit-Driven Activity Streams
    Given agents publish promises and fulfillments
    When generating an ActivityPub feed
    Then it should prioritize activities based on:
      | Factor            | Weight                              |
      | Merit Score       | High                               |
      | Promise Relevance | High                               |
      | Domain Tags       | Medium                             |
      | Time Decay        | Low                                |

  Scenario: Staking Credit on a Promise
    Given "Agent A" stakes 10 credits on a promise
    When publishing the stake via ActivityPub
    Then it should generate an `Offer` activity:
      | Field             | Content                             |
      | type              | "Offer"                            |
      | object            | Promise URI                        |
      | amount            | "10 credits"                       |
    And it should be verifiable via the merit service.

  Scenario: Credit Loss on Promise Failure
    Given "Agent B" stakes 20 credits on a promise
    When "Agent B" fails to fulfill the promise
    Then a `Fail` activity is generated:
      | Field             | Content                             |
      | type              | "Fail"                             |
      | object            | Promise URI                        |
      | lostCredits       | "20 credits"                       |
    And the merit service updates "Agent B"'s scores accordingly.

  Scenario: Merit Score Updates
    Given "Agent C" successfully fulfills a promise
    When the merit service updates their scores
    Then an `Update` activity is published with:
      | Field             | Content                             |
      | type              | "Update"                           |
      | object            | "MeritScore"                       |
      | scoreDelta        | "+0.1"                             |
      | reason            | "Promise Fulfillment"              |

  Scenario: Incentivizing High-Merit Agents
    Given "Agent D" has the highest merit in "Data Sharing"
    When "Agent D" publishes new content
    Then their activities are promoted in federated feeds
    And other agents are notified of "Agent D"'s contributions.
