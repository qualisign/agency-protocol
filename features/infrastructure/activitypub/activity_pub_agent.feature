Feature: ActivityPub Agent Integration
  Support Agency Protocol agents on the ActivityPub network.

  Background:
    Given an Agency Protocol agent exists
    And the agent has a content-based address
    And the agent can send and receive ActivityPub messages

  Scenario: Publishing an Agent's Public Profile
    Given an agent "Agent A" wants to create an ActivityPub profile
    When "Agent A" publishes an `Actor` object
    Then the `Actor` object should include:
      | Field             | Content                             |
      | id                | Agent's ActivityPub URI             |
      | type              | "Service" or "Person"              |
      | preferredUsername | Derived from the agent's promises  |
      | publicKey         | Agent's public key                 |
      | inbox             | Inbox URL for receiving messages   |
      | outbox            | Outbox URL for sending messages    |
    And the profile should be discoverable via WebFinger
    And available for ActivityPub interactions.

  Scenario: Verifying Cross-Protocol Linkage
    Given "Agent A" has an ActivityPub profile and Agency Protocol state
    When "Agent B" checks their identity
    Then verification should confirm:
      | Aspect            | Method                              |
      | AP Identity       | WebFinger + `Actor` resolution     |
      | Agency Identity   | Content address lookup             |
      | Cross-Signature   | Shared verification record         |

  Scenario: Receiving ActivityPub Messages
    Given "Agent A" has a registered ActivityPub inbox
    When another agent sends a message to the inbox
    Then the message should:
      | Property          | Requirement                        |
      | Signature         | Be verifiable using public key     |
      | Content           | Match ActivityStreams vocabulary   |
      | Timestamp         | Be valid within time bounds        |

  Scenario: Forwarding Agency Protocol Updates
    Given "Agent A" updates their promises
    When the update is shared via ActivityPub
    Then it should generate an `Update` activity
    And reference the previous agent state in the message.

  Scenario: Federation Compliance
    Given "Agent A" interacts with federated ActivityPub instances
    When publishing content
    Then it should:
      | Requirement       | Implementation                     |
      | Addressable       | Have a unique ActivityPub URI      |
      | Signed            | Use AP-compliant signatures        |
      | Discoverable      | Via federation protocols           |
      | Compatible        | Maintain ActivityPub formatting    |
