Feature: Matrix Identity Bridge
  Map Agency Protocol identity to Matrix identity system

  Background:
    Given an Agency Protocol agent exists
    And the agent has a content-based address
    And Matrix homeserver federation is available

  Scenario: Agent Identity Registration
    Given the agent needs a Matrix presence
    When registering with a homeserver
    Then it should create:
      | Element          | Content                               |
      | User ID         | @agent:homeserver.tld                 |
      | Device ID       | Derived from agent state              |
      | Cross-signing   | Agency Protocol keys                  |
    And the identity should be verifiable across protocols
    And the agent's Matrix presence should reference its Agency Protocol address

  Scenario: Identity Verification
    Given an agent has a Matrix presence
    When another entity verifies their identity
    Then they should verify through:
      | Method          | Verification                          |
      | Matrix keys    | Device and cross-signing keys         |
      | Agency keys    | Protocol signature chain              |
      | Linkage        | Cross-protocol verification           |
      | State sync     | Matrix state events                   |

Feature: Promise Rooms
  Represent Agency Protocol promises as Matrix rooms

  Scenario: Promise Room Creation
    Given a new promise is made
    When creating a Matrix room
    Then it should configure:
      | Setting         | Value                                 |
      | Room type      | "m.promise"                          |
      | State content  | Promise details                       |
      | History        | Visible to members                    |
      | Encryption     | Enabled                               |
    And include Agency Protocol verification metadata
    And set appropriate power levels for promise participants

  Scenario: Promise State Events
    Given a promise room exists
    When the promise state changes
    Then it should emit:
      | Event Type     | Content                               |
      | m.promise.state| New promise state                    |
      | m.promise.proof| Verification chain                   |
      | m.room.message | Human readable updates               |

  Scenario: Promise Fulfillment
    Given a promise room is active
    When the promise is fulfilled
    Then it should:
      | Action         | Implementation                        |
      | Update state  | Final promise state                   |
      | Add proof     | Fulfillment verification              |
      | Archive room  | Set appropriate history visibility    |
      | Notify        | All room participants                 |

Feature: Merit Space
  Represent Agency Protocol merit system in Matrix spaces

  Scenario: Domain Merit Space Structure
    Given an agent operates in multiple domains
    When organizing merit information
    Then create a space structure:
      | Level          | Content                               |
      | Root space    | Agent's overall merit                 |
      | Sub-spaces    | Domain-specific merit                 |
      | Rooms         | Promise history                       |
      | State events  | Merit scores and proofs               |

  Scenario: Merit Score Updates
    Given merit scores change
    When updating the merit space
    Then emit state events:
      | Event Type     | Content                               |
      | m.merit.score | Updated scores                        |
      | m.merit.proof | Verification chain                    |
      | m.merit.domain| Domain-specific metrics               |

Feature: Data Sharing via Matrix
  Enable secure data sharing through Matrix rooms

  Scenario: Data Room Setup
    Given an agent wants to share data
    When creating a data sharing room
    Then configure:
      | Setting         | Value                                 |
      | Room type      | "m.data.share"                       |
      | Encryption     | Required                              |
      | Access         | Invite only                           |
      | History        | Shared post-join                      |

  Scenario: Data Access Control
    Given a data sharing room exists
    When managing access
    Then implement:
      | Control         | Method                                |
      | Invites        | Verified recipients only              |
      | Key sharing    | Based on promise fulfillment          |
      | Revocation     | Key rotation on access change         |

Feature: State Synchronization
  Maintain Agency Protocol state through Matrix

  Scenario: State Event Chain
    Given agent state changes occur
    When synchronizing state
    Then emit events:
      | Event Type     | Content                               |
      | m.agent.state | New agent state                      |
      | m.agent.proof | State transition proof                |
      | m.agent.refs  | Related promise rooms                 |

  Scenario: Federation Consistency
    Given state events are federated
    When homeservers synchronize
    Then ensure:
      | Aspect         | Requirement                           |
      | Ordering      | Consistent event chain                |
      | Verification  | Cryptographic proof chain             |
      | Availability  | Multi-homeserver redundancy           |

Feature: Promise Interaction Rooms
  Enable promise-related interactions through Matrix

  Scenario: Negotiation Room
    Given a promise is being negotiated
    When setting up the interaction
    Then create:
      | Element        | Purpose                               |
      | Main room     | Promise discussion                    |
      | Thread        | Term negotiation                      |
      | State events  | Agreement tracking                    |
      | Widgets       | Interaction tools                     |

  Scenario: Multi-Party Promise Coordination
    Given multiple parties are involved
    When coordinating promise fulfillment
    Then structure:
      | Component      | Implementation                        |
      | Sub-rooms     | Party-specific coordination           |
      | Space         | Overall promise organization          |
      | Bridges       | External protocol integration         |

Feature: Service Discovery
  Enable service discovery through Matrix

  Scenario: Service Advertisement
    Given an agent provides services
    When publishing availability
    Then create:
      | Element        | Content                               |
      | Public room   | Service directory                     |
      | State events  | Service capabilities                  |
      | Space         | Service organization                  |

  Scenario: Capability Discovery
    Given services are advertised
    When discovering capabilities
    Then provide:
      | Info Type      | Content                               |
      | Domain tags   | Service categories                    |
      | Merit scores  | Service quality metrics               |
      | Availability  | Current status                        |

Feature: Matrix Bridging
  Enable cross-protocol interactions via Matrix bridges

  Scenario: Protocol Bridge Setup
    Given interaction with other protocols is needed
    When setting up bridges
    Then configure:
      | Bridge Type    | Purpose                               |
      | ActivityPub   | Social federation                     |
      | XMPP          | Chat federation                       |
      | Email         | Notification delivery                 |

  Scenario: Bridge State Maintenance
    Given bridges are active
    When maintaining state
    Then ensure:
      | Aspect         | Implementation                        |
      | Identity      | Cross-protocol verification           |
      | Content       | Protocol-appropriate translation      |
      | State         | Consistent synchronization            |

Feature: Security and Privacy
  Implement Agency Protocol security through Matrix

  Scenario: End-to-End Encryption
    Given sensitive information is shared
    When implementing encryption
    Then use:
      | Method         | Implementation                        |
      | Olm           | 1:1 encryption                        |
      | Megolm        | Room encryption                       |
      | Cross-signing | Identity verification                 |

  Scenario: Access Control
    Given protected resources exist
    When managing access
    Then implement:
      | Control        | Method                                |
      | Room access   | Invite + promise fulfillment          |
      | Key sharing   | Merit-based authorization             |
      | Audit         | Event logging                         |

Feature: Network Effect Utilization
  Leverage Matrix network for protocol growth

  Scenario: Cross-Protocol Discovery
    Given Matrix federation exists
    When enabling discovery
    Then provide:
      | Discovery Type | Method                                |
      | Users         | Matrix user directory                 |
      | Services      | Public rooms + spaces                 |
      | Promises      | Room discovery                        |

  Scenario: Network Growth
    Given the network expands
    When measuring growth
    Then track:
      | Metric         | Measurement                           |
      | Users         | Active agents                         |
      | Promises      | Room creation/fulfillment             |
      | Merit flow    | Score propagation                     |
      | Federation    | Homeserver participation              |
