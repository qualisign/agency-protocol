Feature: Content-Addressed State Storage
  Implement reliable storage and retrieval of agent states and promises using content addressing

  Background: 
    Given an S3 bucket exists for content storage with versioning enabled
    And a DynamoDB table "ContentAddresses" exists with schema:
      | Field           | Type      | Purpose                                          |
      | ContentId      | String    | Hash of content (partition key)                  |
      | ContentType    | String    | Type of content (agent_state/promise/etc)        |
      | CreatedAt      | Number    | Timestamp of creation                            |
      | Size           | Number    | Content size in bytes                            |
      | References     | StringSet | Content addresses referenced by this content     |
      | AccessPattern  | String    | Expected access pattern (hot/warm/cold)          |
    And appropriate IAM roles and policies are configured
    And AWS KMS is configured for encryption

  Scenario: Store New Content
    Given content has been hashed to generate its content address
    When storing the content
    Then:
      | Step              | Implementation                                              |
      | Hash verification | Calculate hash of content to verify content address         |
      | Deduplication    | Check DynamoDB for existing content with this hash         |
      | Storage          | If not exists, store in S3 with content address as key     |
      | Metadata         | Create DynamoDB entry with metadata and references         |
      | Encryption       | Use KMS for envelope encryption of sensitive content       |
    And ensure:
      | Requirement      | Implementation                                              |
      | Durability      | Enable S3 versioning                                       |
      | Consistency     | Use strong consistency for DynamoDB reads                   |
      | Performance     | Configure S3 transfer acceleration for large content        |

  Scenario: Retrieve Content
    Given a content address exists
    When retrieving content
    Then follow access pattern:
      | Step              | Service    | Action                                         |
      | Metadata lookup   | DynamoDB   | Query by ContentId with strong consistency    |
      | Access pattern    | Lambda     | Check if content should be cached             |
      | Content fetch     | S3         | Get object using content address              |
      | Verification      | Lambda     | Verify content hash matches address           |
    And handle errors:
      | Error Case        | Action                                                     |
      | Not found        | Return 404 with clear error message                        |
      | Corrupted        | Attempt retrieval from backup region                       |
      | Access denied    | Return 403 with appropriate context                        |

Feature: Promise Event Stream
  Implement reliable, ordered processing of promise events

  Background:
    Given a Kinesis Data Stream "PromiseEvents" exists with appropriate sharding
    And a DynamoDB table "Promises" exists with schema:
      | Field           | Type      | Purpose                                          |
      | PromiseId      | String    | Content address of promise (partition key)       |
      | AgentId        | String    | Content address of promising agent               |
      | Status         | String    | Current promise status                           |
      | CreatedAt      | Number    | Promise creation timestamp                       |
      | UpdatedAt      | Number    | Last status update timestamp                     |
      | Terms          | Map       | Promise terms and conditions                     |
      | Evidence       | StringSet | Content addresses of fulfillment evidence        |
    And Lambda functions are configured for event processing
    And appropriate CloudWatch alarms are set

  Scenario: Process Promise Event
    Given a promise event occurs
    When processing the event
    Then execute pipeline:
      | Stage             | Service    | Implementation                                 |
      | Event validation  | Lambda     | Verify event signature and format             |
      | State update     | DynamoDB   | Update promise status with optimistic lock    |
      | Merit calculation | Lambda     | Update merit scores based on event            |
      | Notification     | SNS        | Notify interested parties                     |
    And ensure:
      | Requirement      | Implementation                                              |
      | Ordering        | Use Kinesis sequence numbers                               |
      | Exactly once    | Implement idempotency tokens                               |
      | Backpressure    | Monitor and adjust Kinesis shards                          |

  Scenario: Promise Query
    Given a need to verify promise history
    When querying promises
    Then support queries:
      | Query Type       | Implementation                                              |
      | By agent        | DynamoDB GSI on AgentId                                    |
      | By status       | DynamoDB GSI on Status + UpdatedAt                         |
      | By time range   | DynamoDB query on CreateAt/UpdatedAt                       |
      | With evidence   | DynamoDB query with evidence content addresses             |
    And ensure performance:
      | Pattern          | Implementation                                              |
      | Hot queries     | DAX caching for frequent queries                           |
      | Analytics       | Periodic snapshots to S3 for analysis                      |
      | Pagination      | Implement efficient cursor-based pagination                |

Feature: Merit Score Calculation
  Implement reliable merit score calculation and querying

  Background:
    Given a DynamoDB table "MeritScores" exists with schema:
      | Field           | Type      | Purpose                                          |
      | AgentId        | String    | Content address of agent (partition key)         |
      | DomainId       | String    | Merit domain (sort key)                         |
      | Score          | Number    | Current merit score                              |
      | UpdatedAt      | Number    | Last calculation timestamp                       |
      | Evidence       | StringSet | Promise IDs contributing to score                |
      | Confidence     | Number    | Score confidence factor                          |
    And appropriate Lambda functions exist for calculation
    And CloudWatch metrics are configured for monitoring

  Scenario: Calculate Merit Score
    Given a promise event affects merit
    When calculating new score
    Then execute:
      | Step              | Implementation                                              |
      | Evidence gather   | Collect all relevant promise events                        |
      | Score calc       | Apply domain-specific calculation rules                    |
      | Confidence calc  | Determine score confidence based on evidence               |
      | State update     | Update DynamoDB with new score and evidence               |
    And ensure:
      | Requirement      | Implementation                                              |
      | Consistency     | Use transactions for related updates                        |
      | Verification    | Store calculation basis with score                         |
      | Performance     | Implement incremental updates where possible               |

  Scenario: Query Merit Scores
    Given need to verify agent merit
    When querying scores
    Then support:
      | Query Type       | Implementation                                              |
      | Current score   | Direct DynamoDB query with caching                         |
      | Score history   | Maintain score changelog in S3                             |
      | Domain ranking  | GSI on DomainId + Score for ranking                       |
      | Evidence chain  | Link to contributing promises                              |
    And optimize:
      | Pattern          | Implementation                                              |
      | Hot queries     | DAX caching with TTL                                       |
      | Cold queries    | S3 analytics queries                                       |
      | Batch queries   | Efficient BatchGetItem operations                          |
