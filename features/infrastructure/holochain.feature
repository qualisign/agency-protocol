Feature: Holochain DNA Implementation
  Define the core DNA for Agency Protocol operations

  Background:
    Given a Holochain conductor is running
    And agent key material is initialized
    And DNA is installed

  Scenario: DNA Architecture
    Given the Agency Protocol needs implementation
    When designing the DNA structure
    Then create zomes for:
      | Zome            | Responsibility                         |
      | Identity       | Agent identity and verification        |
      | Promises       | Promise creation and management        |
      | Merit          | Merit calculation and tracking         |
      | Data          | Data asset management                  |
      | Directory     | Service discovery                      |
    And define entry types for each zome
    And implement validation rules

  Scenario: Entry Type Definitions
    Given the DNA needs entry types
    When defining entries
    Then implement:
      | Entry Type     | Structure                              |
      | agent_state   | Agent metadata and current state       |
      | promise       | Promise terms and conditions           |
      | fulfillment   | Promise completion evidence            |
      | merit_score   | Domain-specific merit data             |
      | data_asset    | Shared data references                 |

Feature: Agent Source Chain
  Implement agent identity using Holochain source chains

  Scenario: Agent State Management
    Given an agent needs to manage state
    When creating source chain entries
    Then record:
      | Entry Type     | Content                                |
      | init          | Initial agent state                    |
      | state_update  | State transitions                      |
      | promises      | Promise commitments                    |
      | fulfillments  | Promise completions                    |

  Scenario: State Validation
    Given source chain entries exist
    When validating state changes
    Then verify:
      | Aspect         | Validation Rule                        |
      | Signatures    | Cryptographic verification             |
      | Sequence      | Proper chain ordering                  |
      | References    | Valid entry references                 |
      | Schema        | Entry type conformance                 |

Feature: Promise Implementation
  Represent promises using Holochain entries and links

  Scenario: Promise Creation
    Given an agent wants to make a promise
    When creating the promise
    Then:
      | Action         | Implementation                         |
      | Create entry  | Promise entry with terms               |
      | Create links  | Link to relevant contexts              |
      | Emit signal   | Notify interested parties              |
      | Update state  | Record in agent state                  |

  Scenario: Promise Validation Rules
    Given a promise entry is created
    When validating the entry
    Then check:
      | Rule           | Verification                           |
      | Schema        | Entry format compliance                |
      | Signatures    | Author verification                    |
      | References    | Valid context links                    |
      | Terms         | Well-formed conditions                 |

Feature: Merit System
  Implement merit calculation using Holochain DHT

  Scenario: Merit Score Publishing
    Given merit scores need updating
    When publishing new scores
    Then:
      | Step           | Implementation                         |
      | Create entry  | Merit score data                      |
      | Link proofs   | Evidence references                    |
      | Update DHT    | Distribute to network                  |
      | Emit signal   | Notify interested parties              |

  Scenario: Merit Calculation
    Given promise fulfillment data exists
    When calculating merit
    Then:
      | Operation      | Method                                 |
      | Data gather   | Query relevant DHT entries             |
      | Calculation   | Apply merit formulas                   |
      | Verification  | Check all proofs                       |
      | Publication   | Create merit score entry               |

Feature: Data Sharing
  Implement secure data sharing via Holochain capabilities

  Scenario: Data Asset Creation
    Given data needs to be shared
    When creating data asset
    Then:
      | Component      | Implementation                         |
      | Entry         | Data asset metadata                    |
      | Capabilities  | Access control rules                   |
      | Links         | Context associations                   |
      | Validation    | Access verification rules              |

  Scenario: Access Control
    Given a data asset exists
    When managing access
    Then implement:
      | Control        | Method                                 |
      | Capabilities  | Granular access rights                 |
      | Validation    | Access rule checking                   |
      | Revocation    | Capability revocation                  |

Feature: DHT-Based Service Discovery
  Enable service discovery using Holochain DHT

  Scenario: Service Advertisement
    Given an agent provides services
    When publishing service info
    Then create:
      | Element        | Content                                |
      | Entry         | Service capabilities                   |
      | Links         | Domain associations                    |
      | Tags          | Discovery metadata                     |
      | Validation    | Service verification rules             |

  Scenario: Service Discovery
    Given services are published
    When discovering services
    Then enable:
      | Method         | Implementation                         |
      | Query         | DHT search patterns                    |
      | Filter        | Capability matching                    |
      | Verify        | Service validation                     |
      | Rank          | Merit-based ordering                   |

Feature: Network Bridges
  Enable interaction with external networks

  Scenario: Bridge Configuration
    Given external network interaction is needed
    When setting up bridges
    Then implement:
      | Bridge Type    | Purpose                                |
      | Websocket     | Real-time updates                      |
      | HTTP          | RESTful interfaces                     |
      | Custom        | Protocol-specific bridges              |

  Scenario: Bridge Security
    Given bridges are active
    When handling cross-network communication
    Then ensure:
      | Aspect         | Implementation                         |
      | Authentication| Capability-based auth                  |
      | Validation    | Cross-network verification             |
      | Rate limiting | Traffic management                     |

Feature: Validation Rules
  Implement comprehensive validation for all operations

  Scenario: Entry Validation
    Given entries are created
    When validating
    Then check:
      | Validation     | Rule                                   |
      | Schema        | Entry type compliance                  |
      | Authorization | Creator permissions                     |
      | References    | Valid entry links                      |
      | Logic         | Business rule compliance               |

  Scenario: Link Validation
    Given links are created
    When validating
    Then verify:
      | Aspect         | Check                                  |
      | Base          | Valid base entry                       |
      | Target        | Valid target entry                     |
      | Tag           | Appropriate link type                  |
      | Authorization | Link creation rights                   |

Feature: Signal System
  Implement real-time updates using Holochain signals

  Scenario: Event Notification
    Given protocol events occur
    When notifying interested parties
    Then emit:
      | Signal Type    | Content                                |
      | Promise       | Promise updates                        |
      | Fulfillment   | Completion evidence                    |
      | Merit         | Score changes                          |
      | Data          | Access changes                         |

  Scenario: Signal Handling
    Given signals are emitted
    When processing updates
    Then:
      | Action         | Implementation                         |
      | Validate      | Signal authenticity                    |
      | Process       | Update local state                     |
      | Respond       | Send acknowledgment                    |
      | Archive       | Record in source chain                 |

Feature: Resilience Patterns
  Implement robust operation patterns

  Scenario: Network Partition Handling
    Given network partitions occur
    When operating under partition
    Then:
      | Strategy       | Implementation                         |
      | Local ops     | Continue local operations              |
      | Queue updates | Store for later sync                   |
      | Validate      | Local consistency checks               |
      | Merge         | Partition resolution rules             |

  Scenario: Consistency Recovery
    Given inconsistencies are detected
    When recovering consistency
    Then implement:
      | Step           | Method                                 |
      | Detection     | Consistency checking                   |
      | Resolution    | Conflict resolution rules              |
      | Repair        | State correction procedures            |
      | Verification  | Consistency confirmation               |
