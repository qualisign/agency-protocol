Feature: IPFS-Native Agent Identity
  Leverage IPFS's content addressing for agent identity

  Background:
    Given an IPFS node is running
    And the agent has IPNS capability
    And libp2p identity is established

  Scenario: Agent State Publication
    Given an agent has a state to publish
    When publishing to IPFS
    Then create:
      | Component      | Implementation                        |
      | State DAG     | IPLD-encoded agent state             |
      | IPNS record   | Points to latest state CID           |
      | DHT record    | Helps locate agent state             |
    And the state should be retrievable via its CID
    And IPNS should resolve to the latest state

  Scenario: Agent Identity Resolution
    Given an agent's IPNS name exists
    When resolving the agent's current state
    Then:
      | Step           | Action                                |
      | Resolve IPNS  | Get latest state CID                  |
      | Fetch DAG     | Retrieve complete state               |
      | Verify        | Check signatures and proofs           |
      | Cache         | Pin important state objects           |

Feature: Promise DAG Structure
  Represent promises using IPLD DAGs

  Scenario: Promise Creation
    Given a new promise is being made
    When creating the promise structure
    Then build IPLD DAG:
      | Node Type      | Content                               |
      | Root          | Promise metadata + signatures         |
      | Terms         | Promise terms and conditions          |
      | State         | Current promise state                 |
      | Evidence      | Linked proof objects                  |
    And publish DAG to IPFS network
    And update agent state to reference promise

  Scenario: Promise Chain Maintenance
    Given a promise has updates
    When the state changes
    Then:
      | Action         | Implementation                        |
      | Create node   | New state IPLD node                  |
      | Link history  | Reference previous states            |
      | Update root   | New promise root CID                 |
      | Publish      | Push to IPFS network                 |

Feature: PubSub Communication
  Enable real-time protocol communication via libp2p pubsub

  Scenario: Promise Event Publication
    Given a promise-related event occurs
    When publishing the event
    Then:
      | Step           | Action                                |
      | Topic create  | Promise-specific pubsub topic         |
      | Message sign  | Sign with agent key                   |
      | Publish      | Send to pubsub network                |
      | Store DAG     | Archive in IPLD structure             |

  Scenario: Event Subscription
    Given an agent monitors promises
    When subscribing to updates
    Then:
      | Action         | Implementation                        |
      | Subscribe     | Relevant pubsub topics                |
      | Verify        | Message signatures                    |
      | Process       | Update local state                    |
      | Archive       | Store in local IPLD DAG               |

Feature: Merit Score Distribution
  Implement merit system using IPLD and pubsub

  Scenario: Merit Score Publication
    Given merit scores need updating
    When publishing new scores
    Then create:
      | Component      | Content                               |
      | Score DAG     | IPLD-encoded merit data              |
      | Proofs        | Linked verification chain             |
      | References    | Promise fulfillment evidence          |
    And broadcast updates via pubsub
    And maintain score history in IPLD structure

  Scenario: Merit Score Verification
    Given published merit scores exist
    When verifying scores
    Then traverse:
      | Element        | Verification                          |
      | Score DAG     | Complete IPLD structure              |
      | Proof chain   | Cryptographic verification           |
      | References    | Promise fulfillment checks           |

Feature: Data Sharing via IPFS
  Enable secure data sharing using IPFS capabilities

  Scenario: Encrypted Data Publication
    Given data needs to be shared
    When publishing to IPFS
    Then:
      | Step           | Implementation                        |
      | Encrypt       | Recipient-specific encryption         |
      | Store DAG     | IPLD structure with access info      |
      | Share key     | Secure key distribution               |
      | Pin data      | Ensure availability                   |

  Scenario: Access Control
    Given shared data exists on IPFS
    When managing access
    Then implement:
      | Control        | Method                                |
      | Encryption    | Per-recipient keys                    |
      | Key rotation  | For access revocation                 |
      | Availability  | Strategic pinning                     |

Feature: IPLD-Based State Management
  Use IPLD for efficient state representation

  Scenario: State DAG Structure
    Given agent state needs organization
    When creating IPLD structure
    Then implement:
      | Level          | Content                               |
      | Root          | Agent metadata + current state        |
      | Promises      | Active and historical promises        |
      | Merit         | Merit scores and history              |
      | Relations     | Links to other agents                 |

  Scenario: State Traversal
    Given complex state exists
    When traversing state
    Then enable:
      | Operation      | Implementation                        |
      | Navigation    | IPLD path traversal                  |
      | Verification  | Merkle proof generation              |
      | Resolution    | CID-based dereferencing              |

Feature: Content Discovery
  Enable service and promise discovery via IPFS

  Scenario: DHT Content Advertisement
    Given discoverable content exists
    When publishing to DHT
    Then provide:
      | Record Type    | Content                               |
      | Content path  | IPFS paths to resources              |
      | Metadata      | Search-relevant information          |
      | Availability  | Provider records                     |

  Scenario: Search Index Maintenance
    Given content needs to be searchable
    When maintaining search index
    Then create:
      | Component      | Implementation                        |
      | Index DAG     | IPLD-based search structure          |
      | Provider info | Network locations                    |
      | Metadata      | Search optimization data             |

Feature: IPFS Cluster Integration
  Enable robust content availability

  Scenario: Strategic Content Pinning
    Given important content exists
    When ensuring availability
    Then implement:
      | Strategy       | Method                                |
      | Priority      | Content importance ranking            |
      | Replication   | Minimum peer count                    |
      | Distribution  | Geographic distribution               |

  Scenario: Cluster Coordination
    Given multiple IPFS nodes exist
    When coordinating content
    Then manage:
      | Aspect         | Implementation                        |
      | Pinning       | Coordinated content pinning           |
      | Rebalancing   | Load distribution                     |
      | Recovery      | Content recovery procedures           |

Feature: Network Resilience
  Ensure protocol operation in various network conditions

  Scenario: Offline Operation
    Given network connectivity is limited
    When operating offline
    Then maintain:
      | Capability     | Method                                |
      | State access  | Local IPLD DAG                       |
      | Verification  | Offline proof checking               |
      | Updates       | Queue for later sync                  |

  Scenario: Network Sync
    Given offline operations occurred
    When reconnecting
    Then perform:
      | Action         | Implementation                        |
      | State sync    | DAG difference resolution            |
      | Publication   | Queued update processing             |
      | Verification  | Consistency checking                 |

Feature: Protocol Evolution Support
  Enable protocol upgrades and versioning

  Scenario: Protocol Version Management
    Given protocol versions exist
    When managing compatibility
    Then implement:
      | Aspect         | Method                                |
      | Versioning    | IPLD schema versioning               |
      | Translation   | Cross-version state mapping          |
      | Compatibility | Version negotiation                   |

  Scenario: Schema Migration
    Given protocol schema changes
    When migrating data
    Then perform:
      | Step           | Implementation                        |
      | Translation   | Data structure conversion             |
      | Verification  | Consistency checking                  |
      | Publication   | New format publication                |
