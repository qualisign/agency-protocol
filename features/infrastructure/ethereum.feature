Feature: Smart Contract Architecture
  Define the core contract system for Agency Protocol

  Background:
    Given the Ethereum network is available
    And gas optimization is a priority
    And contract upgradeability is required

  Scenario: Contract Registry Deployment
    Given the protocol needs contract organization
    When deploying the registry system
    Then create contracts for:
      | Contract Type   | Responsibility                         |
      | AgentRegistry  | Agent identity and verification        |
      | PromiseFactory | Promise creation and management        |
      | MeritToken    | Merit tracking and calculation         |
      | DataRegistry  | Data asset referencing                 |
      | StakeManager  | Credit stake management                |
    And implement proxy patterns for upgradeability
    And emit deployment events

  Scenario: Contract Interfaces
    Given core contracts are deployed
    When defining interfaces
    Then implement:
      | Interface       | Functions                              |
      | IAgent         | Identity management, state updates     |
      | IPromise       | Promise lifecycle management           |
      | IMerit         | Merit calculation and verification     |
      | IData          | Data reference and access control      |
      | IStake         | Stake management and verification      |

Feature: Agent Identity Management
  Implement agent identity using smart contracts

  Scenario: Agent Registration
    Given an agent needs Ethereum presence
    When registering on-chain
    Then:
      | Action         | Implementation                         |
      | Create record | Map address to agent metadata         |
      | Store state   | IPFS hash of full state              |
      | Issue NFT     | Non-transferable identity token       |
      | Emit event    | AgentRegistered                       |

  Scenario: Identity Verification
    Given an agent is registered
    When verifying identity
    Then check:
      | Verification   | Method                                 |
      | Ownership     | Address control                        |
      | State         | IPFS content verification              |
      | Signatures    | Message signing validation             |
      | Status        | Active state confirmation              |

Feature: Promise Smart Contracts
  Implement promises using Ethereum contracts

  Scenario: Promise Creation
    Given an agent wants to make a promise
    When creating the promise contract
    Then:
      | Component      | Implementation                         |
      | Terms         | IPFS hash of detailed terms           |
      | Stakes        | Required token deposits                |
      | Timelock      | Execution constraints                  |
      | Verification  | Fulfillment conditions                 |
    And emit PromiseCreated event
    And update agent state

  Scenario: Promise Fulfillment
    Given a promise contract exists
    When fulfilling the promise
    Then:
      | Action         | Implementation                         |
      | Verify        | Meet fulfillment conditions           |
      | Release       | Return or distribute stakes            |
      | Update        | Record fulfillment state              |
      | Emit          | PromiseFulfilled event                |

Feature: Merit Token System
  Implement merit tracking using ERC-20 and ERC-1155

  Scenario: Domain Merit Tokens
    Given merit needs tokenization
    When implementing merit tokens
    Then create:
      | Token Type     | Purpose                                |
      | DomainMerit   | ERC-1155 for domain-specific merit    |
      | GlobalMerit   | ERC-20 for aggregate merit            |
      | Proofs        | NFTs for achievement proof             |

  Scenario: Merit Calculation
    Given promise fulfillment occurs
    When calculating merit
    Then:
      | Step           | Implementation                         |
      | Validate      | Check fulfillment proofs              |
      | Calculate     | Apply merit formulas                   |
      | Mint          | Issue appropriate tokens               |
      | Record        | Update on-chain state                  |

Feature: Layer 2 Integration
  Optimize for scalability using L2 solutions

  Scenario: L2 Promise Management
    Given gas costs need optimization
    When implementing on L2
    Then use:
      | L2 Feature     | Implementation                         |
      | State channels| For promise negotiation               |
      | Rollups       | For merit calculations                |
      | Optimistic    | For data reference management         |

  Scenario: Cross-Layer Operations
    Given operations span L1 and L2
    When coordinating actions
    Then implement:
      | Aspect         | Method                                 |
      | State sync    | Bridge contract coordination          |
      | Proofs        | Cross-layer verification              |
      | Fallback      | L1 safety guarantees                  |

Feature: Data Reference System
  Implement secure data sharing references

  Scenario: Data Asset Registration
    Given data needs on-chain reference
    When registering data asset
    Then create:
      | Component      | Implementation                         |
      | Reference     | IPFS content identifier               |
      | Access        | Permission NFTs                        |
      | Terms         | Usage conditions                       |
      | Revocation    | Access control mechanism              |

  Scenario: Access Control
    Given data assets are referenced
    When managing access
    Then implement:
      | Control        | Method                                 |
      | Permissions   | Role-based access control             |
      | Tokens        | Access right NFTs                      |
      | Revocation    | Token burning mechanism               |

Feature: Economic Incentives
  Implement protocol incentives using tokens

  Scenario: Stake Management
    Given promises require stakes
    When managing stakes
    Then implement:
      | Component      | Implementation                         |
      | Deposit       | Token locking mechanism               |
      | Escrow        | Time-locked contracts                  |
      | Release       | Condition-based distribution           |
      | Slashing      | Penalty enforcement                    |

  Scenario: Reward Distribution
    Given positive outcomes occur
    When distributing rewards
    Then:
      | Action         | Implementation                         |
      | Calculate     | Apply reward formulas                  |
      | Distribute    | Token transfers                        |
      | Record        | Update merit scores                    |
      | Emit          | RewardDistributed event               |

Feature: Oracle Integration
  Enable external data verification

  Scenario: Promise Verification Oracle
    Given external verification is needed
    When integrating oracles
    Then implement:
      | Component      | Implementation                         |
      | Chainlink     | External data verification            |
      | UMA           | Optimistic verification               |
      | Custom        | Domain-specific oracles               |

  Scenario: Data Availability Verification
    Given data references need verification
    When checking availability
    Then use:
      | Method         | Implementation                         |
      | Proof of Access| Availability oracles                  |
      | IPFS Gateway  | Distributed storage check             |
      | Redundancy    | Multiple oracle consensus             |

Feature: Event System
  Implement comprehensive event tracking

  Scenario: Protocol Events
    Given protocol actions occur
    When emitting events
    Then record:
      | Event Type     | Content                                |
      | AgentUpdate   | State changes                          |
      | PromiseEvent  | Lifecycle updates                      |
      | MeritChange   | Score modifications                    |
      | StakeAction   | Stake management                       |

  Scenario: Event Indexing
    Given events need tracking
    When indexing events
    Then implement:
      | Component      | Implementation                         |
      | TheGraph      | Event indexing and querying           |
      | Substreams    | Event processing pipeline             |
      | Storage       | Distributed event archive             |

Feature: Gas Optimization
  Implement gas-efficient patterns

  Scenario: Storage Optimization
    Given storage costs matter
    When designing storage
    Then use:
      | Pattern        | Implementation                         |
      | Packed structs| Efficient data storage                |
      | IPFS refs     | Off-chain data storage               |
      | Batching      | Combined operations                   |
      | Proxy         | Minimal contract deployment           |

  Scenario: Operation Batching
    Given multiple operations occur
    When processing transactions
    Then implement:
      | Method         | Implementation                         |
      | MultiCall     | Batch transaction processing          |
      | Flash loans   | Atomic operations                     |
      | Meta txns     | Gas station network                   |

Feature: Contract Security
  Implement comprehensive security measures

  Scenario: Access Control
    Given contracts need protection
    When implementing security
    Then use:
      | Mechanism      | Implementation                         |
      | OpenZeppelin  | Standard security patterns            |
      | Roles         | RBAC implementation                   |
      | Pausable      | Emergency stop capability             |
      | Upgradeable   | Safe upgrade patterns                 |

  Scenario: Asset Safety
    Given assets need protection
    When handling value
    Then implement:
      | Safety         | Method                                 |
      | Rate limiting | Transaction caps                      |
      | Timelock      | Delayed execution                     |
      | MultiSig      | Administrative actions                |
      | Insurance     | Risk mitigation                       |
