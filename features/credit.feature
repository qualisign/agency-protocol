Feature: Credit System Fundamentals
  Credit represents transferrable value in the system
  Credit can be staked on promises
  Credit balances update in batches unless expedited

  Background:
    Given the system has registered agents with unique addresses
    And agents can sign promises with their private keys
    And agents can accumulate merit in domains through assessments
    And credit can be obtained through fulfilled promises or transfers
    And intentions can extend base types for specific requirements

  Scenario: Making a Payment Promise
    Given Agent A creates a promise
    And the promise references a payment intention of type "credit_payment"
    And the payment intention specifies 1000 credits
    When Agent A signs the promise with their private key
    Then the promise has a unique content-based address
    And the promise's signature can be verified with A's public key
    And the promise is registered in the system

  Scenario: Conditional Bilateral Promises
    Given Agent B creates a promise P1 with intention "build e-commerce website"
    And P1 is signed by Agent B
    And Agent A creates a promise P2 with intention "transfer 1000 credits to Agent B"
    And P2 is signed by Agent A
    When Agent A adds P1 as a condition of P2
    And Agent B adds P2 as a condition of P1
    Then each promise's fulfillment depends on the other's assessment
    And assessment of P1 as "KEPT" is necessary for P2's credit transfer
    And assessment of P2 as "KEPT" confirms the credit transfer completed

  Scenario: Batch Credit Updates
    Given multiple promises have received assessments
    When the regular batch update time arrives
    Then the system processes credit adjustments based on:
      | Factor               | Source                    |
      | Promise assessments | Kept or broken status     |
      | Staked amounts      | Original promise stakes    |
      | Domain relevance    | Assessment domains        |

  Scenario: Promise Assessment in Domain
    Given a promise has been made
    When an agent assesses the promise
    Then they create an assessment containing:
      | Field             | Content                    |
      | Promise Address   | Target promise             |
      | Assessor Address  | Assessing agent           |
      | Domain Address    | Relevant domain           |
      | Status           | KEPT or BROKEN             |
    And the assessment is signed by the assessor
    And merit updates in the domain await batch processing

  Scenario: Extending Payment Intentions
    Given a base payment intention type exists
    When an agent creates a specialized payment intention
    Then it can specify additional requirements like:
      | Type              | Examples                   |
      | bitcoin_payment   | BTC amount, address        |
      | credit_payment    | Credit amount, timeline    |
      | mutual_credit     | Credit line terms          |
    And merit accumulates specifically for that payment type

  Scenario: Credit Transfer Through Promises
    Given Agent A wants to transfer credit to Agent B
    When Agent A creates a promise referencing a credit_transfer intention
    Then the promise specifies:
      | Component         | Detail                     |
      | Intention        | Type: credit_transfer      |
      | Amount           | Transfer amount            |
      | Recipient        | Agent B's address          |
    And requires A's signature
    And processes in the next batch update

  Scenario: Mutual Credit Line Establishment
    Given Agent A and B want to establish mutual credit
    When they create reciprocal promises with mutual_credit intentions
    Then each promise specifies:
      | Component         | Detail                     |
      | Credit Limit     | Maximum negative balance    |
      | Settlement Terms | When/how to settle         |
      | Duration         | Time period                |
    And both promises must be signed
    And both promises must include the other as a condition

  Scenario: Merit Accumulation in Payment Domains
    Given an agent makes multiple payment promises
    When these promises are assessed as kept
    Then merit accumulates specifically in:
      | Domain Type        | Examples                   |
      | general_payment    | All payment types          |
      | bitcoin_payment    | BTC specific               |
      | mutual_credit     | Credit line handling       |
    And stake requirements adjust based on domain-specific merit

  Scenario: Promise Verification
    Given a promise exists in the system
    When verifying the promise
    Then the system checks:
      | Factor            | Verification                |
      | Signature        | Matches agent's public key  |
      | Address          | Content-based hash matches  |
      | Intention        | Exists and is valid         |
      | Conditions       | All references resolve      |
    And the promise is only valid if all checks pass
