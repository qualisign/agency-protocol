Feature: Evaluating Solutions Across Multiple Strategic Problems

  Background:
    Given I have access to two Large Language Models, LLM-A and LLM-B
    And I have prepared multiple strategic problem test cases:
      | Test Case                      |
      | Classic Stag Hunt scenario     |
      | Prisoner's Dilemma             |
      | Public Goods Game              |
      | Tragedy of the Commons         |
    And I have prepared solution descriptions for each test case:
      | Solution                            |
      | Agency Protocol solution            |
      | Blockchain-based consensus solution |
      | Web of Trust solution               |
    And each solution description includes:
      | Detail                     |
      | Key mechanisms             |
      | How it addresses the problem |
      | Main claimed advantages    |
    And each description is between 500 to 1000 words
    And I have defined evaluation criteria with a scoring scale from 1 to 10:
      | Criterion                      |
      | Game theory fundamentals       |
      | Economic efficiency            |
      | Implementation viability       |
      | Social scalability             |
      | Incentive compatibility        |
      | Defection deterrence           |
      | Collective payoff optimization |
    And I have created a rubric for each criterion

  Scenario Outline: Evaluating solutions to <Test Case> using LLM-A and LLM-B

    When I present the <Test Case> and solution descriptions to LLM-A with the evaluation criteria
    Then LLM-A:
      | Action                                                |
      | Provides a score from 1 to 10 for each solution on each criterion |
      | Provides detailed reasoning for each score            |
      | Identifies key strengths and weaknesses for each solution |
      | Concludes with an overall recommendation              |
      | Shows step-by-step reasoning process                  |

    When I present the same prompt to LLM-B
    Then LLM-B performs the same analysis independently

    When I present LLM-A's analysis to LLM-B with instructions to review
    Then LLM-B:
      | Action                                              |
      | Identifies points of agreement and disagreement     |
      | Assesses the strength of LLM-A's reasoning          |
      | Notes any factors LLM-A may have overlooked         |
      | Explains whether LLM-A's analysis changes its own conclusions |

    When I present LLM-B's analysis to LLM-A with instructions to review
    Then LLM-A performs the same review process on LLM-B's analysis

    Examples:
      | Test Case                  |
      | Classic Stag Hunt scenario |
      | Prisoner's Dilemma         |
      | Public Goods Game          |
      | Tragedy of the Commons     |

  Then I record for each test case:
    | Data                                           |
    | Complete prompts used                          |
    | Raw responses from each LLM                    |
    | Scores assigned to each solution               |
    | Reasoning provided for each score              |
    | Points of agreement and disagreement between LLMs |
    | Final conclusions and recommendations          |
