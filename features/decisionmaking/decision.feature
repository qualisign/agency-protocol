Feature: Consensus Agent
  Enable verifiable consensus-based decision making

  Background:
    Given the Consensus Agent exists
    And it has a content-based address
    And it can verify member signatures
    And it maintains an immutable consensus record

  Scenario: Consensus Process Initiation
    Given a consensus decision needs to be made
    When initiating the process
    Then record:
      | Component       | Content                                                   |
      | Topic          | Clear description of decision needed                      |
      | Scope          | Whether this affects group identity/structure/intent      |
      | Participants   | List of agent addresses authorized to participate         |
      | StartTime      | Timestamp of initiation                                  |
      | State          | "Gathering" (initial state)                              |

  Scenario: Member Contribution
    Given a consensus process is active
    When a member contributes
    Then verify and record:
      | Aspect         | Verification                                              |
      | Identity      | Member's agent address and signature                      |
      | Eligibility   | Current participant status                                |
      | Contribution  | Insight/concern/proposal                                  |
      | Timestamp     | When contribution was made                                |
      | References    | Links to other contributions being responded to           |

  Scenario: Consensus Achievement
    Given all members have contributed
    When consensus appears to be emerging
    Then check:
      | Requirement    | Verification                                              |
      | Participation | All active members have contributed                       |
      | Agreement     | No outstanding objections                                 |
      | Clarity       | Final decision is unambiguously stated                    |
      | Commitment    | Explicit agreement from all members                       |
    And record:
      | Field         | Content                                                   |
      | Decision      | Final consensus reached                                   |
      | Evidence      | All member signatures on final decision                   |
      | CompletedAt   | Timestamp of achievement                                  |
      | State         | "Achieved"                                                |

Feature: Meritocratic Agent
  Enable verifiable merit-based authority delegation

  Background:
    Given the Meritocratic Agent exists
    And it can verify delegated authority
    And it maintains authority scope records

  Scenario: Authority Delegation
    Given consensus has selected a focus person
    When delegating authority
    Then record:
      | Component      | Content                                                   |
      | Authority     | Clear scope of delegated power                           |
      | Delegate      | Agent address of focus person                            |
      | Source        | Reference to consensus decision granting authority       |
      | TimeScope     | Duration or event bounds of authority                    |
      | Constraints   | Explicit limits on authority                             |

  Scenario: Authority Exercise
    Given delegated authority exists
    When the focus person makes a decision
    Then verify and record:
      | Check         | Verification                                              |
      | Authority    | Decision is within delegated scope                        |
      | Timing       | Authority is currently valid                              |
      | Signature    | Focus person has signed the decision                      |
      | Constraints  | Decision respects defined limits                          |

  Scenario: Merit Record Maintenance
    Given authority has been exercised
    When updating merit records
    Then track:
      | Aspect        | Content                                                   |
      | Decisions    | History of decisions made                                 |
      | Outcomes     | Results and consequences                                  |
      | Feedback     | Member assessments of decisions                          |
      | Performance  | Objective metrics where available                         |

Feature: Democratic Agent
  Enable verifiable majority-based process suspension

  Background:
    Given the Democratic Agent exists
    And it can verify member voting rights
    And it maintains voting records

  Scenario: Vote Initiation
    Given a member calls for a vote
    When verifying the call is valid
    Then check:
      | Requirement   | Verification                                              |
      | Caller       | Valid member signature                                    |
      | Type         | Either consensus suspension or authority revocation       |
      | Context      | Clear identification of what is being voted on            |
      | Urgency      | Immediate vote requirement verification                   |

  Scenario: Vote Processing
    Given a valid vote is called
    When collecting votes
    Then verify each vote:
      | Aspect        | Verification                                              |
      | Member       | Valid member signature                                    |
      | Timing       | Within voting window                                      |
      | Uniqueness   | One vote per member                                      |
      | Format       | Boolean (yes/no) only                                    |

  Scenario: Vote Resolution
    Given votes have been collected
    When determining outcome
    Then calculate:
      | Factor        | Calculation                                              |
      | Participation| Percentage of eligible members voting                    |
      | Result       | Simple majority of votes cast                           |
      | Outcome      | Whether threshold for action is met                     |
    And record:
      | Field         | Content                                                  |
      | VoteResult   | Final tally and outcome                                 |
      | Evidence     | All vote signatures and calculations                    |
      | Effect       | Automatic triggering of consensus or authority change   |

Feature: Agent Interaction Patterns
  Define how decision agents work together

  Scenario: Consensus to Merit Transition
    Given consensus has been achieved
    When delegating authority
    Then:
      | Step          | Implementation                                           |
      | Consensus    | Records final agreement                                 |
      | Merit        | Creates new authority record                            |
      | Democracy    | Begins monitoring for suspension calls                  |

  Scenario: Merit to Consensus Return
    Given democratic vote suspends authority
    When transitioning back to consensus
    Then:
      | Step          | Implementation                                           |
      | Democracy    | Records vote outcome                                    |
      | Merit        | Archives authority record as suspended                  |
      | Consensus    | Initiates new process if needed                        |

  Scenario: Consensus Process Suspension
    Given democratic vote suspends consensus
    When managing the transition
    Then:
      | Step          | Implementation                                           |
      | Democracy    | Records vote outcome                                    |
      | Consensus    | Archives current state                                 |
      | Merit        | May initiate temporary coordination                    |


Feature: Secure Consensus and Governance with Zero-Knowledge Proofs

  Background:
    Given the system supports Consensus, Meritocratic, and Democratic Agents
    And Zero-Knowledge Proofs (ZKPs) enable privacy-preserving operations
    And agents have unique content-based addresses and verifiable signatures
    And the system maintains immutable records of decisions and actions

  Scenario: Initiating a Consensus Process
    Given a consensus decision is required
    When a Consensus Agent initiates the process
    Then record:
      | Component       | Content                                               |
      | Topic           | Description of the decision                          |
      | Scope           | Group identity/structure/intent impact               |
      | Participants    | Authorized agent addresses                           |
      | StartTime       | Timestamp of initiation                              |
      | State           | "Gathering" (initial state)                          |

  Scenario: Member Contributions with Privacy
    Given a consensus process is active
    And a member contributes anonymously using ZKP
    Then verify:
      | Aspect          | Verification                                          |
      | Eligibility     | Prove the contributor is authorized                  |
      | Uniqueness      | Ensure no duplicate contributions                    |
      | Timestamp       | Validate submission timing                           |
    And do not reveal the contributor's identity or contribution content
    And record the verified contribution securely

  Scenario: Achieving Consensus
    Given members have contributed to a consensus process
    When the system evaluates contributions
    Then verify:
      | Requirement     | Verification                                          |
      | Participation   | All authorized members contributed                   |
      | Agreement       | No unresolved objections                             |
      | Clarity         | Final decision is unambiguously stated               |
    And record:
      | Field           | Content                                               |
      | Decision        | Final consensus result                               |
      | Evidence        | Member signatures or ZKPs                            |
      | CompletedAt     | Timestamp of consensus                               |
      | State           | "Achieved"                                           |

  Scenario: Delegating Authority through Meritocratic Agents
    Given a consensus decision delegates authority
    When a Meritocratic Agent assigns authority
    Then record:
      | Component       | Content                                               |
      | Authority       | Scope of delegated power                             |
      | Delegate        | Agent address of the focus person                    |
      | Source          | Reference to consensus decision                      |
      | TimeScope       | Duration or event bounds of authority                |
      | Constraints     | Explicit limits on authority                         |

  Scenario: Exercising Authority with Accountability
    Given authority has been delegated
    When the focus person exercises their authority
    Then verify:
      | Aspect          | Verification                                          |
      | Scope           | Action is within delegated authority                 |
      | Timing          | Action occurs within time bounds                     |
      | Constraints     | Respects defined limits                              |
      | Signature       | Decision is signed by the focus person               |
    And record the action and outcomes in the system

  Scenario: Democratic Suspension of Authority or Consensus
    Given a member calls for a vote to suspend authority or consensus
    And a Democratic Agent processes the vote
    Then verify:
      | Aspect          | Verification                                          |
      | Caller          | Valid member signature                               |
      | Type            | Consensus or authority suspension                    |
      | Eligibility     | Voters are authorized participants                   |
      | Timing          | Votes are cast within the voting window              |
      | Uniqueness      | Each voter casts a single vote                       |
    And calculate:
      | Factor          | Calculation                                           |
      | Participation   | Percentage of eligible members voting                |
      | Result          | Majority or threshold-based outcome                  |
    And record:
      | Field           | Content                                               |
      | VoteResult      | Final tally and outcome                              |
      | Evidence        | All vote signatures or ZKPs                          |
      | Effect          | Automatic triggering of suspension                   |

  Scenario: Privacy-Preserving Group Validation
    Given all eligible members must participate in a process
    And member identities are confidential
    When verifying participation
    Then prove:
      | Aspect          | Verification                                          |
      | Inclusivity     | All eligible members contributed                     |
    And do not reveal individual identities or actions

  Scenario: Proving Compliance with Data Policies
    Given an agent shares data under specific usage policies
    When the recipient generates a ZKP to prove compliance
    Then verify:
      | Aspect          | Verification                                          |
      | Scope           | Data usage adheres to agreed terms                   |
      | Timing          | Data was accessed within the permitted timeframe     |
    And confirm compliance without revealing sensitive details

  Scenario: Monitoring Merit with Accountability
    Given authority is delegated and actions must be tracked
    When monitoring merit use
    Then verify:
      | Aspect          | Verification                                          |
      | Compliance      | Actions align with constraints                       |
      | Outcomes        | Results match expected performance                   |
    And maintain an immutable record of actions and outcomes

  Scenario: Privacy-Preserving Voting in Governance
    Given members participate in a merit-weighted vote
    And votes must remain private
    When members cast their votes using ZKPs
    Then verify:
      | Aspect          | Verification                                          |
      | Eligibility     | Voters are authorized participants                   |
      | Uniqueness      | Each member votes only once                          |
      | Timing          | Votes are cast within the valid window               |
    And calculate:
      | Factor          | Calculation                                           |
      | Participation   | Percentage of eligible members voting                |
      | Result          | Majority or threshold-based outcome                  |
    And ensure vote integrity without exposing voter choices
      
