Feature: EGP Process Agent
  Enable structured inquiry and question generation in community contexts

  Background:
    Given the EGP Agent exists
    And it can track participant interactions
    And it maintains a question archive
    And it supports Inquiry Coach guidance

  Scenario: Base Question Selection
    Given community members have registered interests
    When organizing small groups
    Then record:
      | Component          | Content                                                      |
      | BaseQuestion      | Core inquiry focus                                          |
      | Participants      | List of interested member addresses                         |
      | InquiryCoach     | Assigned facilitator address                                |
      | Context          | Relevant community background                                |
    And ensure:
      | Requirement       | Verification                                                 |
      | GroupSize        | Between 5-12 participants                                    |
      | CoachQualified   | Inquiry Coach has valid credentials                         |
      | QuestionScope    | Base question aligns with community focus                    |

  Scenario: Meeting Initialization
    Given a small group is forming
    When starting the meeting
    Then establish:
      | Action            | Implementation                                               |
      | Recording        | Start session recording                                     |
      | Authentication   | Verify all participant IDs                                  |
      | Context Setting  | Share base question and relevant history                    |
      | Phase Tracking   | Initialize three-phase process tracking                     |

  Scenario: Experience Exploration Phase
    Given the meeting has started
    When exploring participant experiences
    Then track:
      | Element           | Content                                                      |
      | Contributions    | Individual experience sharing                                |
      | Connections      | Links between experiences                                    |
      | Patterns        | Emerging themes and insights                                 |
    And ensure:
      | Requirement      | Implementation                                               |
      | Participation   | All members contribute                                       |
      | Depth           | Move beyond surface observations                             |
      | Documentation   | Capture key insights                                         |

  Scenario: Branch Question Generation
    Given experience exploration is complete
    When generating branch questions
    Then record:
      | Question Element  | Verification                                                 |
      | Derivation      | Clear link to base question                                  |
      | Uniqueness      | Not previously asked/archived                                |
      | Quality         | Meets deeper inquiry criteria                                |
      | Context         | Captures relevant background                                 |
    And track:
      | Metadata         | Content                                                      |
      | Creator         | Question originator                                          |
      | Evolution       | How question emerged                                         |
      | Dependencies    | Related questions needed                                     |
      | Implications    | Potential impact on base question                            |

  Scenario: Meeting Summary Phase
    Given branch questions have been generated
    When summarizing the meeting
    Then create:
      | Component        | Content                                                      |
      | KeyInsights     | Major realizations and patterns                             |
      | QuestionMap     | Hierarchy of generated questions                            |
      | NextSteps       | Implications for future inquiry                             |
      | Metadata        | Session context and participation                           |

  Scenario: Question Archive Management
    Given a meeting has concluded
    When archiving results
    Then process:
      | Archive Element   | Implementation                                               |
      | Questions        | Store base and branch questions                             |
      | Relationships    | Map question dependencies and evolution                     |
      | Context         | Link to community background                                |
      | Access          | Define visibility and usage rights                          |
    And ensure:
      | Quality Check    | Verification                                                 |
      | Completeness    | All required elements present                               |
      | Coherence       | Clear narrative of inquiry                                  |
      | Accessibility   | Searchable and discoverable                                 |

Feature: Inquiry Coach Support
  Enable effective facilitation of EGP meetings

  Scenario: Coach Guidance
    Given an EGP meeting is in progress
    When providing facilitation support
    Then offer:
      | Support Type     | Implementation                                               |
      | PhasePrompts    | Suggested questions for each phase                          |
      | TimeTracking    | Phase duration monitoring                                   |
      | GroupDynamics   | Participation balance indicators                            |
      | QuestionQuality | Real-time feedback on question development                  |

  Scenario: Real-time Analytics
    Given a meeting is active
    When monitoring group dynamics
    Then track:
      | Metric           | Measurement                                                  |
      | Participation   | Speaking time distribution                                   |
      | Depth           | Question evolution patterns                                  |
      | Engagement      | Active listening indicators                                  |
      | Progress        | Phase completion status                                      |

Feature: Community Integration
  Enable community-level insight emergence

  Scenario: Pattern Recognition
    Given multiple meetings have occurred
    When analyzing question archives
    Then identify:
      | Pattern Type     | Analysis                                                     |
      | Themes          | Recurring topics and concerns                               |
      | Dependencies    | Question relationships across groups                        |
      | Evolution       | How questions mature over time                              |
      | Gaps            | Unexplored areas of inquiry                                |

  Scenario: Community Navigation
    Given archived questions exist
    When community members explore
    Then provide:
      | View             | Implementation                                               |
      | Timeline        | Question evolution over time                                |
      | Network         | Question relationship mapping                               |
      | Themes          | Topic-based organization                                    |
      | Impact          | Connection to community challenges                          |

  Scenario: Process Evolution
    Given community inquiry patterns emerge
    When adapting the process
    Then update:
      | Component        | Adaptation                                                   |
      | BaseQuestions   | Refine based on emergent patterns                          |
      | GroupFormation  | Adjust based on interaction quality                         |
      | CoachGuidance   | Enhance based on effective patterns                         |
      | Documentation   | Improve based on usage patterns                             |
