Feature: API Interface
  Enable programmatic interaction with the Agency Protocol

  Background:
    Given the API server is running
    And authentication is configured

  Scenario: Create Agent
    When a POST request is sent to "/agents" with:
      | publicKey | signature |
      | key123    | sig456    |
    Then the response status should be 201
    And the response should contain an agent address
    And the agent should be registered in the system

  Scenario: Make Promise
    Given an agent exists with address "agent123"
    When a POST request is sent to "/promises" with:
      | agentAddress | description        | signature |
      | agent123     | deliver messages   | sig789    |
    Then the response status should be 201
    And the response should contain a promise address
    And the promise should be recorded in the system

  Scenario: Assess Promise
    Given a promise exists with address "promise123"
    When a POST request is sent to "/assessments" with:
      | promiseAddress | assessorAddress | status | signature |
      | promise123     | agent456        | KEPT   | sig101    |
    Then the response status should be 201
    And the assessment should be recorded in the system
