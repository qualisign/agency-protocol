Feature: Content-Addressable Storage Service
  Enable reliable, distributed storage of protocol objects by content address

  Background:
    Given a content-addressable storage service exists
    And it supports multi-region replication
    And it has configurable access controls

  Scenario: Store Agent State
    Given an agent state has a content-based address
    When the state is stored
    Then it should be retrievable by its content address
    And its integrity should be verifiable

  Scenario: Content Deduplication
    Given multiple agents store identical content
    When the content is stored
    Then only one copy should be physically stored
    And all content addresses should resolve correctly
