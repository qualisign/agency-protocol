Feature: Interface Independence
  Protocol must work with any interface type

  Scenario Outline: Interface Agnostic Operations
    Given an agent uses a <interface_type> interface
    When the agent performs protocol operations
    Then all operations must work correctly
    And all cryptographic verifications must succeed
    And all protocol guarantees must be maintained

    Examples:
      | interface_type |
      | command line   |
      | web interface  |
      | emacs          |
