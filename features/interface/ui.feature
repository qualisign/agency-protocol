Feature: Web Interface
  Enable user interaction with the Agency Protocol through a web interface

  Background:
    Given the web application is running
    And a user is viewing the interface

  Scenario: View Agent Creation Form
    When the user navigates to the agent creation page
    Then they should see:
      | Field     | Type   |
      | Name      | input  |
      | PublicKey | input  |
    And a "Create Agent" button should be visible

  Scenario: Create New Agent
    When the user fills in the agent creation form with:
      | Field     | Value     |
      | Name      | TestAgent |
      | PublicKey | key123    |
    And clicks the "Create Agent" button
    Then they should see a success message
    And the new agent should appear in their agent list

  Scenario: Make Promise
    Given the user has an agent "TestAgent"
    When they navigate to the promise creation page
    And fill in the promise form with:
      | Field       | Value           |
      | Description | deliver message |
    And click "Make Promise"
    Then they should see the new promise in their list
