Feature: Security and Handling Malicious Behavior
  The Agency Protocol must ensure authenticity and integrity of all operations
  All interactions are based on cryptographically signed promises and assessments
  Merit and credit calculations are verifiable through signed assessment chains
  Batch processing ensures consistent system state updates

  Background:
    Given agents have content-based addresses derived from their public keys and states
    And all promises reference specific intentions
    And all assessments reference specific domains
    And updates to credit and merit occur in batches
    And each agent's state links to its previous state

  Scenario: Agent State Creation and Authentication
    Given an agent creates their initial state
    When they register in the system
    Then their state must contain:
      | Component        | Content                           |
      | Public Key      | Agent's verification key          |
      | Name            | Result of name promise            |
      | Promises        | Array of signed promises          |
      | Parents         | Optional array of parent agents   |
      | Previous        | None for initial state            |
      | Signature       | Signed with private key           |
    And results in a unique content-based address

  Scenario: Promise Creation and Verification
    Given Agent A wants to make a promise
    When they create the promise
    Then it must contain:
      | Component        | Content                           |
      | Intention       | Reference to existing intention   |
      | Conditions      | Array of promises/intentions      |
      | Credit Stake    | Amount based on domain merit      |
      | Signature       | Signed with A's private key       |
    And the promise gets a unique content-based address
    And other agents can verify authenticity using A's public key

  Scenario: Assessment Security
    Given Agent B wants to assess Agent A's promise P
    When they create an assessment
    Then it must contain:
      | Component        | Content                           |
      | Promise Address | Reference to promise P            |
      | Domain Address  | Specific domain being assessed    |
      | Status          | KEPT or BROKEN                    |
      | Timestamp       | When assessment was made          |
      | Signature       | Signed with B's private key       |
    And enters pending batch queue for processing

  Scenario: Batch Update Security
    Given multiple operations are pending
    When a batch update occurs
    Then the system processes in order:
      | Operation Type   | Security Checks                   |
      | Promises        | Signature verification            |
      | Assessments     | Domain and promise validation     |
      | Credit Updates  | Balance and stake verification    |
      | Merit Updates   | Assessment chain verification     |
    And produces a new verifiable system state

  Scenario: Credit Transfer Security
    Given Agent A initiates credit transfer to Agent B
    When creating the transfer promise
    Then it must contain:
      | Component        | Content                           |
      | Intention       | Credit transfer intention         |
      | Amount          | Transfer amount                   |
      | Recipient       | Agent B's address                 |
      | Signature       | A's signature                     |
    And waits for next batch update
    And can be verified by all agents

  Scenario: Merit Calculation Security
    Given a domain D has multiple assessments
    When calculating merit for Agent A
    Then any agent can:
      | Step             | Verification                      |
      | Collect         | Gather signed assessments in D    |
      | Verify          | Check all signatures              |
      | Order           | Sort by timestamp                 |
      | Calculate       | Apply merit formula               |
    And arrive at the same result

  Scenario: Preventing State Tampering
    Given Agent A updates their state
    When they publish the new state
    Then it must:
      | Requirement      | Verification                      |
      | Link Previous   | Reference old state address       |
      | Sign Updates    | Cover all state changes           |
      | Verify Chain    | Maintain unbroken state chain     |
    And all agents can verify the update chain

  Scenario: Expedited Update Security
    Given Agent A requests expedited processing
    When they pay the processing fee
    Then the system must:
      | Step             | Verification                      |
      | Verify Payment  | Confirm fee transfer              |
      | Process Updates | Apply pending operations          |
      | Create State    | Generate new valid state          |
      | Notify          | Inform affected agents            |
    And maintain all security guarantees

  Scenario: Domain Reference Security
    Given an assessment references domain D
    When processing the assessment
    Then the system verifies:
      | Check            | Verification                      |
      | Domain Exists   | Valid domain address              |
      | Scope Valid     | Promise matches domain            |
      | Authority       | Assessor can assess in domain     |
    And rejects invalid domain references

  Scenario: Preventing Duplicate Operations
    Given operation O has been processed
    When an attempt is made to replay O
    Then the system detects:
      | Check            | Issue                             |
      | Address Exists  | Content-based address collision   |
      | Batch Processed | Already included in past batch    |
      | Timing Invalid  | Outside processing window         |
    And rejects the duplicate

  Scenario: Multi-Domain Promise Security
    Given a promise involves domains D1 and D2
    When processing the promise
    Then for each domain:
      | Verification     | Requirement                       |
      | Merit Chain     | Valid calculation history         |
      | Assessments     | Domain-specific validation        |
      | Stakes          | Appropriate credit amounts        |
    And all aspects must be cryptographically verifiable

  Scenario: Agent Identity Protection
    Given Agent A makes sensitive promise P
    When they include private data
    Then they can:
      | Action           | Method                            |
      | Encrypt Data    | Use recipient's public key        |
      | Sign Package    | With their private key            |
      | Control Access  | Specify authorized readers        |
    And maintain confidentiality while ensuring authenticity

  Scenario: Batch Timing Security
    Given batch B is being processed
    When including operations
    Then the system enforces:
      | Rule             | Enforcement                       |
      | Time Window     | Valid operation period            |
      | Order           | Correct sequence                  |
      | Dependencies    | All prerequisites met             |
    And maintains consistent state updates
