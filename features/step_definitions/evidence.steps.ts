import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { createAssessment, Evidence, Agent, Promise, AssessmentStatus } from '../../src/agent';

Given('a restaurant makes promise P:', function(dataTable) {
  const data = dataTable.rowsHash();
  this.promiseData = data;
});

When('they submit daily evidence:', function(dataTable) {
  const evidenceData = dataTable.rowsHash();
  this.evidence = {
    type: this.promiseData['Evidence'],
    data: evidenceData,
    timestamp: Date.now()
  };
});

Then('validator group must:', function(dataTable) {
  const requirements = dataTable.rowsHash();
  // Store requirements for later validation
  this.validatorRequirements = requirements;
});

When('customers submit assessments', function() {
  // Setup for customer assessment scenario
  this.assessmentType = 'customer_experience';
});

Then('assessments are weighted by:', function(dataTable) {
  const weights = dataTable.rowsHash();
  // Store weights for merit calculation
  this.assessmentWeights = weights;
});

Then('no formal evidence is required', function() {
  expect(this.assessmentType).to.equal('customer_experience');
});
