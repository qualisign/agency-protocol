import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { Message, verifyMessage } from '../../src/infrastructure';

Given('an agent uses a {word} interface', function(interfaceType: string) {
  this.interfaceType = interfaceType;
});

When('the agent performs protocol operations', function() {
  this.message = {
    type: 'promise',
    content: { test: 'data' },
    sender: 'test-agent-address',
    signature: 'test-signature',
    timestamp: Date.now()
  };
});

Then('all operations must work correctly', function() {
  expect(this.message).to.exist;
});

Then('all cryptographic verifications must succeed', function() {
  const isValid = verifyMessage(this.message, 'test-public-key');
  expect(isValid).to.be.true;
});

Then('all protocol guarantees must be maintained', function() {
  expect(this.message.sender).to.exist;
  expect(this.message.signature).to.exist;
  expect(this.message.timestamp).to.be.a('number');
});
