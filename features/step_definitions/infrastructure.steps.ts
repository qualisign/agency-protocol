import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { AWSInfrastructure } from '../../src/infrastructure';

let infrastructure: AWSInfrastructure;
let testContent: any;
let testContentId: string;

Given('an S3 bucket exists for content storage with versioning enabled', function() {
  // Using test bucket name
  infrastructure = new AWSInfrastructure('test-bucket', 'ContentAddresses');
});

Given('a DynamoDB table {string} exists with schema:', function(tableName: string) {
  // Table existence is verified in infrastructure constructor
  infrastructure = new AWSInfrastructure('test-bucket', tableName);
});

When('storing the content', async function() {
  testContent = { test: 'data' };
  testContentId = 'test-content-id';
  await infrastructure.storeContent(testContentId, testContent);
  await infrastructure.storeMetadata(testContentId, {
    ContentType: 'test',
    CreatedAt: Date.now(),
    Size: JSON.stringify(testContent).length,
    References: '[]',
    AccessPattern: 'hot'
  });
});

Then('ensure:', function(dataTable: any) {
  // Verification steps would go here
  // For now we're just checking the infrastructure is initialized
  expect(infrastructure).to.not.be.undefined;
});
