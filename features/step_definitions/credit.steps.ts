import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { createAgent, createIntention, makePromise, PaymentIntention } from '../../src/agent';

Given('Agent A creates a promise', function() {
  const keyPair = createKeyPair();
  this.agentA = createAgent(keyPair);
});

Given('the promise references a payment intention of type {string}', function(paymentType: string) {
  const intention: PaymentIntention = {
    address: `intention-${Math.random().toString(36).substring(7)}`,
    description: `Payment of type ${paymentType}`,
    type: paymentType as 'credit_payment',
    amount: 1000
  };
  this.intention = intention;
});

When('Agent A signs the promise with their private key', function() {
  this.promise = makePromise(this.agentA, this.intention);
});

Then('the promise has a unique content-based address', function() {
  expect(this.promise.address).to.be.a('string');
  expect(this.promise.address).to.not.be.empty;
});

Then('the promise\'s signature can be verified with A\'s public key', function() {
  const isValid = verifySignature(this.agentA.publicKey, this.promise.signature, this.promise.description);
  expect(isValid).to.be.true;
});

Then('the promise is registered in the system', function() {
  this.registry = new Registry();
  this.registry.register(this.promise);
  const events = this.registry.getEvents();
  const registerEvent = events.find(e => e.type === 'REGISTER' && e.promiseAddress === this.promise.address);
  expect(registerEvent).to.exist;
});
