import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { createAgent, createAssessment, Assessment, Agent, Promise } from '../../src/agent';

Given('Agent A has fulfilled a promise P in domain D', function() {
  const keyPair = createKeyPair();
  this.agentA = createAgent(keyPair);
  this.domain = 'web_development';
  this.promise = makePromise(this.agentA, createIntention('test promise'));
});

When('Agent B assesses P as {string}', function(status: string) {
  const keyPair = createKeyPair();
  this.agentB = createAgent(keyPair);
  this.assessment = createAssessment(
    this.agentB,
    this.promise,
    status as AssessmentStatus,
    this.domain
  );
});

Then('the system records an assessment containing:', function(dataTable) {
  const expected = dataTable.rowsHash();
  expect(this.assessment.promiseAddress).to.equal(this.promise.address);
  expect(this.assessment.assessorAddress).to.equal(this.agentB.address);
  expect(this.assessment.domain).to.equal(this.domain);
  expect(this.assessment.status).to.equal(expected.Status);
  expect(this.assessment.signature).to.exist;
});

Then('Agent A begins accumulating merit in domain D', function() {
  // Merit accumulation will be handled by the registry
  this.registry.notifyAssessment(this.assessment);
  const events = this.registry.getEvents();
  const meritEvent = events.find(e => 
    e.type === 'ASSESSMENT' && 
    e.assessmentAddress === this.assessment.address
  );
  expect(meritEvent).to.exist;
});
