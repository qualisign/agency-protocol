import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';

Given('the web application is running', function() {
  // Handled by test environment setup
});

Given('a user is viewing the interface', function() {
  // Handled by test environment setup
});

When('the user navigates to the agent creation page', function() {
  // Navigation simulation in test environment
});

Then('they should see:', function(dataTable: any) {
  const fields = dataTable.hashes();
  // Verify fields are present in the UI
});

When('the user fills in the agent creation form with:', function(dataTable: any) {
  const formData = dataTable.hashes()[0];
  this.formData = formData;
});

Then('they should see a success message', function() {
  // Verify success message is shown
});

Given('the user has an agent {string}', function(agentName: string) {
  this.agentName = agentName;
});
