import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { ConsensusAgent, ConsensusDecision } from '../../src/decision';

let consensusAgent: ConsensusAgent;
let currentDecisionId: string;

Given('the Consensus Agent exists', function() {
  consensusAgent = new ConsensusAgent();
});

When('initiating the process', function() {
  currentDecisionId = consensusAgent.initiateConsensus(
    'Test decision',
    'structure',
    ['agent1', 'agent2', 'agent3']
  );
});

Then('record:', function(dataTable: any) {
  const decision = consensusAgent.getDecision(currentDecisionId);
  expect(decision).to.not.be.undefined;
  expect(decision?.state).to.equal('Gathering');
  expect(decision?.participants).to.have.length(3);
});

When('a member contributes', function() {
  consensusAgent.contribute(
    currentDecisionId,
    'agent1',
    'Test contribution',
    []
  );
});

Then('verify and record:', function(dataTable: any) {
  const decision = consensusAgent.getDecision(currentDecisionId);
  expect(decision?.contributions).to.have.length(1);
  expect(decision?.contributions?.[0].agentAddress).to.equal('agent1');
});
