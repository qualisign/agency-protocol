import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { createAgent, createPromise, createAssessment } from '../../src/api';

let response: any;

Given('the API server is running', function() {
  // Server setup handled by test environment
});

Given('authentication is configured', function() {
  // Auth setup handled by test environment
});

When('a POST request is sent to {string} with:', async function(endpoint: string, dataTable: any) {
  const data = dataTable.hashes()[0];
  switch(endpoint) {
    case '/agents':
      response = await createAgent(data);
      break;
    case '/promises':
      response = await createPromise(data);
      break;
    case '/assessments':
      response = await createAssessment(data);
      break;
  }
});

Then('the response status should be {int}', function(status: number) {
  expect(response.status).to.equal(status);
});

Then('the response should contain an agent address', function() {
  expect(response.data.address).to.be.a('string');
});

Then('the agent should be registered in the system', function() {
  // Verification handled by agent registration tests
});

Given('an agent exists with address {string}', function(address: string) {
  this.agentAddress = address;
});

Then('the response should contain a promise address', function() {
  expect(response.data.address).to.be.a('string');
});

Then('the promise should be recorded in the system', function() {
  // Verification handled by promise creation tests
});

Given('a promise exists with address {string}', function(address: string) {
  this.promiseAddress = address;
});

Then('the assessment should be recorded in the system', function() {
  // Verification handled by assessment creation tests
});
