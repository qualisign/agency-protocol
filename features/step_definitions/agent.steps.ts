import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { createKeyPair, createAgent, createIntention, makePromise, verifySignature } from '../../src/agent';
import { Registry } from '../../src/registry';

Given('a key pair', function() {
  this.keyPair = createKeyPair();
  expect(this.assessment.domain).to.equal(domain);
});

Given('an intention with the description {string}', function(description: string) {
  this.intention = createIntention(description);
});

When('the intention is created', function() {
  // Already handled in Given step
});

Then('the intention should have a unique content-based address', function() {
  expect(this.intention.address).to.be.a('string');
  expect(this.intention.address).to.not.be.empty;
});

Then('the intention\'s description should be {string}', function(description: string) {
  expect(this.intention.description).to.equal(description);
});

When('an agent is created from this key pair', function() {
  this.agent = createAgent(this.keyPair);
});

Then('the agent has a content-based address derived from its base state', function() {
  expect(this.agent.address).to.be.a('string');
  expect(this.agent.address).to.not.be.empty;
});

Then('the agent\'s public key can be retrieved from this address', function() {
  expect(this.agent.publicKey).to.deep.equal(this.keyPair.publicKey);
});

Then('the agent\'s signature of their own public key can be retrieved and verified', function() {
  const isValid = verifySignature(this.agent.publicKey, this.agent.signature, this.agent.publicKey);
  expect(isValid).to.be.true;
});

Given('an agent {string} exists', function(agentName: string) {
  const keyPair = createKeyPair();
  this.agent = createAgent(keyPair);
  this.agent.name = agentName;
});

When('{string} makes and signs a promise to fulfill the intention {string}', function(agentName: string, intentionDesc: string) {
  const intention = createIntention(intentionDesc);
  this.promise = makePromise(this.agent, intention);
});

Then('the promise should have a unique content-based address', function() {
  expect(this.promise.address).to.be.a('string');
  expect(this.promise.address).to.not.be.empty;
});

Then('the promise should be signed by {string}', function(agentName: string) {
  expect(this.promise.agentAddress).to.equal(this.agent.address);
  expect(this.promise.signature).to.be.a('string');
});

Then('the promise should reflect the intention {string}', function(intentionDesc: string) {
  expect(this.promise.description).to.equal(intentionDesc);
});

Then('the promise\'s signature can be verified using {string}\'s public key', function(agentName: string) {
  const isValid = verifySignature(this.agent.publicKey, this.promise.signature, this.promise.description);
  expect(isValid).to.be.true;
});

Given('an agent {string} has been created and has a content-based address', function(agentName: string) {
  const keyPair = createKeyPair();
  this.agent = createAgent(keyPair);
  this.agent.name = agentName;
});

When('the agent registers with the Registry service', function() {
  this.registry = new Registry();
  this.registry.register(this.agent);
});

Then('a REGISTER event is emitted, announcing that an agent with address {string} has come online', function(address: string) {
  const events = this.registry.getEvents();
  const registerEvent = events.find(e => e.type === 'REGISTER' && e.agentAddress === this.agent.address);
  expect(registerEvent).to.exist;
});
Then('other agents and services are informed of the new agent\'s existence', function() {
  const events = this.registry.getEvents();
  const notificationEvents = events.filter(e => e.type === 'NOTIFY' && e.agentAddress === this.agent.address);
  expect(notificationEvents.length).to.be.greaterThan(0);
});

Given('the following agents with their promises:', function(dataTable) {
  this.agents = {};
  dataTable.hashes().forEach((row) => {
    const keyPair = createKeyPair();
    const agent = createAgent(keyPair);
    agent.name = row['Agent'];
    const intention = createIntention(row['Promise']);
    const promise = makePromise(agent, intention);
    agent.promises = [promise];
    this.agents[row['Agent']] = agent;
  });
});

When('{string} declares {string} as its parent', function(childName: string, parentName: string) {
  const child = this.agents[childName];
  const parent = this.agents[parentName];
  child.parents = [parent.address];
});

Then('the promises {string} should be among them', function(promiseList: string) {
  const promises = promiseList.split(' and ').map(p => p.trim());
  const agent = this.agent || Object.values(this.agents)[0];
  const parentPromises = agent.parents?.flatMap(parentAddr => 
    Object.values(this.agents).find(a => a.address === parentAddr)?.promises || []
  ) || [];
  
  promises.forEach(promise => {
    const found = parentPromises.some(p => p.description === promise);
    expect(found).to.be.true;
  });
});

Then('the promise\'s signature can be verified using {string}\'s public key', function(agentName: string) {
  const isValid = verifySignature(this.agent.publicKey, this.promise.signature, this.promise.description);
  expect(isValid).to.be.true;
});

Given('an agent {string} assesses a promise made by {string}', function(assessorName: string, promiserName: string) {
  const assessorKeyPair = createKeyPair();
  this.assessor = createAgent(assessorKeyPair);
  this.assessor.name = assessorName;

  const promiserKeyPair = createKeyPair();
  this.promiser = createAgent(promiserKeyPair);
  this.promiser.name = promiserName;
  
  const intention = createIntention("test intention");
  this.promise = makePromise(this.promiser, intention);
});

When('{string} marks the promise as {string} in domain {string}', function(assessorName: string, status: string, domain: string) {
  this.assessment = createAssessment(this.assessor, this.promise, status as AssessmentStatus, domain);
});

Then('the assessment is recorded in domain {string}', function(domain: string) {
  expect(this.assessment.address).to.be.a('string');
  expect(this.assessment.promiseAddress).to.equal(this.promise.address);
  expect(this.assessment.assessorAddress).to.equal(this.assessor.address);
  expect(this.assessment.signature).to.be.a('string');
  expect(this.assessment.timestamp).to.be.a('number');
});

Then('{string} is notified of the {string} promise assessment', function(agentName: string, status: string) {
  this.registry.notifyAssessment(this.assessment);
  const events = this.registry.getEvents();
  const assessmentEvent = events.find(e => 
    e.type === 'ASSESSMENT' && 
    e.assessmentAddress === this.assessment.address &&
    e.status === status.toUpperCase()
  );
  expect(assessmentEvent).to.exist;
});
