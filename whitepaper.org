
#+html_head: <link rel="stylesheet" href="https://sandyuraz.com/styles/org.css">

#+TITLE: Agency Protocol Whitepaper
* Abstract

The Agency Protocol (AP) introduces a decentralized framework for assessing trust and credibility for general online resources across diverse domains. It defines a formal model for tracking promises and assessments thereof, such that their *fulfillment rate* can be determined objectively.  AP enables individuals and organizations to make data-driven, reliable decisions.

Unlike traditional reputation systems, which are often manipulated or lack context-specific precision, AP provides a verifiable method for measuring domain-specific merit. Agents—whether individuals, organizations, or even autonomous systems—cryptographically sign promises and are evaluated based on their performance. These actions create an immutable record of accountability and discourage manipulative behaviors by attaching real-world consequences to unfulfilled promises.

Merit within AP is contextual. An agent’s credibility in one domain (e.g., healthcare) does not automatically transfer to another (e.g., logistics), ensuring trust assessments are relevant and meaningful. Drawing inspiration from evolutionary principles such as inheritance, variation, and fitness, AP allows merit to evolve naturally as agents adapt and improve over time.

By aligning incentives with accountability, the Agency Protocol eliminates many weaknesses of centralized systems. It fosters an environment where agents have "skin in the game" and are rewarded for honest participation, enabling more trustworthy and efficient digital ecosystems.

* Introduction
** The Challenge of Trust in Digital Systems
Establishing trust in digital systems remains an ongoing challenge. Existing approaches to trust can be grouped into three main categories, each with notable weaknesses:

1. **Centralized Authorities**
   - Trust is managed by a single entity, such as a platform operator, government, or corporation.
   - **Examples**:
     - Social media platforms like Facebook control identity verification and content moderation.
     - Credit agencies like Experian determine financial trustworthiness.
   - **Weaknesses**:
     - Single points of failure: Platforms are vulnerable to outages, hacks, or censorship.
     - Risk of manipulation: Authorities can act with bias, corruption, or opaque decision-making.
     - Trust reliance: Users must trust the authority rather than transparent, verifiable actions.

2. **Traditional Reputation Systems**
   - Reputation is determined through simplistic scoring mechanisms, such as ratings, reviews, or upvotes.
   - **Examples**:
     - Star ratings on platforms like Amazon or Yelp.
     - Seller feedback systems on eBay or gig-economy apps like Upwork.
   - **Weaknesses**:
     - Easily gamed: Fake reviews, coordinated upvoting, or bot-driven manipulation skew results.
     - Lack of specificity: A single score often blends unrelated attributes (e.g., personality vs. skill).
     - Surface-level trust: Systems cannot uncover deeper patterns of failure, such as inconsistent reliability or hidden misconduct.

3. **Blockchain and Decentralized Alternatives**
   - Emerging systems leverage distributed ledgers to improve transparency and trust.
   - **Examples**:
     - DeFi protocols like Aave or Compound that rely on token staking for trust.
     - Blockchain-based identity systems like Sovrin or Worldcoin.
     - NFT marketplaces like OpenSea, where ownership is verifiable but quality is subjective.
   - **Weaknesses**:
     - Limited to financial or ownership proofs: These systems excel at tracking transactions but lack mechanisms for broader, contextual trust (e.g., expertise, reliability).
     - Lack of context-awareness: They do not differentiate domain-specific merit (e.g., reputation in healthcare vs. logistics).
     - Adoption barriers: Technical complexity and niche focus limit mainstream usability.

These limitations reveal a fundamental gap: the absence of a **generalizable, decentralized trust system** that can evaluate credibility across arbitrary domains, using both **verifiable actions** and **domain-specific merit**.

The **Agency Protocol (AP)** addresses this gap by introducing:
- A system where agents (individuals, organizations, or machines) can make **verifiable promises** tied to specific domains.
- **Cryptographically signed assessments** to determine whether promises are kept or broken.
- **Domain-specific merit scores** that accurately reflect an agent’s reliability and expertise within a given context.

Unlike its predecessors, AP combines:
- Transparency: Promises and outcomes are recorded publicly and immutably.
- Contextual Accuracy: Trustworthiness is evaluated within relevant domains.
- Incentive Alignment: Agents are economically and reputationally motivated to act honestly.

By building on these principles, the Agency Protocol creates a decentralized and adaptable foundation for establishing trust in a wide range of digital ecosystems.

** Promise Theory

The **Agency Protocol (AP)** builds its foundation on **Promise Theory**, a mathematical framework that formalizes promises and assessments thereof in order to provide a foundation of non-repudiable data upon which merit/credibility calculations can be based. In Promise Theory, agents-—whether individuals, organizations, or systems—-declare which commitments they are able to keep through explicit promises.  This is much different than typical online environments where promises are made implicitly (e.g. "best vegetarian restaurant in town") without direct accountability.  Promises in Agency Protocol are cryptographically signed, as are assessments made by other agents.  Because honest assessments have the effect of reducing uncertainty in the system, corroborated assessments are rewarded, while uncorroborated assessments are punished.  

This approach shifts the focus from *assumed trust* (relying on authority or reputation) to *earned trust* (built through actions). In AP:
- Promises are **cryptographically signed commitments**, ensuring they are authentic and non-repudiable.
- Fulfilled promises build a transparent and verifiable **track record** of credibility.
- Breaking promises carries consequences because actions are recorded on a **public ledger**, making deceit both visible and costly.

---

** Contextual Merit

In the Agency Protocol, **merit** is a domain-specific, quantifiable measure of credibility, grounded in an agent’s history of fulfilled promises. Unlike traditional reputation systems, merit avoids generalization and instead ties trust to specific contexts, making it **precise, verifiable, and meaningful**.

**Why Merit?**

The word *merit*** conveys a stronger and more universal idea than *reputation*. While reputation suggests **perception**, merit implies **demonstrable value**. It applies broadly to individuals, objects, systems, and even ideas:

1. **Merit for Individuals**:  
   Merit reflects an individual’s proven track record within a specific domain.  
   - *Example*: "Dr. M’s surgical outcomes have merit, as his success rate exceeds 98% across 1,000 procedures."  
   - *Real-World Usage*: "That candidate has the merit needed to lead this project."  

2. **Merit for Objects**:  
   Objects, like articles or products, can also be assessed for merit based on their quality and performance.  
   - *Example*: "The article’s merit lies in its factual accuracy, verified sources, and clarity."  
   - *Real-World Usage*: "This study has merit—it’s well-researched and supported by evidence."  

3. **Merit for Systems**:  
   Systems can earn merit by delivering consistent, reliable outcomes.  
   - *Example*: "The supply chain’s merit comes from a 99% on-time delivery rate across thousands of orders."  
   - *Real-World Usage*: "The program has merit—it streamlines operations effectively."  

4. **Merit for Ideas**:  
   Even abstract concepts or proposals can have merit if they demonstrate value or potential.  
   - *Example*: "The idea of incentivized reporting has merit because it encourages accountability without fear of retaliation."  
   - *Real-World Usage*: "That argument has merit—it’s logical and well-founded."

---

**Why Context Matters: Avoiding the One-Dimensional Trap**

Traditional reputation systems fail because they aggregate **irrelevant qualities** into a single score. This generalization can mask critical issues. Consider this example:

> *Dr. M is a surgeon known for his kind demeanor, leaving patients at ease during consultations. However, his surgical performance is inconsistent due to personal struggles. Most reviews reflect his personality, not his surgical skill. As a result, his general reputation remains positive while systemic failures go unnoticed.*

This example highlights two key problems with reputation systems:
- **Lack of Precision**: Combining unrelated qualities (e.g., *bedside manner*** vs. *surgical outcomes*) creates misleading assessments.  
- **Failure to Incentivize Reporting**: Observers hesitate to highlight issues due to fear or lack of reward.  

---

**How Agency Protocol Fixes This**  

The Agency Protocol introduces **merit** as a more accurate alternative to reputation:  

1. **Domain-Specific**: Merit ties credibility to a precise namespace.  
   - *Example*: Dr. M’s merit in *surgical skill*** is distinct from his merit in *bedside manner*.  

2. **Non-Gameable**: Merit reflects only verifiable, fulfilled promises, avoiding inflation through superficial interactions.  

3. **Incentivized for Honesty**:  
   - Observers can safely and anonymously report unfulfilled promises.  
   - Reporting is rewarded, encouraging truthfulness.  

For instance, in the healthcare context:  
- A surgeon’s **surgical merit** is informed by outcomes, peer assessments, and verified data.  
- Assistants who observe issues can report them anonymously, safeguarding their role and improving system accountability.

---


** Evolutionary Principles in Agent Dynamics

The Agency Protocol (AP) incorporates **evolutionary principles*** to ensure adaptability and resilience across the system. These principles—**heritability**, **variability**, and **fitness**—are inspired by natural selection, enabling agents to evolve over time and align more effectively with system goals.

**Heritability***  
Agents can inherit promises and attributes from **parent agents**. This allows proven behaviors and traits to propagate within the system.  
- *Example*: A “dog” agent inherits a promise of **having a pulse*** because all metabolic life forms share this attribute. If a parent agent (e.g., *Golden Retriever*) makes specific promises (e.g., “obedient,” “friendly”), those promises may also propagate to child agents (e.g., *Old Yeller*).

**Variability***  
Agents are not static. They can **introduce variations**, adopting new promises or behaviors to adapt to changing circumstances. This variability drives innovation and ensures the system evolves in response to new challenges.  
- *Example*: A robotic agent initially promises to "move forward" but evolves to "navigate obstacles" after repeated environmental failures.  

**Fitness***  
Agents are subject to a form of **selection*** based on their merit. Agents that consistently fulfill their promises and maintain high merit scores are more likely to be trusted and selected for further interactions.  
- *Example*: In a competitive environment, an agent with a high merit score for “accurate delivery” is selected more frequently over others, akin to how fitter organisms reproduce more successfully in nature.

**Why This Matters***  
By applying these evolutionary principles, AP fosters a **self-improving ecosystem*** where successful traits propagate, innovation thrives, and agents adapt to align better with real-world demands. This creates a dynamic, robust system resilient to failure and responsive to participant values.

---

The following diagram demonstrates how promises propagate from parent agents to child agents, clarifying the concept of **heritability**.

#+begin_src mermaid :file agent-promise-inheritance.png
classDiagram
    class LifeForm {
        + Promise: "Has a Pulse"
    }

    class Dog {
        + Promise: "Has a Pulse"
        + Promise: "Loyal"
    }

    class OldYeller {
        + Promise: "Has a Pulse"
        + Promise: "Loyal"
        + Promise: "Protective"
    }

    LifeForm <|-- Dog
    Dog <|-- OldYeller

    LifeForm : "Has a Pulse"
    Dog : "Loyal"
    OldYeller : "Protective"
#+end_src

* Evidence

While most assessments in the Agency Protocol rely on subjective experience, **evidence** provides an additional layer of verifiability where it is needed. It supports trust in promises where direct observation or experience alone is insufficient. By introducing structured requirements for evidence in specific domains, the protocol ensures that more critical or high-impact promises can be objectively verified.

** Types of Evidence

1. **Experience-Based Assessments**  
   - Most promises are assessed based on **direct experience**, requiring no formal evidence.  
   - Assessors provide evaluations (e.g., "KEPT" or "BROKEN") informed by their interaction with the promise.  
   - Weighting factors such as the assessor's merit, past accuracy, and recency of assessments ensure credibility.

   *Example*: A restaurant promises "Authentic Italian taste." Customers assess the promise based on their experience without providing formal evidence.

2. **Automatic Evidence**  
   - System-generated data (e.g., timestamps, GPS logs, sensor readings) is automatically verified.  
   - No manual intervention is required unless anomalies are detected.

   *Example*: A delivery service promises "Delivery within 45 minutes," which is verified through order and delivery timestamps.

3. **Validated Evidence**  
   - Requires submission of tangible evidence (e.g., documents, photos, receipts) that is reviewed by human validators.  
   - Validators are agents with sufficient domain-specific merit, ensuring qualified assessment.

   *Example*: A vendor promises "Fresh ingredients delivered daily" and submits supplier receipts and photos for review.

4. **Mixed Evidence**  
   - Combines automatic and validated evidence to strengthen verification.  
   - Automatic checks flag anomalies, which are escalated to human validators.

   *Example*: "Same day lab results" may use timestamps for automatic validation and require lab reports for final confirmation.

5. **Progressive Evidence**  
   - Agents with lower merit in a domain must provide stricter or more comprehensive evidence to compensate for lower trust.  
   - Evidence requirements scale with the agent's domain credibility.

   *Example*: A new vendor with low domain merit must provide independent third-party validation, while an established vendor may only require standard evidence.

---

** Evidence Requirements by Promise Type

| **Promise Type**               | **Evidence Requirement**            | **Verification Method**         |  
|--------------------------------+-------------------------------------+---------------------------------|  
| Experience-Based               | None required                      | Assessor judgment               |  
| Time-Bound (e.g., delivery)    | Automated timestamps               | System verification             |  
| Tangible Deliverables          | Receipts, images, or documents     | Validator group review          |  
| High-Impact Commitments        | Progressive requirements           | Combination of methods          |  

---

** Experience vs Evidence

The Agency Protocol recognizes that **most trust emerges from experience**. Assessments based on direct interaction are sufficient for many promises, particularly those involving subjective outcomes or day-to-day interactions.  

However, evidence becomes essential when:  
- A promise has **high economic or reputational impact**.  
- Assessments require **objective verification** beyond individual judgment.  
- An agent's **merit is insufficient** to inspire default trust.  

By balancing experiential assessments and evidence-based verification, the protocol ensures flexibility while maintaining rigorous standards for promises that demand higher scrutiny.

---

** Validator Requirements

For validated evidence, human validators play a key role:  
- Validators must possess **high domain merit** to ensure credibility.  
- They **stake credit** proportional to the promise's impact to align incentives.  
- Validator groups require **minimum participation thresholds** to prevent bias.  

*Example*:  
| **Domain**                     | **Min Validators** | **Merit Threshold** |  
|--------------------------------+--------------------+---------------------|  
| Food Quality                   | 3                  | >85                 |  
| Healthcare Lab Results         | 5                  | >90                 |  

---

** Evidence and Merit

The quality and type of evidence influence merit updates:  

| **Aspect**       | **Impact on Merit**             |  
|-------------------+---------------------------------|  
| High-Quality Evidence | Full positive merit update.   |  
| Partial or Delayed    | Reduced positive impact.      |  
| Low-Quality Evidence  | No merit change or negative impact. |  

For agents with lower merit, rigorous evidence helps them earn trust and credibility over time.

---

** Storage and Access

Evidence storage ensures integrity, privacy, and accessibility:  

| **Type**          | **Storage**                | **Access**                      |  
|-------------------+---------------------------+--------------------------------|  
| Public Evidence    | Publicly available.       | Open to all agents.             |  
| Private Evidence   | Encrypted and restricted. | Accessible to validators only.  |  
| Sensitive Evidence | Special controls applied. | Restricted to high-trust agents.|  

Content-addressed storage guarantees immutability, while role-based access control ensures privacy where necessary.

---

** Evidence in Batch Processing

For validated or mixed evidence, batch processing improves scalability:  

| **Step**           | **Action**                             |  
|--------------------+----------------------------------------|  
| Collection         | Gather evidence submitted by agents.  |  
| Validation         | Route evidence to appropriate validators. |  
| Assessment         | Aggregate validator consensus.        |  
| Merit Update       | Apply changes based on validation results. |  

Batch processing ensures efficient updates without overwhelming the system.  

---

By structuring evidence requirements to align with promise types, impact, and agent merit, the Agency Protocol balances flexibility with verifiability. This approach allows experiential trust to flourish where sufficient while introducing rigorous verification only when necessary.  
* Scalability and Performance

The Agency Protocol (AP) ensures scalability and performance across growing networks by focusing on efficient **technical infrastructure** and **resource utilization**. AP leverages a distributed architecture to optimize data storage, query processing, and performance under increasing load.

** Distributed Storage Architecture

AP employs a multi-tiered storage system to manage frequently accessed data (hot) and archival data (cold):  

- **Hot Path**: Stores active promises, recent assessments, and current merit for low-latency access.  
- **Cold Path**: Uses distributed, immutable content-addressed storage for historical promises and assessments.  

#+NAME: storage-diagram  
#+BEGIN_SRC mermaid :file storage_hierarchy.png  
classDiagram  
    class HotPath {  
        + Active Promises  
        + Recent Assessments  
        + Current Merit  
    }  
    class ColdPath {  
        + Historical Promises  
        + Old Assessments  
        + Merit History  
    }  
    HotPath --> ColdPath : "Data Aging"  
#+END_SRC  

** Performance Optimization

Performance is enhanced through key strategies:  

- **Caching**:  
  - Local Caching: Agents locally cache frequently accessed data.  
  - Predictive Pre-fetching: Anticipates data needs based on usage patterns.  

- **Query Efficiency**:  
  - **Indexing**: Promises and assessments are indexed for rapid retrieval.  
  - **Lazy Evaluation**: Defers computations until explicitly required.  

- **Batch Processing**: Groups updates to reduce overhead, e.g., batched merit calculations.

** Throughput and Latency Management

- **Asynchronous Processing**: Promises and assessments are handled independently to prevent system bottlenecks.  
- **Parallel Validation**: Merit calculations and state verifications are distributed across nodes.  
- **Latency Optimization**:  
  - **Edge Computing**: Processes data closer to agents to reduce latency.  
  - **Optimized Routing**: Ensures the shortest communication path.  

** Resource Utilization

AP dynamically scales resources to match system demand:  

- **Load Balancing**: Distributes workloads evenly across nodes.  
- **Adaptive Scaling**: Expands or contracts resource usage in response to load.  
- **Resource Pooling**: Shared resource pools maximize efficiency.  

Performance metrics include CPU efficiency, storage utilization, and cache hit rates to monitor resource effectiveness.

---

* Applications

The following real-world applications demonstrate how the Agency Protocol's evidence framework ensures accountability, trust, and performance.

** Healthcare Provider Trust Network

In decentralized healthcare systems, providers establish verifiable trust through **vouching**, **assessments**, and **evidence-based outcomes**.

*** Vouching for New Providers  

Senior healthcare providers can vouch for new physicians, staking part of their merit:  

| **Component**         | **Requirement**                         |  
| Voucher               | Experienced provider's address          |  
| Basis                 | Direct supervision or collaboration     |  
| Stake                 | Portion of voucher's merit (e.g., 20%)  |  

**Example**:  
Dr. Chen vouches for Dr. Rodriguez, citing "direct supervision during residency." Dr. Rodriguez gains initial merit in *internal_medicine*, partially tied to Dr. Chen's reputation.

*** Merit and Vouching Chain Impact  

If Dr. Rodriguez underperforms, the negative impact propagates through the vouching chain:  

| **Provider**          | **Merit Impact**                       |  
| Dr. Rodriguez         | Direct impact                         |  
| Dr. Chen              | Proportional reduction (20% stake)    |  

This ensures accountability and encourages careful vouching.

*** Surgical Verification with Evidence  

Evidence requirements for surgical procedures vary based on provider merit:  

| **Merit Range**       | **Evidence Required**                                      |  
| Below 60              | Video recording, detailed notes, supervision              |  
| 60–80                 | Peer-reviewed notes, photo documentation, outcomes        |  
| Above 80              | Standard notes and outcome tracking                       |  

This progressive system ensures trust while reducing unnecessary overhead for high-merit providers.

---

** Scientific Research and Funding Transparency

The Agency Protocol fosters **merit-based research funding** and ensures results are transparent, verifiable, and reproducible.

*** Research Proposal Promises  

Researchers make explicit promises about their work:  

| **Promise Type**      | **Content**                                   |  
| Methodology           | Rigorous experimental design                 |  
| Transparency          | Publish raw data, assumptions, and limitations |  
| Reproducibility       | Provide all scripts and protocols            |  

Funding decisions depend on promise quality, past performance, and public assessments.

*** Evidence for Groundbreaking Claims  

Extraordinary claims require extraordinary evidence:  

| **Claim Type**                  | **Evidence Requirements**                          |  
| Room Temperature Superconductor | Raw data, live demonstrations, independent replication |  
| Unexpected Material Properties  | Extensive monitoring, cross-lab validations          |  

If claims fail replication:  
- Merit penalties apply.  
- Future claims demand stricter evidence.  

*** Addressing the Replication Crisis  

AP tracks replication attempts for studies:  

| **Aspect**             | **Evidence Detail**                              |  
| Methods Match          | Original protocol adherence                      |  
| Results Match          | Degree of alignment with original findings       |  
| Deviations             | Necessary changes in methods                    |  

Failed replications trigger a review of the original study and its promises, ensuring result reliability.

---

** Supply Chain Trust and Traceability

In supply chain ecosystems, the AP enables verifiable **product traceability** and **quality assurance**.  

*** Product Traceability  

At each stage, participants make promises backed by evidence:  

| **Stage**            | **Promise**                      | **Evidence**                       |  
| Raw Material         | Ethical sourcing                 | Certification, audit reports       |  
| Manufacturing        | Labor compliance                 | Worker records, audits             |  
| Logistics            | Delivery timelines               | GPS timestamps, delivery logs      |  

*** Sustainable Practices Verification  

Suppliers must provide evidence of sustainability:  

| **Category**         | **Metrics**                      | **Evidence**                       |  
| Water Usage          | Liters per kg of product         | IoT sensor data, audit reports     |  
| Carbon Footprint     | CO2 emissions                    | Measurement logs, certificates     |  

Validators assess evidence quality, ensuring sustainability claims are verifiable.

---

** Professional Services and Credential Validation

Professionals use the Agency Protocol to verify skills, credentials, and reliability.  

*** Skill Verification  

| **Promise**           | **Evidence**                      |  
| Code Quality          | Code reviews, test coverage       |  
| Project Deadlines     | Delivery logs, milestone reports  |  

Clients and peers assess these promises, weighted by their merit in relevant domains.  

*** Experience Validation  

Time-stamped records and peer corroboration ensure that professional experience is verifiable over time.

---

** Collaborative Systems and Teams

Teams can align efforts and build trust through collective promises:  

| **Promise Type**      | **Evidence**                      |  
| Milestone Completion  | Task logs, verified deliverables  |  
| Collaboration Quality | Peer assessments, meeting logs    |  

---

The Agency Protocol's structured approach to evidence enhances trust, accountability, and transparency across various domains. From supply chains to professional services and governance systems, AP ensures that promises are upheld, assessed, and verifiable, creating a robust foundation for trust-based ecosyste
