import React, { useState } from 'react';

interface PromiseCreationFormProps {
  onSubmit: (data: { description: string; creditStake?: number }) => void;
}

export const PromiseCreationForm: React.FC<PromiseCreationFormProps> = ({ onSubmit }) => {
  const [formData, setFormData] = useState({
    description: '',
    creditStake: ''
  });

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onSubmit({
      description: formData.description,
      creditStake: formData.creditStake ? Number(formData.creditStake) : undefined
    });
  };

  return (
    <form onSubmit={handleSubmit} className="space-y-4">
      <div>
        <label htmlFor="description" className="block text-sm font-medium">
          Promise Description
        </label>
        <input
          type="text"
          id="description"
          value={formData.description}
          onChange={(e) => setFormData(prev => ({ ...prev, description: e.target.value }))}
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm"
        />
      </div>
      <div>
        <label htmlFor="creditStake" className="block text-sm font-medium">
          Credit Stake (optional)
        </label>
        <input
          type="number"
          id="creditStake"
          value={formData.creditStake}
          onChange={(e) => setFormData(prev => ({ ...prev, creditStake: e.target.value }))}
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm"
        />
      </div>
      <button
        type="submit"
        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700"
      >
        Create Promise
      </button>
    </form>
  );
};
