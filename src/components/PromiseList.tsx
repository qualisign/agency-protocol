import React from 'react';

interface Promise {
  address: string;
  description: string;
  status: string;
}

interface PromiseListProps {
  promises: Promise[];
}

export const PromiseList: React.FC<PromiseListProps> = ({ promises }) => {
  return (
    <div className="space-y-4">
      <h3 className="text-lg font-medium">Promises</h3>
      <div className="divide-y divide-gray-200">
        {promises.map((promise) => (
          <div key={promise.address} className="py-4">
            <div className="flex justify-between">
              <div>
                <p className="text-sm font-medium">{promise.description}</p>
                <p className="text-sm text-gray-500">
                  Status: {promise.status}
                </p>
              </div>
              <div className="text-sm text-gray-500">
                {promise.address}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
