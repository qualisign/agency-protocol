import React, { useState } from 'react';

interface Agent {
  name: string;
  tags: string[];
  address: string;
}

interface AgentSearchProps {
  onSearch: (tags: string[]) => void;
  agents: Agent[];
  isLoading?: boolean;
}

export const AgentSearch: React.FC<AgentSearchProps> = ({ onSearch, agents, isLoading }) => {
  const [searchTags, setSearchTags] = useState('');

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const tags = searchTags.split(',').map(tag => tag.trim()).filter(Boolean);
    onSearch(tags);
  };

  return (
    <div className="space-y-4">
      <form onSubmit={handleSubmit} className="flex gap-2">
        <input
          type="text"
          value={searchTags}
          onChange={(e) => setSearchTags(e.target.value)}
          placeholder="Search tags (comma separated)"
          className="flex-1 rounded-md border-gray-300 shadow-sm"
          disabled={isLoading}
        />
        <button
          type="submit"
          disabled={isLoading}
          className="px-4 py-2 bg-blue-600 text-white rounded-md hover:bg-blue-700 disabled:opacity-50"
        >
          Search
        </button>
      </form>

      <div className="divide-y divide-gray-200">
        {agents.map((agent) => (
          <div key={agent.address} className="py-4">
            <p className="font-medium">{agent.name}</p>
            <p className="text-sm text-gray-500">Tags: {agent.tags.join(', ')}</p>
            <p className="text-sm text-gray-500">Address: {agent.address}</p>
          </div>
        ))}
      </div>
    </div>
  );
};
