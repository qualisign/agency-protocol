import React from 'react';

interface Assessment {
  address: string;
  promiseAddress: string;
  status: string;
  timestamp: number;
}

interface AssessmentListProps {
  assessments: Assessment[];
}

export const AssessmentList: React.FC<AssessmentListProps> = ({ assessments }) => {
  return (
    <div className="space-y-4">
      <h3 className="text-lg font-medium">Assessments</h3>
      <div className="divide-y divide-gray-200">
        {assessments.map((assessment) => (
          <div key={assessment.address} className="py-4">
            <div className="flex justify-between">
              <div>
                <p className="text-sm font-medium">Promise: {assessment.promiseAddress}</p>
                <p className="text-sm text-gray-500">
                  Status: {assessment.status}
                </p>
              </div>
              <div className="text-sm text-gray-500">
                {new Date(assessment.timestamp).toLocaleDateString()}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
