import React, { useState } from 'react';

interface AssessmentFormProps {
  onSubmit: (data: { 
    promiseAddress: string;
    status: 'KEPT' | 'BROKEN';
    evidence?: { type: string; data: any };
  }) => void;
  isLoading?: boolean;
  error?: string;
}

export const AssessmentForm: React.FC<AssessmentFormProps> = ({ onSubmit, isLoading, error }) => {
  const [formData, setFormData] = useState({
    promiseAddress: '',
    status: 'KEPT' as 'KEPT' | 'BROKEN',
    evidenceType: '',
    evidenceData: ''
  });

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onSubmit({
      promiseAddress: formData.promiseAddress,
      status: formData.status,
      evidence: formData.evidenceType ? {
        type: formData.evidenceType,
        data: formData.evidenceData
      } : undefined
    });
  };

  return (
    <form onSubmit={handleSubmit} className="space-y-4">
      <div>
        <label htmlFor="promiseAddress" className="block text-sm font-medium">
          Promise Address
        </label>
        <input
          type="text"
          id="promiseAddress"
          value={formData.promiseAddress}
          onChange={(e) => setFormData(prev => ({ ...prev, promiseAddress: e.target.value }))}
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm"
          disabled={isLoading}
        />
      </div>
      <div>
        <label htmlFor="status" className="block text-sm font-medium">
          Status
        </label>
        <select
          id="status"
          value={formData.status}
          onChange={(e) => setFormData(prev => ({ ...prev, status: e.target.value as 'KEPT' | 'BROKEN' }))}
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm"
          disabled={isLoading}
        >
          <option value="KEPT">Kept</option>
          <option value="BROKEN">Broken</option>
        </select>
      </div>
      {error && <p className="text-red-500 text-sm">{error}</p>}
      <button
        type="submit"
        disabled={isLoading}
        className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 disabled:opacity-50"
      >
        {isLoading ? 'Submitting...' : 'Submit Assessment'}
      </button>
    </form>
  );
};
