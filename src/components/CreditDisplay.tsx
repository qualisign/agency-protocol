import React from 'react';

interface CreditDisplayProps {
  credit: number;
  lockedCredit: number;
  availableCredit: number;
}

export const CreditDisplay: React.FC<CreditDisplayProps> = ({ credit, lockedCredit, availableCredit }) => {
  return (
    <div className="p-4 bg-white rounded-lg shadow">
      <h3 className="text-lg font-medium mb-4">Credit Status</h3>
      <div className="grid grid-cols-3 gap-4">
        <div>
          <p className="text-sm text-gray-500">Total Credit</p>
          <p className="text-2xl font-bold">{credit}</p>
        </div>
        <div>
          <p className="text-sm text-gray-500">Locked</p>
          <p className="text-2xl font-bold">{lockedCredit}</p>
        </div>
        <div>
          <p className="text-sm text-gray-500">Available</p>
          <p className="text-2xl font-bold">{availableCredit}</p>
        </div>
      </div>
    </div>
  );
};
