export class Registry {
  private events: any[] = [];

  register(agent: any) {
    this.events.push({
      type: 'REGISTER',
      agentAddress: agent.address,
      timestamp: Date.now()
    });

    // Notify other agents/services
    this.events.push({
      type: 'NOTIFY',
      agentAddress: agent.address,
      timestamp: Date.now()
    });
  }

  notifyAssessment(assessment: any) {
    this.events.push({
      type: 'ASSESSMENT',
      assessmentAddress: assessment.address,
      promiseAddress: assessment.promiseAddress,
      status: assessment.status,
      timestamp: Date.now()
    });
  }

  getEvents() {
    return this.events;
  }
}
