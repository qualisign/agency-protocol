import { createKeyPair, createAgent as createAgentCore, Agent, Promise, Assessment, Evidence } from './agent';
import { Registry } from './registry';

const registry = new Registry();

interface APIResponse {
  status: number;
  data: any;
}

export async function createAgent(data: { publicKey: string, signature: string }): Promise<APIResponse> {
  const keyPair = createKeyPair();
  const agent = createAgentCore(keyPair);
  registry.register(agent);
  
  return {
    status: 201,
    data: { address: agent.address }
  };
}

export async function createPromise(data: { 
  agentAddress: string, 
  description: string, 
  signature: string,
  intentionAddress?: string,
  creditStake?: number
}): Promise<APIResponse> {
  // Mock promise creation
  const promise = {
    address: `promise-${Date.now()}`,
    description: data.description,
    agentAddress: data.agentAddress,
    signature: data.signature,
    intentionAddress: data.intentionAddress || `intention-${Date.now()}`,
    creditStake: data.creditStake
  };

  return {
    status: 201,
    data: { address: promise.address }
  };
}

export async function createAssessment(data: {
  promiseAddress: string,
  assessorAddress: string,
  status: string,
  signature: string,
  evidence?: Evidence
}): Promise<APIResponse> {
  // Mock assessment creation
  const assessment = {
    address: `assessment-${Date.now()}`,
    promiseAddress: data.promiseAddress,
    assessorAddress: data.assessorAddress,
    status: data.status,
    signature: data.signature,
    timestamp: Date.now(),
    evidence: data.evidence
  };

  return {
    status: 201,
    data: { address: assessment.address }
  };
}

export async function getAgentPromises(agentAddress: string): Promise<APIResponse> {
  // Mock getting agent's promises
  const mockPromises = [
    {
      address: 'promise-1',
      description: 'Build shelter',
      status: 'KEPT'
    },
    {
      address: 'promise-2',
      description: 'Gather resources',
      status: 'IN_PROGRESS'
    }
  ];

  return {
    status: 200,
    data: { promises: mockPromises }
  };
}

export async function getAgentAssessments(agentAddress: string): Promise<APIResponse> {
  // Mock getting agent's assessments
  const mockAssessments = [
    {
      address: 'assessment-1',
      promiseAddress: 'promise-1',
      status: 'KEPT',
      timestamp: Date.now() - 86400000
    },
    {
      address: 'assessment-2',
      promiseAddress: 'promise-2',
      status: 'IN_PROGRESS',
      timestamp: Date.now()
    }
  ];

  return {
    status: 200,
    data: { assessments: mockAssessments }
  };
}

export async function getAgentCredit(agentAddress: string): Promise<APIResponse> {
  // Mock getting agent's credit
  return {
    status: 200,
    data: { 
      credit: 100,
      lockedCredit: 20,
      availableCredit: 80
    }
  };
}

export async function updateAgent(data: {
  agentAddress: string,
  newPromises: Promise[],
  signature: string
}): Promise<APIResponse> {
  // Mock agent update
  return {
    status: 200,
    data: {
      address: `${data.agentAddress}-updated`,
      timestamp: Date.now()
    }
  };
}
