export type ConsensusState = 'Gathering' | 'Discussing' | 'Achieved';

export interface ConsensusDecision {
  topic: string;
  scope: 'identity' | 'structure' | 'intent';
  participants: string[]; // Agent addresses
  startTime: number;
  state: ConsensusState;
  contributions?: Contribution[];
  decision?: string;
  completedAt?: number;
}

export interface Contribution {
  agentAddress: string;
  content: string;
  timestamp: number;
  references?: string[]; // References to other contributions
}

export class ConsensusAgent {
  private decisions: Map<string, ConsensusDecision> = new Map();

  initiateConsensus(topic: string, scope: ConsensusDecision['scope'], participants: string[]): string {
    const decisionId = `decision-${Date.now()}`;
    const decision: ConsensusDecision = {
      topic,
      scope,
      participants,
      startTime: Date.now(),
      state: 'Gathering'
    };
    this.decisions.set(decisionId, decision);
    return decisionId;
  }

  contribute(decisionId: string, agentAddress: string, content: string, references?: string[]): void {
    const decision = this.decisions.get(decisionId);
    if (!decision) throw new Error('Decision not found');
    if (!decision.participants.includes(agentAddress)) throw new Error('Agent not authorized');
    
    const contribution: Contribution = {
      agentAddress,
      content,
      timestamp: Date.now(),
      references
    };
    
    decision.contributions = decision.contributions || [];
    decision.contributions.push(contribution);
  }

  getDecision(decisionId: string): ConsensusDecision | undefined {
    return this.decisions.get(decisionId);
  }
}
