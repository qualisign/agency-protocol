export interface KeyPair {
  publicKey: string;
  privateKey: string;
}

export interface Agent {
  address: string;
  publicKey: string;
  signature: string;
  name?: string;
  parents?: string[]; // Parent agent addresses
  promises?: Promise[];
}

export interface Intention {
  address: string;
  description: string;
}

export function createKeyPair(): KeyPair {
  // TODO: Implement actual cryptographic key pair generation
  return {
    publicKey: 'mock-public-key',
    privateKey: 'mock-private-key'
  };
}

export function createAgent(keyPair: KeyPair, parents?: Agent[]): Agent {
  const publicKey = keyPair.publicKey;
  // TODO: Implement actual content-based addressing
  const address = `agent-${Math.random().toString(36).substring(7)}`;
  // TODO: Implement actual signature generation
  const signature = 'mock-signature';
  
  return {
    address,
    publicKey,
    signature,
    parents: parents?.map(p => p.address),
    promises: []
  };
}

export function createIntention(description: string): Intention {
  // TODO: Implement actual content-based addressing
  const address = `intention-${Math.random().toString(36).substring(7)}`;
  
  return {
    address,
    description
  };
}

export type AssessmentStatus = 'KEPT' | 'BROKEN';

export interface Assessment {
  address: string;
  promiseAddress: string;
  assessorAddress: string;
  status: AssessmentStatus;
  signature: string;
  timestamp: number;
  domain?: string; // Domain/namespace for merit tracking
  evidence?: Evidence;
}

export interface Evidence {
  type: 'automated_timestamps' | 'supplier_receipts' | 'customer_experience' | 'documented';
  data: any;
  timestamp: number;
  validatorAddresses?: string[];
}

export function createAssessment(
  assessor: Agent, 
  promise: Promise, 
  status: AssessmentStatus, 
  domain?: string,
  evidence?: Evidence
): Assessment {
  // TODO: Implement actual content-based addressing and signing
  const address = `assessment-${Math.random().toString(36).substring(7)}`;
  const signature = 'mock-signature';
  
  return {
    address,
    promiseAddress: promise.address,
    assessorAddress: assessor.address,
    status,
    signature,
    timestamp: Date.now(),
    domain,
    evidence
  };
}

export interface Promise {
  address: string;
  description: string;
  agentAddress: string;
  signature: string;
  intentionAddress: string;
  creditStake?: number;
}

export interface Credit {
  amount: number;
  owner: string;
  locked: boolean;
}

export interface PaymentIntention extends Intention {
  type: 'credit_payment' | 'bitcoin_payment' | 'mutual_credit';
  amount: number;
  recipient?: string;
}

export function makePromise(agent: Agent, intention: Intention): Promise {
  // TODO: Implement actual content-based addressing and signing
  const address = `promise-${Math.random().toString(36).substring(7)}`;
  const signature = 'mock-signature';
  
  return {
    address,
    description: intention.description,
    agentAddress: agent.address,
    signature,
    intentionAddress: intention.address
  };
}

export function verifySignature(publicKey: string, signature: string, data: string): boolean {
  // TODO: Implement actual signature verification
  return true;
}
