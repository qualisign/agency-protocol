import { useState, useEffect, useRef } from "react";

// Custom Hooks for color animation
const useMorphicColor = (baseColors, duration = 15000) => {
    const [colors, setColors] = useState(baseColors);

    useEffect(() => {
        const interval = setInterval(() => {
            const phi = (1 + Math.sqrt(5)) / 2;
            const time = Date.now() / duration;
            const hueShift = Math.sin(time * phi) * 10;

            const newColors = baseColors.map((color) =>
                color.replace(
                    /hsl\((\d+),\s*(\d+)%,\s*(\d+)%/,
                    (_, h, s, l) =>
                        `hsl(${(parseFloat(h) + hueShift).toFixed(1)}, ${s}%, ${l}%)`
                )
            );
            setColors(newColors);
        }, 100);

        return () => clearInterval(interval);
    }, [baseColors, duration]);

    return colors;
};

// Icons
const GoalsIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M50,20 L50,50 L70,70" />
    </svg>
);

const ReprogrammingIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <path d="M50,20 L76,35 L76,65 L50,80 L24,65 L24,35 Z" />
        <path d="M37,50 L50,57.5 L63,50 L50,42.5 Z" fill="currentColor" />
    </svg>
);

// Renamed from Training to Learn on individual side
const LearnIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M35,65 L35,55 L50,55 L50,45 L65,45 L65,35" />
    </svg>
);

const EventsIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <path d="M50,20 L58.66,65 L41.34,65 Z M24,35 L76,35 L50,80 Z" />
    </svg>
);

const AgentsIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="35" cy="35" r="5" fill="currentColor" />
        <circle cx="65" cy="35" r="5" fill="currentColor" />
        <circle cx="50" cy="65" r="5" fill="currentColor" />
        <path d="M35,35 L65,35 L50,65 L35,35" />
    </svg>
);

const EvidenceIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <path d="M30,30 L70,30 L70,70 L30,70 Z" />
        <path d="M35,35 L65,35 M35,45 L65,45 M35,55 L65,55 M35,65 L65,65" />
    </svg>
);

const InvestingIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <path d="M50,20 L76,35 L76,65 L50,80 L24,65 L24,35 Z" />
        <path d="M50,35 L63,50 L50,65 L37,50 Z" />
        <circle cx="50" cy="50" r="5" fill="currentColor" />
    </svg>
);

// Consulting icon (replacing Training on collective side)
const ConsultingIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M40,50 L60,50" />
    </svg>
);

// Evidence sub-items icons
const LLMIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <text x="50%" y="54%" dominantBaseline="middle" textAnchor="middle" fontSize="20" fill="currentColor">LLM</text>
    </svg>
);

const ABMIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M30,50 L50,30 L70,50 L50,70 Z" />
    </svg>
);

const ProofIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <circle cx="50" cy="35" r="4" />
        <circle cx="40" cy="55" r="4" />
        <circle cx="60" cy="55" r="4" />
    </svg>
);

const VouchingIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M40,55 L50,65 L60,55" />
    </svg>
);

// Events sub-items: 
// Group hypnosis unchanged
const GroupHypnosisIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M50,20 L50,80" />
    </svg>
);

// Change to circle with vertical line and add "(de-hypnosis)"
const GroupSensemakingIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M20,50 L80,50" />
    </svg>
);

// Learn sub-items (individual side)
const CoursesIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <rect x="30" y="30" width="40" height="40" />
        <path d="M50,30 L50,70" />
    </svg>
);

const TutorialsLearnIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <rect x="30" y="30" width="40" height="40" />
        <path d="M30,50 L70,50" />
    </svg>
);

const DocsIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <path d="M35,30 L65,30 L65,70 L35,70 Z" />
        <path d="M40,40 L60,40 M40,50 L60,50" />
    </svg>
);

const MentoringIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <path d="M50,20 L76,35 L76,65 L50,80 L24,65 L24,35 Z" />
        <circle cx="50" cy="50" r="5" fill="currentColor" />
    </svg>
);

// Investing sub-items
const BusinessPlanIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <rect x="30" y="30" width="40" height="40" />
        <path d="M35,35 L65,35 M35,45 L65,45 M35,55 L65,55" />
    </svg>
);

const InquiriesIcon = (
    <svg width="40" height="40" viewBox="0 0 100 100" stroke="currentColor" fill="none" strokeWidth="2">
        <circle cx="50" cy="50" r="30" />
        <path d="M50,35 L50,55 L60,65" />
    </svg>
);

// Agents items
const AgentsItems = [
    { label: "agent", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/agent.feature" },
    { label: "credit", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/credit.feature" },
    { label: "merit", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/merit.feature" },
    { label: "decision", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/decisionmaking/decision.feature" },
    { label: "skin in the game", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/skin_in_the_game.feature" },
    { label: "data as labor", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/data_as_labor.feature" },
    { label: "infrastructure", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/infrastructure/infrastructure.feature" },
    { label: "security", url: "https://gitlab.com/qualisign/agency-protocol/-/blob/main/features/security.feature" },
];

function AgentItemIcon({ label }) {
    return (
        <svg width="40" height="40" viewBox="0 0 100 100" fill="none" stroke="currentColor" strokeWidth="2">
            <circle cx="50" cy="50" r="30" />
            <text x="50%" y="54%" dominantBaseline="middle" textAnchor="middle" fontSize="12" fill="currentColor">
                {label[0].toUpperCase()}
            </text>
        </svg>
    );
}

// Utility function to send email
const sendEmail = () => {
    window.location.href = "mailto:agency.gaitway@gmail.com";
};

const AlchemicalSection = ({
    type,
    colors,
    topText,
    bottomText,
    hovered,
    onMouseEnter,
    onMouseLeave,
    items,
    baseColor,
    hoverColor,
    onItemClick
}) => {
    const verticalLetters = type.toUpperCase().slice(0, 5).split("");
    const horizontalLetters = type.toUpperCase().slice(5, -1).split("");

    const lettersNormalOpacity = 0.7;
    const lettersHoveredOpacity = 0.14;

    return (
        <div
            className="relative overflow-visible p-12 min-h-full text-white"
            style={{
                background: `linear-gradient(135deg, ${colors.join(", ")})`,
                transition: "background 0.3s ease-in-out"
            }}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
        >
            <div
                className="relative mx-auto"
                style={{
                    width: "300px",
                    height: "300px",
                    position: "relative"
                }}
            >
                {/* Letters Layer */}
                <div
                    className="absolute inset-0 flex items-center justify-center"
                    style={{
                        zIndex: 1,
                        opacity: hovered ? lettersHoveredOpacity : lettersNormalOpacity,
                        filter: hovered ? "blur(2px)" : "none",
                        transition: "opacity 0.3s ease-in-out, filter 0.3s ease-in-out"
                    }}
                >
                    <div className="relative w-full h-full">
                        {/* Vertical letters */}
                        <div className="absolute left-0 flex flex-col justify-between h-full">
                            {verticalLetters.map((letter, index) => (
                                <span
                                    key={index}
                                    className="text-5xl text-white font-bold national-parks tracking-wide"
                                >
                                    {letter}
                                </span>
                            ))}
                        </div>

                        {/* Horizontal letters */}
                        <div
                            className="absolute bottom-0 w-full flex justify-between"
                            style={{
                                paddingLeft: "50px",
                                paddingRight: "50px",
                                gap: "25px",
                            }}
                        >
                            {horizontalLetters.map((letter, index) => (
                                <span
                                    key={index}
                                    className="text-5xl text-white font-bold national-parks tracking-wide"
                                >
                                    {letter}
                                </span>
                            ))}
                        </div>
                    </div>
                </div>

                {/* Top Text Layer */}
                <div
                    className="absolute top-0 right-0 p-4 transition-opacity duration-300 ease-in-out"
                    style={{
                        width: "200px",
                        zIndex: 2,
                        opacity: hovered ? 0 : 1,
                        textAlign: "right",
                    }}
                >
                    <p className="text-2xl text-white leading-snug font-bold tracking-wide national-parks">
                        {topText}
                    </p>
                </div>

                {/* Buttons Layer */}
                <div
                    className="absolute inset-0 flex flex-col items-center justify-center"
                    style={{
                        zIndex: 3,
                        opacity: hovered ? 1 : 0,
                        transition: "opacity 0.3s ease-in-out, transform 0.3s ease-in-out",
                        transform: hovered ? "scale(1.07)" : "scale(0.9)",
                    }}
                >
                    {/* 2x2 grid (150x150 each) */}
                    <div className="w-full h-full grid grid-cols-2 grid-rows-2">
                        {items.map(({ label, icon }) => (
                            <div
                                key={label}
                                className="flex flex-col items-center justify-center border border-white transition-colors duration-300 ease-in-out"
                                style={{
                                    backgroundColor: baseColor,
                                    cursor: "pointer",
                                    padding: "10px"
                                }}
                                onClick={() => onItemClick({ label, icon, colors, baseColor, hoverColor })}
                            >
                                <div className="mb-2" style={{ color: "white" }}>
                                    {icon}
                                </div>
                                <p className="text-lg font-bold">{label}</p>
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {/* Bottom Text (unchanged) */}
            <div className="text-center mt-8">
                <p className="text-2xl font-bold tracking-wide national-parks text-white">
                    {bottomText}
                </p>
            </div>
        </div>
    );
};

const ExpandedView = ({ item, onClose, containerRef }) => {
    const { label, icon, hoverColor } = item;

    const [rect, setRect] = useState(null);

    // Evidence sub-items
    const evidenceItems = [
        { label: "LLM studies", icon: LLMIcon, onClick: () => window.open("https://gitlab.com/qualisign/agency-protocol/-/blob/main/docs/llm_experiment_protocols.org", "_blank") },
        { label: "ABM studies", icon: ABMIcon, onClick: () => alert("coming soon") },
        { label: "Proof", icon: ProofIcon, onClick: () => window.open("https://gitlab.com/qualisign/agency-protocol/-/blob/main/docs/proof.org", "_blank") },
        { label: "Vouching", icon: VouchingIcon, onClick: () => alert("coming soon") },
    ];

    // Events sub-items (change group sensemaking label and icon)
    const eventsItems = [
        { label: "Group hypnosis", icon: GroupHypnosisIcon, onClick: () => alert("coming soon") },
        { label: "Group sensemaking (de-hypnosis)", icon: GroupSensemakingIcon, onClick: () => alert("coming soon") },
    ];

    // Learn sub-items (instead of Training)
    const learnItems = [
        { label: "Courses", icon: CoursesIcon, onClick: () => alert("coming soon") },
        { label: "Tutorials", icon: TutorialsLearnIcon, onClick: () => alert("coming soon") },
        { label: "Docs", icon: DocsIcon, onClick: () => alert("coming soon") },
        { label: "Mentoring", icon: MentoringIcon, onClick: () => sendEmail() }, // send email
    ];

    // Consulting (collective side) just sends email
    // Investing inquiries sends email
    const investingItems = [
        { label: "Business plan", icon: BusinessPlanIcon, onClick: () => alert("coming soon") },
        { label: "Inquiries", icon: InquiriesIcon, onClick: () => sendEmail() }, // send email
    ];

    // Agents sub-items
    const agentsGridItems = AgentsItems.map((a) => ({
        label: a.label,
        icon: <AgentItemIcon label={a.label} />,
        onClick: () => window.open(a.url, "_blank")
    }));

    useEffect(() => {
        if (containerRef.current) {
            const bounds = containerRef.current.getBoundingClientRect();
            setRect(bounds);
        }
    }, [containerRef]);

    if (!rect) return null;

    const contentStyle = {
        color: "white",
        fontSize: "1.3rem",
        lineHeight: "1.8",
        marginBottom: "20px",
        marginTop: "20px"
    };

    return (
        <div
            style={{
                position: "absolute",
                top: 0,
                left: 0,
                width: rect.width,
                height: rect.height,
                background: `linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), ${hoverColor}`,
                border: "2px solid white",
                boxSizing: "border-box",
                zIndex: 999,
                overflow: "hidden",
                padding: "30px",
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
                backdropFilter: "blur(10px)",
                transition: "transform 1s ease-in-out, opacity 1s ease-in-out"
            }}
        >
            {/* Close X button */}
            <button
                onClick={onClose}
                style={{
                    position: "absolute",
                    top: "30px",
                    right: "30px",
                    background: "none",
                    border: "none",
                    fontSize: "1.5rem",
                    color: "white",
                    cursor: "pointer"
                }}
            >
                ✕
            </button>

            {/* Title and icon top-left */}
            <div style={{ display: "flex", alignItems: "center", marginBottom: "20px", color: "white" }}>
                <div style={{ marginRight: "8px" }}>
                    {icon}
                </div>
                <h2 className="text-2xl font-bold">{label}</h2>
            </div>

            {label === "Evidence" && (
                <div className="w-full h-full grid grid-cols-2 grid-rows-2 gap-4">
                    {evidenceItems.map((it) => (
                        <div
                            key={it.label}
                            className="flex flex-col items-center justify-center border border-white"
                            style={{
                                backgroundColor: hoverColor,
                                cursor: "pointer",
                                padding: "10px"
                            }}
                            onClick={it.onClick}
                        >
                            <div className="mb-2" style={{ color: "white" }}>
                                {it.icon}
                            </div>
                            <p className="text-lg font-bold text-white text-center">{it.label}</p>
                        </div>
                    ))}
                </div>
            )}

            {label === "Events" && (
                <div className="w-full h-full grid grid-cols-2 gap-4">
                    {eventsItems.map((it) => (
                        <div
                            key={it.label}
                            className="flex flex-col items-center justify-center border border-white"
                            style={{
                                backgroundColor: hoverColor,
                                cursor: "pointer",
                                padding: "10px"
                            }}
                            onClick={it.onClick}
                        >
                            <div className="mb-2" style={{ color: "white" }}>
                                {it.icon}
                            </div>
                            <p className="text-lg font-bold text-white text-center">{it.label}</p>
                        </div>
                    ))}
                </div>
            )}

            {label === "Learn" && (
                <div className="w-full h-full grid grid-cols-2 grid-rows-2 gap-4">
                    {learnItems.map((it) => (
                        <div
                            key={it.label}
                            className="flex flex-col items-center justify-center border border-white"
                            style={{
                                backgroundColor: hoverColor,
                                cursor: "pointer",
                                padding: "10px"
                            }}
                            onClick={it.onClick}
                        >
                            <div className="mb-2" style={{ color: "white" }}>
                                {it.icon}
                            </div>
                            <p className="text-lg font-bold text-white text-center">{it.label}</p>
                        </div>
                    ))}
                </div>
            )}

            {label === "Investing" && (
                <div className="w-full h-full grid grid-cols-2 gap-4">
                    {investingItems.map((it) => (
                        <div
                            key={it.label}
                            className="flex flex-col items-center justify-center border border-white"
                            style={{
                                backgroundColor: hoverColor,
                                cursor: "pointer",
                                padding: "10px"
                            }}
                            onClick={it.onClick}
                        >
                            <div className="mb-2" style={{ color: "white" }}>
                                {it.icon}
                            </div>
                            <p className="text-lg font-bold text-white text-center">{it.label}</p>
                        </div>
                    ))}
                </div>
            )}

            {label === "Agents" && (
                <div className="w-full h-full grid grid-cols-4 grid-rows-2 gap-4">
                    {agentsGridItems.map((it) => (
                        <div
                            key={it.label}
                            className="flex flex-col items-center justify-center border border-white"
                            style={{
                                backgroundColor: hoverColor,
                                cursor: "pointer",
                                padding: "10px"
                            }}
                            onClick={it.onClick}
                        >
                            <div className="mb-2" style={{ color: "white" }}>
                                {it.icon}
                            </div>
                            <p className="text-sm font-bold text-white text-center">{it.label}</p>
                        </div>
                    ))}
                </div>
            )}

            {label === "Consulting" && (
                <div style={{ color: "white", fontSize: "1.3rem", lineHeight: "1.8", marginBottom: "20px", marginTop: "20px" }}>
                    <p style={{ marginBottom: "30px" }}>
                        Contact us for consulting services.
                    </p>
                    <button
                        style={{
                            backgroundColor: hoverColor,
                            border: "2px solid white",
                            color: "white",
                            padding: "15px 30px",
                            cursor: "pointer",
                            fontWeight: "bold",
                            fontSize: "1.1rem"
                        }}
                        onClick={() => sendEmail()}
                    >
                        Send Email
                    </button>
                </div>
            )}

            {label === "Reprogramming" && (
                <div style={{ overflowY: "auto" }}>
                    {/* Only keep the three requested paragraphs */}
                    <p style={{ ...contentStyle }}>
                        In the domain of AI, context priming is a way to give an LLM a set of instructions that will prime it to respond to you in a certain way.
                    </p>
                    <p style={{ ...contentStyle }}>
                        For example, before asking the LLM to help you with a software problem, you can tell it "you are a software engineer", and its answers will be better.
                    </p>
                    <p style={{ ...contentStyle }}>
                        Human Context Priming is based on the realization that humans can consciously prime themselves very similarly to how we prime LLMs. In other words, there are ways of telling oneself, "I am a successful entrepreneur" such that it makes one more likely to act like one.
                    </p>

                    <div style={{ marginTop: "30px", display: "flex", gap: "20px" }}>
                        <button
                            style={{
                                backgroundColor: hoverColor,
                                border: "2px solid white",
                                color: "white",
                                padding: "15px 30px",
                                cursor: "pointer",
                                fontWeight: "bold",
                                fontSize: "1.1rem"
                            }}
                            onClick={() => alert("coming soon")}
                        >
                            Schedule exploratory session
                        </button>

                        <button
                            style={{
                                backgroundColor: hoverColor,
                                border: "2px solid white",
                                color: "white",
                                padding: "15px 30px",
                                cursor: "pointer",
                                fontWeight: "bold",
                                fontSize: "1.1rem"
                            }}
                            onClick={() => alert("coming soon")}
                        >
                            Schedule HCP session
                        </button>
                    </div>
                </div>
            )}

            {label === "Goals" && (
                <div style={{ overflowY: "auto" }}>
                    <p style={{ ...contentStyle }}>
                        The goals of this system are to enhance personal and collective agency, facilitate meaningful transformations, and foster collaborative sensemaking. By providing resources, studies, and interactive sessions, we aim to empower individuals and groups to achieve their aspirations and adapt effectively in a changing world.
                    </p>
                </div>
            )}
        </div>
    );
};

const AgencyProtocol = () => {
    const [hoveredIndividual, setHoveredIndividual] = useState(false);
    const [hoveredCollective, setHoveredCollective] = useState(false);
    const [expandedItem, setExpandedItem] = useState(null);

    const containerRef = useRef(null);

    const individualBaseColors = [
        "hsl(210, 85%, 15%)",
        "hsl(190, 70%, 25%)",
        "hsl(180, 60%, 25%)"
    ];
    const collectiveBaseColors = [
        "hsl(40, 85%, 35%)",
        "hsl(30, 75%, 25%)",
        "hsl(25, 65%, 20%)"
    ];

    const individualColors = useMorphicColor(individualBaseColors);
    const collectiveColors = useMorphicColor(collectiveBaseColors);

    // Individuals: Goals (was Exploration), Learn (was Training), Reprogramming, Events
    const individualItems = [
        { label: "Goals", icon: GoalsIcon },
        { label: "Learn", icon: LearnIcon },
        { label: "Reprogramming", icon: ReprogrammingIcon },
        { label: "Events", icon: EventsIcon },
    ];

    // Collectives: Agents, Consulting (was Training), Investing, Evidence
    const collectiveItems = [
        { label: "Agents", icon: AgentsIcon },
        { label: "Consulting", icon: ConsultingIcon },
        { label: "Investing", icon: InvestingIcon },
        { label: "Evidence", icon: EvidenceIcon },
    ];

    const individualBaseColor = "rgba(0, 40, 60, 0.5)";
    const individualHoverColor = "rgba(0, 30, 45, 0.7)";

    const collectiveBaseColor = "rgba(60, 40, 0, 0.5)";
    const collectiveHoverColor = "rgba(45, 30, 0, 0.7)";

    return (
        <div className="min-h-screen bg-slate-900">
            <div className="max-w-7xl mx-auto p-12" style={{ position: "relative" }}>
                <div className="text-center mb-6">
                    <h1 className="inline-block text-6xl font-bold text-white tracking-widest uppercase national-parks pb-1">
                        AGENCY PROTOCOL
                    </h1>
                    <div
                        className="h-1 mx-auto"
                        style={{
                            width: "calc(100% / 2)",
                            maxWidth: "550px",
                            backgroundColor: "hsl(195, 75%, 50%)",
                        }}
                    ></div>
                </div>

                <div
                    ref={containerRef}
                    className="grid grid-cols-2 gap-12"
                    style={{ position: "relative" }}
                >
                    <AlchemicalSection
                        type="Individuals"
                        colors={individualColors}
                        topText="Enhance personal agency"
                        bottomText="Reprogram yourself"
                        hovered={hoveredIndividual}
                        onMouseEnter={() => setHoveredIndividual(true)}
                        onMouseLeave={() => setHoveredIndividual(false)}
                        items={individualItems}
                        baseColor={individualBaseColor}
                        hoverColor={individualHoverColor}
                        onItemClick={(item) => setExpandedItem(item)}
                    />

                    <AlchemicalSection
                        type="Collectives"
                        colors={collectiveColors}
                        topText="Enhance agency at scale"
                        bottomText="Reprogram society"
                        hovered={hoveredCollective}
                        onMouseEnter={() => setHoveredCollective(true)}
                        onMouseLeave={() => setHoveredCollective(false)}
                        items={collectiveItems}
                        baseColor={collectiveBaseColor}
                        hoverColor={collectiveHoverColor}
                        onItemClick={(item) => setExpandedItem(item)}
                    />

                    {expandedItem && (
                        <ExpandedView
                            item={expandedItem}
                            onClose={() => setExpandedItem(null)}
                            containerRef={containerRef}
                        />
                    )}
                </div>
            </div>
        </div>
    );
};

export default AgencyProtocol;
