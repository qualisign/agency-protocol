/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  
  theme: {
    extend: {
      animation: {
        'gradient-shift': 'gradient-shift 20s ease infinite',
      },
      keyframes: {
        'gradient-shift': {
          '0%': { 'background-position': '0% 50%' },
          '50%': { 'background-position': '50% 100%' },
          '100%': { 'background-position': '100% 50%' },
        },
      },
      backgroundImage: {
        'individual-gradient': 'linear-gradient(135deg, hsl(230, 45%, 65%), hsl(225, 40%, 55%), hsl(220, 35%, 45%))',
        'collective-gradient': 'linear-gradient(135deg, hsl(320, 25%, 65%), hsl(315, 20%, 55%), hsl(310, 15%, 45%))',
      },
      backgroundSize: {
        '200%': '200% 200%',
      },
    },
  },
  plugins: [],
}
