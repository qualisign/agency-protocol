# Agency Protocol Architecture

## Domain-Driven Design Structure

### Layers
1. Domain Layer
   - Core business logic and entities
   - No dependencies on other layers
   - Contains: Agent, Promise, Assessment, Merit aggregates

2. Application Layer
   - Orchestrates domain objects
   - Use cases and application services
   - Depends only on domain layer

3. Infrastructure Layer
   - Implements interfaces defined in domain
   - External services, persistence, etc
   - Content-addressable storage implementation
   - Cryptographic services

4. Interface Layer
   - API endpoints
   - Request/response handling
   - DTOs and transformations

### Key DDD Patterns Used
- Aggregates: Agent is root aggregate
- Value Objects: ContentAddress, PublicKey, Signature, Merit
- Entities: Promise, Assessment, Credit
- Domain Events: AgentCreated, PromiseMade, MeritUpdated, etc.
- Repositories: AgentRepository, PromiseRepository, MeritRepository

### Testing Strategy
- Domain layer: Unit tests
- Application layer: Integration tests
- Interface layer: E2E tests with cucumber
- Infrastructure layer: Integration tests

#### BDD Implementation Guidelines
- Feature files define behavior in business language
- Step definitions map to domain operations
- Each scenario maps to a specific use case
- Use regex patterns for dynamic values in steps
- Keep step definitions focused on single responsibility
- Reuse steps across scenarios when possible
- Maintain test isolation with proper setup/teardown

### Merit and Credit System
[Rest of existing content...]
