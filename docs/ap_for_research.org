#+TITLE: Applying the Agency Protocol to Scientific Research

By modeling concepts as agents with specific promises, the Agency Protocol (AP) facilitates transparent and collaborative scientific discourse. The following examples demonstrate how researchers can navigate differing definitions and perspectives using AP.

* Example 1: Differing Definitions of "Species" in Biology

** Creating an Agent for a New Butterfly Species

A researcher discovers a new butterfly species and creates an agent to represent it:

- *Agent*: Butterfly → *Some Species*

** Promises Made by the New Species Agent

The agent representing *Some Species* inherits promises from its ancestors and makes its own specific promises:

- *Inherited Promises*:
  - All promises made by the general *Butterfly* agent.
  - All promises made by butterfly ancestors (e.g., insects, animals).
- *Specific Promises of Some Species*:
  - Unique promises not made by its ancestors, such as specific wing patterns or behaviors.

** Modeling Differing Definitions of "Species"

In biology, multiple definitions exist for what constitutes a species. Each definition can be modeled as an agent with its own set of promises, even if they share the same name.

*** Agent: Species v1 (Reproductive Definition)
:PROPERTIES:
:ID: species-v1
:END:

- *Promise to be Named*: "Species"
- *Promises*:
  - **Promise of Reproduction**: Engages in reproductive behaviors to ensure the continuation of its genetic lineage.
    - *Example*: Flowering plants releasing pollen for cross-pollination.
  - **Promise of Reproductive Isolation**: Maintains mechanisms to prevent interbreeding with other species.
    - *Example*: Unique mating calls in bird species that prevent hybridization.

*** Agent: Species v2 (Genetic Cohesion Definition)
:PROPERTIES:
:ID: species-v2
:END:

- *Promise to be Named*: "Species"
- *Promises*:
  - **Promise of Genetic Similarity**: Members maintain a certain level of genetic compatibility.
    - *Example*: Consistent genetic markers within a mammalian species.
  - **Promise of a Common Gene Pool**: Ensures the gene pool remains relatively closed, barring mutations or rare genetic exchanges.
    - *Example*: Self-pollinating plants maintaining genetic consistency.

*** Agent: Species v3 (Ecological Niche Definition)
:PROPERTIES:
:ID: species-v3
:END:

- *Promise to be Named*: "Species"
- *Promises*:
  - **Promise to Occupy a Specific Niche**: Fulfills a particular role within the ecosystem (e.g., pollinator, predator, decomposer).
    - *Example*: Bees acting as pollinators for flowering plants.
  - **Promise of Resource Utilization**: Uses resources to support survival without depleting the ecosystem.
    - *Example*: Grazing herbivores regulating plant populations.

** Scientific Debate Using the Agency Protocol

*** Researcher A's Perspective (Genetic Cohesion)

- Researcher A believes that *Some Species* qualifies as a species under the Genetic Cohesion Definition (*species-v2*).
- *Evidence Provided*:
  - Demonstrates genetic similarity among specimens.
  - Shows a closed gene pool with limited genetic exchange.

*** Researcher B's Perspective (Ecological Niche)

- Researcher B subscribes to the Ecological Niche Definition (*species-v3*).
- *Assessment of Some Species*:
  - Assesses the promise to be named "Species" as **Broken** under *species-v3*.
- *Reasoning*:
  - *Some Species* does not occupy a distinct ecological niche.
  - Lacks unique resource utilization patterns.

** Assessing Promises and Facilitating Debate

Using the Agency Protocol, researchers can formally assess the promises made by agents:

- *Assessment by Researcher B*:
  - *Agent Assessed*: *Some Species*
  - *Promise Assessed*: To be a "Species" under *species-v3*.
  - *Assessment Result*: **Broken**
  - *Comments*:
    - Provides data on ecological overlap with existing species.
    - Questions the distinctiveness of *Some Species* in the ecosystem.

** Role of Comments as Agents

Comments and critiques are also modeled as agents with their own promises:

- *Agent*: Comment by Researcher B
- *Promises*:
  - **Promise to be Factual**: Presents accurate ecological data.
  - **Promise to be On Topic**: Addresses the classification under *species-v3*.
- *Assessment of Comment*:
  - Other researchers assess the comment for adherence to its promises.
  - Promotes a constructive and transparent debate.

** Consensus Building Through the Agency Protocol

The AP framework facilitates open scientific discourse:

- *Transparent Process*:
  - All agents, promises, and assessments are recorded.
  - Researchers can review and contribute to the discussion.
- *Potential Outcomes*:
  - Consensus may be reached on the classification of *Some Species*.
  - Alternatively, the debate highlights the need for revised definitions or further research.

** Conclusion

By modeling definitions, species, and discussions as agents with explicit promises, the Agency Protocol provides a structured and transparent method for scientific debate and consensus formation. This approach allows diverse viewpoints to be considered and assessed systematically, fostering collaboration and progress in scientific research.

* Example 2: The Reclassification of Pluto as a Dwarf Planet

** Introduction

In 2006, the International Astronomical Union (IAU) redefined the criteria for what constitutes a planet, leading to the reclassification of Pluto from a planet to a dwarf planet. This decision resulted from extensive debate and consensus formation within the astronomical community. Using the Agency Protocol, we can model this process where agents represent celestial bodies, definitions, and scientific viewpoints, and promises represent the properties and commitments associated with them.

** Agency Protocol Representation

*** 1. Agents and Their Promises

- **Agent: Planet v1 (Classical Definition)**
  - *Promise to be Named*: "Planet"
  - *Promises*:
    - **Promise of Orbital Dominance**: Orbits the Sun and is the dominant gravitational body in its orbit.
    - **Promise of Sphericity**: Has sufficient mass for its self-gravity to overcome rigid body forces, assuming a nearly round shape.
  - *Examples*: Earth, Mars, Jupiter.

- **Agent: Planet v2 (IAU 2006 Definition)**
  - *Promise to be Named*: "Planet"
  - *Promises*:
    - **Orbits the Sun**: The body must orbit the Sun.
    - **Spherical Shape**: Must be in hydrostatic equilibrium (nearly round).
    - **Cleared the Neighborhood**: Has cleared its orbital neighborhood of other debris.
  - *Examples*: Earth, Mars, Jupiter (Pluto does not fulfill the third promise under this definition).

- **Agent: Dwarf Planet**
  - *Promise to be Named*: "Dwarf Planet"
  - *Promises*:
    - Orbits the Sun.
    - Spherical Shape.
    - **Has NOT Cleared the Neighborhood**: Does not dominate its orbit.
  - *Examples*: Pluto, Eris, Ceres.

*** 2. Process of Consensus Formation

- **Discovery of Trans-Neptunian Objects (TNOs)**
  - New celestial bodies similar in size to Pluto were discovered in the Kuiper Belt (e.g., Eris).
  - *Agent: Eris*
    - *Promises*:
      - Shares characteristics with Pluto.
      - Challenges the existing classification of planets.

- **Scientific Debate**
  - *Agent: Astronomer A* (Supporting Classical Definition)
    - *Promise*: Advocates that Pluto should remain a planet based on historical context and its characteristics.
    - *Assessment*: Assesses the promise of *Planet v1* as **Kept** concerning Pluto.
  - *Agent: Astronomer B* (Supporting New Definition)
    - *Promise*: Proposes a new definition to accommodate new discoveries.
    - *Assessment*: Assesses the promise of *Planet v2* as **Not Kept** by Pluto.

*** 3. Assessments and Revisions

- **Assessments of Promises**
  - The astronomical community assesses the promises made by *Planet v1* and *Planet v2* agents.
  - The promise of "Cleared the Neighborhood" becomes a critical criterion.
- **Comments and Discussions**
  - Agents (astronomers) submit comments (also agents) assessed for factual accuracy and relevance.
  - *Agent: Comment on Orbital Dominance*
    - *Promises*:
      - Provides data on objects in Pluto's orbit.
      - Supports the assessment that Pluto has not cleared its neighborhood.

*** 4. Formation of Consensus

- **Voting and Formal Decision**
  - The IAU holds a vote among its members to adopt the new definition.
  - *Agent: IAU Resolution 5A*
    - *Promises*:
      - Establishes the new definition of a planet.
      - Reclassifies Pluto as a dwarf planet.
- **Community Assessment**
  - The astronomical community assesses the IAU's promises.
  - Majority accept the new definition, considering the promise **Kept**.
  - Some dissenting astronomers assess the promise as **Broken**, leading to ongoing debate.

*** 5. Outcome

- **Reclassification of Pluto**
  - *Agent: Pluto*
    - *Promises*:
      - Orbits the Sun.
      - Is spherical in shape.
      - Does not clear its neighborhood.
    - Under *Planet v2*, Pluto's promise to be a planet is assessed as **Broken**.
    - Under *Dwarf Planet*, Pluto's promises are assessed as **Kept**.
- **Transparency and Participation**
  - The process is documented and accessible.
  - Astronomers worldwide can participate in further assessments and discussions.

** Benefits of Using the Agency Protocol in This Context

- **Clarity of Definitions**: By representing definitions as agents with explicit promises, the criteria being proposed or challenged are clear.
- **Structured Debate**: Assessments and comments are formalized, making the debate focused on specific promises.
- **Transparency**: All assessments and revisions are recorded, allowing others to understand the rationale behind decisions.
- **Consensus Building**: Facilitates a process where the scientific community can contribute to consensus formation.

** Conclusion

The reclassification of Pluto illustrates how scientific consensus can be modeled using the Agency Protocol. By defining agents (e.g., celestial bodies, definitions, scientists) and their promises, and allowing assessments and comments to be systematically recorded and evaluated, the scientific community can transparently navigate complex debates. This approach promotes openness, accountability, and collaborative progress in advancing knowledge.

* Example 3: Debating the Effectiveness of Hypnosis for IBS Treatment

** Introduction

In the medical community, significant debate exists over the effectiveness of hypnosis as a treatment for Irritable Bowel Syndrome (IBS). The controversy centers around differing views on what constitutes valid evidence for medical interventions:

1. **Viewpoint 1 (RCT Gold Standard)**: Randomized Controlled Trials (RCTs) are considered the gold standard for determining effectiveness. Proponents argue that hypnosis lacks sufficient RCT evidence to support its use, leading them to dismiss it as a valid treatment.

2. **Viewpoint 2 (Alternative Evidence Proponents)**: Supporters of hypnosis cite studies, including RCTs and meta-analyses, showing that hypnosis can be more effective for treating IBS than some interventions backed by the traditional "gold standard." They argue that the unique nature of hypnosis requires a broader interpretation of evidence.

Using the Agency Protocol, we can model these differing viewpoints as agents with distinct promises and illustrate how the debate might unfold, including concrete examples and realistic scenarios.

** Agency Protocol Representation

1. **Agents Representing Definitions of "Evidence"**

   *** Agent: Evidence v1 (RCT Gold Standard Definition)

   - **Promise to be Named**: "Evidence"
   - **Promises**:
     - **Promise of High-Quality RCTs**: Only considers evidence from large-scale, double-blind, placebo-controlled RCTs.
     - **Promise of Quantitative Outcomes**: Requires statistically significant results measured by standardized instruments.
     - **Promise of Reproducibility**: Findings must be replicated consistently across multiple studies.
     - *Example*: A multicenter RCT demonstrating that a new medication significantly reduces IBS symptoms compared to a placebo.

   *** Agent: Evidence v2 (Holistic Evidence Definition)

   - **Promise to be Named**: "Evidence"
   - **Promises**:
     - **Promise of Diverse Research Methods**: Accepts evidence from RCTs but also values meta-analyses, observational studies, and clinical practice outcomes.
     - **Promise of Clinical Effectiveness**: Emphasizes real-world effectiveness and patient-reported outcomes.
     - **Promise of Mechanistic Understanding**: Considers physiological and psychological mechanisms supporting the intervention's efficacy.
     - *Example*: A meta-analysis showing that hypnosis leads to significant improvements in IBS symptoms, along with explanations of how hypnosis affects gut-brain interactions.

2. **An Agent Created as Evidence by a Practitioner**

   *** Agent: Hypnosis for IBS Evidence

   - **Created by**: Dr. Emily Smith, a gastroenterologist and certified hypnotherapist who subscribes to *Evidence v2*.
   - **Promises**:
     - **Promise to be Named**: "Evidence"
     - **Promise of Effective Treatment Outcomes**: Presents data showing significant symptom reduction in IBS patients treated with hypnosis.
     - **Promise of Supporting Research**: References specific studies, including RCTs and meta-analyses, demonstrating hypnosis's efficacy.
     - **Promise of Mechanistic Plausibility**: Explains how hypnosis may influence gut motility and pain perception through the gut-brain axis.
   - **Concrete Examples**:
     - **Study 1**: A 2003 RCT published in *The Lancet* by Whorwell et al., where 204 IBS patients underwent gut-focused hypnotherapy, resulting in a 70% improvement rate compared to 25% in the control group.
     - **Study 2**: A 2015 meta-analysis in *The American Journal of Clinical Hypnosis* indicating that hypnosis significantly reduces IBS symptoms across multiple studies.
     - **Clinical Practice**: In Dr. Smith's practice, 65 out of 80 IBS patients reported substantial symptom relief after a 12-session hypnotherapy program.

3. **Assessments by Practitioners with a Different Definition**

   *** Agent: Dr. Michael Johnson

   - **Subscribes to**: *Evidence v1* (RCT Gold Standard Definition).
   - **Assessment of Hypnosis for IBS Evidence**:
     - **Assesses the Promise to be Named "Evidence" as**: **Broken**
     - **Reasoning**:
       - **Quality of RCTs**: Argues that the cited RCTs have methodological flaws, such as small sample sizes, lack of proper blinding, or inadequate control groups.
       - **Placebo Effect Concerns**: Suggests that hypnosis benefits may be due to placebo effects rather than a specific therapeutic action.
       - **Reproducibility Issues**: Points out inconsistencies across different studies and difficulty replicating outcomes in larger, more rigorous trials.
       - **Standardization Challenges**: Notes that hypnosis protocols vary widely, making it hard to standardize treatment for evaluation.
     - **Concrete Examples**:
       - **Study Critique**: References a 2012 systematic review in *Gastroenterology* that found the overall quality of hypnosis studies for IBS to be low to moderate, with a high risk of bias.
       - **Failed Replication**: Mentions a large-scale RCT attempted in 2016 that failed to show significant differences between hypnosis and an educational control group.

4. **Group Sensemaking Process Facilitated by AP**

   - **Transparent Recording of Assessments**:
     - Dr. Johnson's assessments and detailed critiques are recorded in the AP system.
     - Other practitioners and researchers can access these assessments, review the reasoning, and examine the evidence.
   - **Additional Agents Join the Discussion**:
     - *** Agent: Dr. Lisa Martinez
       - **Subscribes to**: *Evidence v2*.
       - **Assessment of Dr. Johnson's Critique**:
         - **Assesses Dr. Johnson's Promise of Factual Accuracy as**: **Partially Broken**
         - **Reasoning**:
           - **Methodological Improvements**: Points out recent high-quality studies that address previous methodological concerns.
           - **Effect Size Considerations**: Argues that even modest improvements are clinically significant for IBS patients with limited treatment options.
           - **Mechanistic Evidence**: Emphasizes emerging neuroimaging studies showing how hypnosis alters brain activity related to pain perception.
         - **Concrete Examples**:
           - **Recent Study**: Highlights a 2019 RCT published in *Alimentary Pharmacology & Therapeutics* where a well-controlled trial showed that hypnosis significantly reduced IBS symptoms compared to both standard care and active controls.
           - **Neuroimaging Evidence**: References a 2018 study using fMRI scans demonstrating that hypnosis modulates activity in brain regions associated with pain processing.
     - *** Agent: Dr. Robert Lee
       - **Subscribes to**: *Evidence v1*.
       - **Proposes a New Agent**: *Evidence v1.1*
       - **Promises**:
         - **Promise of Rigorous Evaluation**: Agrees that while RCTs are essential, there is value in high-quality meta-analyses and systematic reviews.
         - **Promise of Critical Appraisal**: Emphasizes the need for critical appraisal of all evidence, regardless of source.
         - **Promise of Patient-Centered Outcomes**: Recognizes the importance of treatments that improve quality of life.
       - **Assessment**:
         - Suggests re-evaluating the existing evidence with stricter inclusion criteria.
         - Proposes collaborating on a comprehensive meta-analysis.

5. **Comments as Agents**

   - *** Agent: Comment by Dr. Martinez
     - **Promises**:
       - **Promise to be Factual**: Provides updated data on hypnosis effectiveness.
       - **Promise to be On Topic**: Addresses specific concerns raised by Dr. Johnson.
     - **Assessed by Others**:
       - Dr. Johnson assesses the promise to be factual as **Partially Kept**, acknowledging new studies but questioning their impact.
       - Dr. Lee assesses promises as **Kept**, appreciating the constructive contribution.

   - *** Agent: Comment by Dr. Johnson
     - **Promises**:
       - **Promise to be Factual**: Critiques the statistical significance and practical relevance of new studies.
       - **Promise to be On Topic**: Focuses on the validity of hypnosis as a treatment.
     - **Assessed by Others**:
       - Dr. Martinez assesses the promise to be factual as **Partially Broken**, citing potential misinterpretation of statistical data.
       - Dr. Smith provides additional data to clarify points.

6. **Evolution of Definitions and Consensus Formation**

   - **Assessing the Definitions Themselves**:
     - Practitioners begin to see merit in each other's perspectives.
     - *** Agent: Evidence v3 (Integrated Evidence Definition)
       - **Promises**:
         - **Promise of Comprehensive Evidence Assessment**: Combines high-quality RCTs, meta-analyses, and real-world clinical outcomes.
         - **Promise of Methodological Rigor**: Advocates for improving study designs to address previous limitations.
         - **Promise of Individualized Patient Care**: Acknowledges that patient preferences and responses vary, supporting a more personalized approach.
       - **Concrete Steps Proposed**:
         - Launching a collaborative, multicenter RCT with standardized hypnosis protocols and robust controls.
         - Establishing a registry for collecting long-term patient outcomes from clinical practice.
   - **Community Voting and Adoption**:
     - Practitioners use the AP system to vote on adopting *Evidence v3*.
     - A consensus emerges to broaden the definition of acceptable evidence while maintaining rigorous standards.

7. **Outcome and Consequences**

   - **Policy Changes**:
     - Medical societies update guidelines to reflect the inclusion of hypnosis as a potential treatment option for IBS under specific conditions.
     - Funding agencies provide grants for more extensive and rigorous research on hypnosis.
   - **Implementation in Practice**:
     - Gastroenterologists begin to refer patients for hypnotherapy as part of a multidisciplinary approach.
     - Insurance companies consider covering hypnotherapy sessions for IBS patients who have not responded to conventional treatments.
   - **Ongoing Assessments**:
     - The AP system continues to facilitate data sharing and assessment of new evidence.
     - Practitioners monitor patient outcomes to inform future treatment recommendations.

** How This Plays Out in Agency Protocol Terms

- **Transparency**: All promises, evidence, assessments, and reasoning are openly documented in the AP system, allowing for scrutiny and collaboration.
- **Inclusivity**: The AP framework accommodates multiple viewpoints, enabling practitioners to contribute diverse evidence and perspectives.
- **Dynamic Evolution**: Definitions of "evidence" evolve based on collective assessments, leading to more refined and applicable criteria.
- **Merit and Credibility**: Practitioners who provide well-substantiated contributions gain merit, influencing the direction of the debate and future research priorities.
- **Conflict Resolution**: By structuring the debate and encouraging collaborative solutions, AP helps reconcile differing viewpoints and promotes consensus.

** Conclusion

In this example, the Agency Protocol facilitates group sensemaking by:

- **Structuring the Debate**: Clearly defining differing views on what constitutes valid evidence for hypnosis in treating IBS.
- **Facilitating Constructive Dialogue**: Allowing practitioners to present specific studies, critique methodologies, and propose improvements.
- **Promoting Consensus Building**: Enabling the community to adopt a more nuanced definition of evidence that incorporates rigorous standards and acknowledges treatment complexities.
- **Enhancing Decision-Making**: Informing clinical guidelines and research priorities based on a comprehensive evaluation of available evidence.

This approach helps bridge the gap between strict adherence to traditional evidence hierarchies and the recognition of valuable alternative therapies, ultimately leading to more effective patient care and advancing medical knowledge.
